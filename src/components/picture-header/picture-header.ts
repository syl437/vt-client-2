import {Component, NgZone} from '@angular/core';
import {UtilsProvider} from '../../providers/utils/utils';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {NavController} from 'ionic-angular';

@Component({
    selector: 'picture-header',
    templateUrl: 'picture-header.html'
})
export class PictureHeaderComponent {

    full: number = 0;
    empty: number = 100;
    uploadProcess: boolean = false;
    avatar: string = '';
    points: number = 0;
    language: string;

    constructor(private zone: NgZone,
                public utils: UtilsProvider,
                public init: InitServiceProvider,
                public navCtrl: NavController) {

        this.utils.progress$.subscribe(full => {

            this.zone.run(() => {
                this.full = full;
                this.empty = 100 - full;
            });
        });

        this.utils.uploadProcess$.subscribe(value => this.uploadProcess = value);
        this.init.avatar$.subscribe(value => this.avatar = value); // same code here
        this.init.points$.subscribe(value => this.points = value);
        this.utils.language$.subscribe(data => this.language = data);
    }

    goToUser () {
        this.navCtrl.push('ProfilePage');
    }

}
