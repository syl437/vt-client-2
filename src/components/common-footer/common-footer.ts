import {Component} from '@angular/core';
import {NavController} from "ionic-angular";
import {InitServiceProvider} from "../../providers/init-service/init-service";

@Component({
    selector: 'common-footer',
    templateUrl: 'common-footer.html'
})
export class CommonFooterComponent {

    text: string;

    constructor(public navCtrl: NavController,
                public init: InitServiceProvider) {
    }


    goToHome() {
        this.navCtrl.setRoot('HomePage');
    }

    goToCoupons() {
        this.navCtrl.push('CouponsPage', {company_id: 0});
    }

    goToNetwork() {
        this.navCtrl.push('UsersPage');
    }

    goToMap() {
        if (this.init.coordinates.lat !== 0 && this.init.coordinates.lng !== 0) {
            this.navCtrl.push('MapPage');
        } else {
            this.init.getLocation().subscribe(data => this.navCtrl.push('MapPage'));
        }
    }

}
