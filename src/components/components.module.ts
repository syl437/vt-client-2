import { NgModule } from '@angular/core';
import {IonicPageModule} from "ionic-angular";
import {PictureHeaderComponent} from './picture-header/picture-header';
import {CommonHeaderComponent} from './common-header/common-header';
import {CommonFooterComponent} from './common-footer/common-footer';
import {DirectivesModule} from '../directives/directives.module';
import {TranslateModule} from '@ngx-translate/core';
@NgModule({
	declarations: [PictureHeaderComponent, CommonHeaderComponent, CommonFooterComponent],
	// imports: [DirectivesModule, TranslateModule],
	imports: [IonicPageModule.forChild([
		PictureHeaderComponent,
		CommonHeaderComponent,
		CommonFooterComponent,

	]),
        DirectivesModule,
        TranslateModule],
	exports: [PictureHeaderComponent, CommonHeaderComponent, CommonFooterComponent]
})
export class ComponentsModule {}
