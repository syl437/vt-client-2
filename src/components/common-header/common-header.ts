import {Component, Input, NgZone} from '@angular/core';
import {UtilsProvider} from "../../providers/utils/utils";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {NavController} from "ionic-angular";

@Component({
    selector: 'common-header',
    templateUrl: 'common-header.html'
})
export class CommonHeaderComponent {

    @Input()
    title: string;

    full: number = 0;
    empty: number = 100;
    uploadProcess: boolean = false;
    avatar: string = '';
    points: number = 0;

    constructor(private zone: NgZone,
                public utils: UtilsProvider,
                public init: InitServiceProvider,
                public navCtrl: NavController) {

        this.utils.progress$.subscribe(full => {

            this.zone.run(() => {
                this.full = full;
                this.empty = 100 - full;
            });
        });

        this.utils.uploadProcess$.subscribe(value => this.uploadProcess = value);
        this.init.avatar$.subscribe(value => this.avatar = value);
        this.init.points$.subscribe(value => this.points = value);
    }

    goToUser () {

        this.navCtrl.push('ProfilePage');

    }

}
