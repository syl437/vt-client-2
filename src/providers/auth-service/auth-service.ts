import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {BehaviorSubject, Observable} from 'rxjs';
import {Storage} from '@ionic/storage';
import {UtilsProvider} from '../utils/utils';

@Injectable()
export class AuthServiceProvider {

    // Token as string
    public token: string;
    public id: number;

    public _token: BehaviorSubject<string> = new BehaviorSubject('');

    // token as observable
    public readonly token$: Observable<string> = this._token.asObservable();

    constructor(private storage: Storage, public utils: UtilsProvider) {}

    // Reads token from storage (should return promise)
    initialize() {
        return new Promise((resolve, reject) => {

            this.storage.get('token').then((token) => {

                this.storage.get('id').then((val) => {

                    console.log('initialize()', val, token);
                    this.id = val || null;
                    this._token.next(token || '');
                    this.token = token || '';

                    resolve();

                })

            });
        });
    }

    // Getter
    getToken(): string {
        return this.token;
    }

    // Setter
    setToken(token: string): void {
        this.token = token;
        this.storage.set('token', this.token);
        this._token.next(token);
    }

    logout () {

        return new Promise (resolve => {

            this.storage.clear().then(() => {

                this.storage.set('entered', 1).then(() => {

                    this.utils.setLanguage('en');
                    this.id = 0;
                    this.token = '';
                    this._token.next('');
                    resolve();
                })

            })

        })

    }
}
