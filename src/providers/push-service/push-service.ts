import {Injectable} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {AuthServiceProvider} from '../auth-service/auth-service';
import {OneSignal} from '@ionic-native/onesignal';
import {ModalController, Platform} from 'ionic-angular';
import {InitServiceProvider} from '../init-service/init-service';

@Injectable()
export class PushService {

    constructor(public auth: AuthServiceProvider,
                public restangular: Restangular,
                private oneSignal: OneSignal,
                public initService: InitServiceProvider,
                public modalCtrl: ModalController,
                public platform: Platform){}

    async init() {

        if (this.platform.is('cordova')){
            this.oneSignal.startInit('e4ed69d6-511b-4601-ab3b-0f5b615f817f', '595323574268');

            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

            this.oneSignal.handleNotificationReceived().subscribe((data) => {
                console.log('handleNotificationReceived', JSON.stringify(data));
                if (data && data.payload && data.payload.additionalData && data.payload.additionalData.topic){
                    if (data.payload.additionalData.topic == 'points'){
                        this.initService.getUser();
                    }
                }
            });

            this.oneSignal.handleNotificationOpened().subscribe((data) => {
                console.log('handleNotificationOpened', JSON.stringify(data));
                if (data && data.notification && data.notification.payload && data.notification.payload.additionalData && data.notification.payload.additionalData.topic){
                    if (data.notification.payload.additionalData.topic == 'registration'){
                        let modal = this.modalCtrl.create('FacebookPage', {entity: 'banner'});
                        modal.present();
                    }
                }
            });

            this.oneSignal.endInit();
        }

    }

    logout () {
        this.restangular.one('users', this.auth.id).customGET('push').subscribe(data => console.log(data))
    }
}
