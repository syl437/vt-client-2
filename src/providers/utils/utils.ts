import {Injectable} from '@angular/core';
import {AlertController, Platform} from "ionic-angular";
import {Diagnostic} from '@ionic-native/diagnostic';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {TranslateService} from "@ngx-translate/core";
import {Storage} from "@ionic/storage";

@Injectable()
export class UtilsProvider {

    progress: number = 0;
    _progress: BehaviorSubject<number> = new BehaviorSubject(null);
    progress$: Observable<number> = this._progress.asObservable();

    uploadProcess: boolean = false;
    _uploadProcess: BehaviorSubject<boolean> = new BehaviorSubject(null);
    uploadProcess$: Observable<boolean> = this._uploadProcess.asObservable();

    _language: BehaviorSubject<string> = new BehaviorSubject('en');
    language$: Observable<string> = this._language.asObservable();
    get language() : string {return this._language.getValue()};

    baseUrl:string = 'http://app.y-travel.net/';
    // baseUrl:string = 'http://vt.kartisim.co.il/';

    constructor(public alertCtrl: AlertController,
                public diagnostic: Diagnostic,
                public translate: TranslateService,
                public storage: Storage,
                public platform: Platform) {}

    setLanguage (value) {
        return new Promise(async (resolve) => {
            await this.storage.set('language', value);
            await this._language.next(value);
            await this.translate.use(value);
            await this.platform.setDir(value === 'en' ? 'ltr' : 'rtl', true);
            resolve();
        })
    }

    isAnyFieldEmpty(x) {

        let keys = Object.getOwnPropertyNames(x);

        for (let key in keys) {

            if (x[keys[key]] === '' || typeof x[keys[key]] === 'undefined' || x[keys[key]] === null) {

                let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK']});
                alert.present();

                return true;

            }

        }

        return false;

    }

    isLocationAvailable (): any {

        return new Observable(observer => {

            this.diagnostic.isLocationAuthorized().then(isAuthorized => {

                console.log('isLocationAuthorized', isAuthorized);

                if (!isAuthorized) {

                    this.diagnostic.requestLocationAuthorization().then(status => {

                        if (status === "GRANTED"){

                            this.diagnostic.isLocationEnabled().then(isEnabled => {

                                console.log('isLocationEnabled', isEnabled);

                                if (isEnabled){

                                    observer.next({status: 'success'});
                                    observer.complete();

                                } else {

                                    observer.next({status: 'error', message: "Location is disabled"});
                                    observer.complete();

                                }

                            })

                        } else {

                            observer.next({status: 'error', message: "Location is not authorized"});
                            observer.complete();

                        }

                    });

                } else {

                    this.diagnostic.isLocationEnabled().then(isEnabled => {

                        console.log('isLocationEnabled', isEnabled);

                        if (isEnabled){

                            observer.next({status: 'success'});
                            observer.complete();

                        } else {

                            observer.next({status: 'error', message: "Location is disabled"});
                            observer.complete();

                        }

                    })

                }

            })

        })

    }

    setProgress (value) {
        this.progress = value;
        this._progress.next(value);
    }

    setUploadProcess (value) {
        this.uploadProcess = value;
        this._uploadProcess.next(value);
    }

}
