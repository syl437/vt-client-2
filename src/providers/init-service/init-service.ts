import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {Storage} from "@ionic/storage";
import {AuthServiceProvider} from "../auth-service/auth-service";
import {Restangular} from "ngx-restangular";
import {Geolocation} from '@ionic-native/geolocation';
import {UtilsProvider} from "../utils/utils";
import {Platform} from 'ionic-angular';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class InitServiceProvider {

    coordinates: any = {lat: 0, lng: 0};
    user: any;
    phone: string;
    following: Array<any>;
    countries: Array<{code: string, name: string}>;

    banners: Array<any>;
    _banners: BehaviorSubject<Array<any>> = new BehaviorSubject(null);
    banners$: Observable<Array<any>> = this._banners.asObservable();
    
    points: number;
    _points: BehaviorSubject<number> = new BehaviorSubject(null);
    points$: Observable<number> = this._points.asObservable();

    faq: Array<any>;
    _faq: BehaviorSubject<Array<any>> = new BehaviorSubject(null);
    faq$: Observable<Array<any>> = this._faq.asObservable();

    company_categories: Array<any>;
    _company_categories: BehaviorSubject<Array<any>> = new BehaviorSubject(null);
    company_categories$: Observable<Array<any>> = this._company_categories.asObservable();

    regions: Array<any>;
    _regions: BehaviorSubject<Array<any>> = new BehaviorSubject(null);
    regions$: Observable<Array<any>> = this._regions.asObservable();
    
    avatar: string;
    _avatar: BehaviorSubject<string> = new BehaviorSubject(null);
    avatar$: Observable<string> = this._avatar.asObservable();

    name: string;
    _name: BehaviorSubject<string> = new BehaviorSubject(null);
    name$: Observable<string> = this._name.asObservable();

    constructor(public restangular: Restangular,
                public storage: Storage,
                public auth: AuthServiceProvider,
                public geolocation: Geolocation,
                public utils: UtilsProvider,
                public platform: Platform) {
    }

    onInitApp() {

        return new Promise(resolve => {

            this.getLocation().subscribe(data => console.log(data));

            if (this.auth.getToken() !== '') {

                this.restangular.all('init').customGET().subscribe(data => {

                    this.company_categories = data.company_categories;
                    this._company_categories.next(data.company_categories);
                    
                    this.regions = data.regions;
                    this._regions.next(data.regions);
                    
                    this.user = data.user;
                    this.following = data.following;
                    this.phone = data.phone;
                    this.countries = data.countries;
                    
                    this.faq = data.faq;
                    this._faq.next(data.faq);

                    this.banners = data.banners;
                    this._banners.next(data.banners);
                    
                    this.setPoints(data.user.points);
                    this.setAvatar(data.user.avatar);
                    this.setName(data.user.name);

                    resolve();

                });

            } else {

                resolve();
            }

        });

    }

    getUser () {
        this.restangular.one('users', this.user.id).get().subscribe(data => {
            this.user = data;
            this.setPoints(data.points);
        });
    }

    setPoints (value) {
        this.points = value;
        this._points.next(value);
    }

    setAvatar (value) {
        this.avatar = value;
        this._avatar.next(value);
    }

    setName (value) {
        this.name = value;
        this._name.next(value);
    }

    getLocation() {

        return new Observable(observer => {

            if (this.platform.is('cordova')) {   // for phones

                this.utils.isLocationAvailable().subscribe(response => {

                    console.log(response);

                    if (response.status === 'success') {

                        this.geolocation.getCurrentPosition().then((response) => {

                            console.log(response.coords.latitude, response.coords.longitude);
                            this.coordinates.lat = response.coords.latitude;
                            this.coordinates.lng = response.coords.longitude;

                            observer.next(this.coordinates);
                            observer.complete();

                        }).catch((error) => {

                            observer.next({status: 'error', message: "Can't get location"});
                            observer.complete();

                        });

                    } else {

                        observer.next({status: 'error', message: "Location is not available"});
                        observer.complete();

                    }

                });

            } else {        // for browser

                this.geolocation.getCurrentPosition().then((response) => {

                    // console.log(response.coords.latitude, response.coords.longitude);
                    this.coordinates.lat = response.coords.latitude;
                    this.coordinates.lng = response.coords.longitude;

                    observer.next(this.coordinates);
                    observer.complete();

                }).catch((error) => {

                    observer.next({status: 'error', message: "Can't get location"});
                    observer.complete();

                });

            }

        })

    }

}
