import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, ErrorHandler, NgModule} from '@angular/core';
import {AlertController, App, IonicApp, IonicErrorHandler, IonicModule, LoadingController} from 'ionic-angular';

import {MyApp} from './app.component';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {IonicStorageModule, Storage} from '@ionic/storage';
import {RestangularModule} from 'ngx-restangular';
import {Camera} from '@ionic-native/camera';
import {MediaCapture} from '@ionic-native/media-capture';
import {AgmCoreModule} from '@agm/core';
import {FileTransfer} from '@ionic-native/file-transfer';
import {Geolocation} from '@ionic-native/geolocation';
import {Diagnostic} from '@ionic-native/diagnostic';
import {CallNumber} from '@ionic-native/call-number';
import {LaunchNavigator} from '@ionic-native/launch-navigator';
import {SocialSharing} from '@ionic-native/social-sharing';

import {UtilsProvider} from '../providers/utils/utils';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';
import {InitServiceProvider} from '../providers/init-service/init-service';
import {QRCodeModule} from 'angular2-qrcode';
import {ScreenOrientation} from '@ionic-native/screen-orientation';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NativeGeocoder} from '@ionic-native/native-geocoder';
import {File} from '@ionic-native/file';
import {PushService} from '../providers/push-service/push-service';
import {OneSignal} from '@ionic-native/onesignal';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {DirectivesModule} from '../directives/directives.module';
import {ComponentsModule} from '../components/components.module';
import {Facebook} from '@ionic-native/facebook';

export function initializeApplication (storage: Storage, utils: UtilsProvider) {
    return (): Promise<void> => {
        return new Promise<void>(async (resolve) => {
            try {
               let language = await storage.get('language');
               if (language == null){
                   language = 'en';
               }
                utils.setLanguage(language);
            }
            catch (err) {
                console.log('initializeApplicationError', err);
            }
            finally {
                resolve();
            }
        });
    };
}

export function RestangularConfigFactory(RestangularProvider, App, LoadingController, AlertController, AuthServiceProvider, UtilsProvider) {

    let loading;

    RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params)=> {

        loading = LoadingController.create({content: 'Please wait...'});
        loading.present();

        UtilsProvider.language$.subscribe(language => {
            params.lang = language;
            return {params: params, headers: headers, element: element}
        });

    });

    // RestangularProvider.setBaseUrl('http://vt.test/api/v1');
    // RestangularProvider.setBaseUrl('http://vt.kartisim.co.il/api/v1');
    RestangularProvider.setBaseUrl('https://app.y-travel.net/api/v1');

    // Set initial default header

    RestangularProvider.setDefaultHeaders({Authorization: `Bearer ${AuthServiceProvider.getToken()}`});

    // Subscribe to token change

    AuthServiceProvider.token$.subscribe((token) => RestangularProvider.setDefaultHeaders({Authorization: `Bearer ${token}`}));

    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {

        loading.dismiss();

        if (data.data) {
            console.log(url, data.data);
        }

        let extractedData = data.data;

        if (extractedData === null){
            extractedData = [];
        }

        return extractedData;

    });

    RestangularProvider.addErrorInterceptor((response, subject, responseHandler) => {

        loading.dismiss();

        console.log(response);

        if (response.error && response.error.message){
            AlertController.create({title:response.error.message, buttons: ['OK']}).present();
            return true;
        }

        if (response.data) {

            let title = response.data.message ? response.data.message : 'Server error';
            let message = '';

            if (response.data.error && response.data.error.message){
                message += response.data.error.message;
            }

            if (response.data.error && response.data.error.fields){
                for (let errors in response.data.error.fields){
                    for (let error in response.data.error.fields[errors]){
                        message += ' ' + response.data.error.fields[errors][error];
                    }
                }
            }

            AlertController.create({title:title, message: message, buttons: ['OK']}).present();

            if (response && response.data && response.data.status && response.data.status == 401){
                AuthServiceProvider.logout();
                App.getRootNavs()[0].setRoot('LoginPage');
            }

            return true;

        }

        return true;

    });

}

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        MyApp,

    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot({
            name: "__mydb",
            driverOrder: ["sqlite", "websql", "indexeddb"]
        }),
        RestangularModule.forRoot([App, LoadingController, AlertController, AuthServiceProvider, UtilsProvider], RestangularConfigFactory),
        AgmCoreModule.forRoot({apiKey: "AIzaSyA4uXPidpv7gDsUIXwS30CMQNs5M-t6DOs", libraries: ["places"]}),
        QRCodeModule,
        HttpClientModule,
        DirectivesModule,
        ComponentsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        Camera,
        MediaCapture,
        UtilsProvider,
        AuthServiceProvider,
        File,
        FileTransfer,
        Diagnostic,
        CallNumber,
        Geolocation,
        LaunchNavigator,
        InitServiceProvider,
        SocialSharing,
        ScreenOrientation,
        NativeGeocoder,
        PushService,
        Facebook,
        OneSignal,
        {
            'provide': APP_INITIALIZER,
            'useFactory': initializeApplication,
            'deps': [Storage, UtilsProvider],
            'multi': true
        },
    ]
})
export class AppModule {
}
