import {Component, ViewChild} from '@angular/core';
import {AlertController, Events, IonicApp, ModalController, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {UtilsProvider} from '../providers/utils/utils';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';
import {InitServiceProvider} from '../providers/init-service/init-service';
import {ScreenOrientation} from '@ionic-native/screen-orientation';
import {Storage} from '@ionic/storage';
import {TranslateService} from '@ngx-translate/core';
import {PushService} from '../providers/push-service/push-service';
import {Restangular} from 'ngx-restangular';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any;
    titles: Array<any>;
    pages: Array<{ title: string, component: any, icon: string }>;
    user: any = {avatar: '', name: '', points: 0};
    language: string;

    constructor(public platform: Platform,
                public statusBar: StatusBar,
                public splashScreen: SplashScreen,
                private auth: AuthServiceProvider,
                public utils: UtilsProvider,
                public init: InitServiceProvider,
                public screen: ScreenOrientation,
                public storage: Storage,
                public translate: TranslateService,
                public alertCtrl: AlertController,
                private ionicApp: IonicApp,
                public push: PushService,
                public events: Events,
                public restangular: Restangular,
                public modalCtrl: ModalController) {

        this.initializeApp();

        this.init.points$.subscribe(value => this.user.points = value);
        this.init.avatar$.subscribe(value => this.user.avatar = value);
        this.init.name$.subscribe(value => this.user.name = value);
        this.utils.language$.subscribe(value => this.language = value);

        translate.setDefaultLang('en');

        events.subscribe('language:hebrew', () => {console.log('hebrew');this.initialSetup()});

    }

    initializeApp() {

        this.platform.ready().then(() => {

            // close modal on back button

            this.platform.registerBackButtonAction(() => {

                let activePortal = this.ionicApp._loadingPortal.getActive() ||
                    this.ionicApp._modalPortal.getActive() ||
                    this.ionicApp._toastPortal.getActive() ||
                    this.ionicApp._overlayPortal.getActive();

                if (activePortal) {
                    activePortal.dismiss();
                } else {

                    if (this.nav.length() == 1) {

                        let alert = this.alertCtrl.create({
                            title: this.translate.instant("POPUPS.EXIT"),
                            message: this.translate.instant("POPUPS.WANTEXIT"),
                            buttons: [
                                {
                                    text: "OK", handler: () => {
                                    this.platform.exitApp();
                                }
                                },
                                {text: "Cancel", role: 'cancel'}
                            ]
                        });
                        alert.present();

                    } else
                        this.nav.pop()

                }

            });

            // design settings

            this.statusBar.overlaysWebView(false);
            this.statusBar.styleDefault();

            // orientation settings

            if (this.platform.is('cordova')) {
                this.screen.lock('portrait');
            }

            // launching

            this.auth.initialize().then(async () => {

                // if the user is logged in

                if (this.auth.getToken() !== '') {

                    await this.initialSetup();
                    this.init.onInitApp().then(() =>{
                            setTimeout(() => this.splashScreen.hide(), 100);
                            this.push.init();
                            this.rootPage = 'HomePage';
                    });

                } else {

                    // if it's the first launch

                    this.storage.get('entered').then(async (data) => {
                        if (data === null) {
                            setTimeout(() => this.splashScreen.hide(), 100);
                            this.rootPage = 'EnterPage';
                            this.initialSetup();
                            this.push.init();
                        }
                        else {
                            // if he is unauthorized
                            await this.initialSetup();
                            setTimeout(() => this.splashScreen.hide(), 100);
                            this.push.init();
                            this.rootPage = 'LoginPage';
                        }

                    });

                }

            })
        });
    }

    openPage(page) {
        this.nav.push(page.component);
    }

    async logout(): Promise<any> {

        this.push.logout();
        await this.auth.logout();
        this.nav.setRoot('LoginPage');

    }

    selectLanguage() {

        let alert = this.alertCtrl.create();
        alert.setTitle(this.translate.instant("POPUPS.SELECTLANGUAGE"));

        alert.addInput({type: 'radio', label: 'English', value: 'en', checked: this.language === 'en'});
        alert.addInput({type: 'radio', label: 'עברית', value: 'he', checked: this.language === 'he'});

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {
                this.utils.setLanguage(data).then(() => {
                    this.initialSetup();
                    this.init.onInitApp().then(() => {this.setLanguageOnServer(data);})
                });
            }
        });
        alert.present();

    }

    async setLanguageOnServer (data: string) {
        try {
            await this.restangular.one('users', this.auth.id).customGET('locale', {language: data}).toPromise();
        } catch (err){
            console.log(err);
        }
    }

    initialSetup() {

        return new Promise(resolve => {

            this.translate.get('MENU').subscribe(res => {

                this.titles = res;

                this.pages = [
                    {title: this.titles['HOMEPAGE'], component: 'HomePage', icon: 'home'},
                    {title: this.titles['COMPANYREGISTER'], component: 'CompanyRegisterPage', icon: 'create'},
                    {title: this.titles['PROFILE'], component: 'ProfilePage', icon: 'person'},
                    {title: this.titles['SOCIALNETWORK'], component: 'UsersPage', icon: 'contacts'},
                    {title: this.titles['FEED'], component: 'FeedPage', icon: 'logo-youtube'},
                    {title: this.titles['SHOP'], component: 'CouponsPage', icon: 'cart'},
                    {title: this.titles['MAP'], component: 'MapPage', icon: 'map'},
                    {title: this.titles['UPLOAD'], component: 'UploadPage', icon: 'cloud-upload'},
                    {title: this.titles['FAQ'], component: 'FaqPage', icon: 'help-circle'},
                    {title: this.titles['CONTACT'], component: 'ContactPage', icon: 'mail'},
                    {title: this.titles['NOTIFICATIONS'], component: 'NotificationsPage', icon: 'chatbubbles'},
                ];
                resolve();

            });

        })

    }

    watchVideo (entity: string){
        let modal = this.modalCtrl.create('FacebookPage', {entity: entity});
        modal.present();
    }

}
