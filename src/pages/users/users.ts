import {Component} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {AlertController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {UtilsProvider} from '../../providers/utils/utils';
import {TranslateService} from '@ngx-translate/core';
import {SocialSharing} from '@ionic-native/social-sharing';

@IonicPage()
@Component({
    selector: 'page-users',
    templateUrl: 'users.html',
})
export class UsersPage {

    videos: Array<any> = [];
    filteredVideos: Array<any> = [];
    search: string = '';
    pageNumber:number = 1;
    lastPage: boolean = false;
    user = this.init.user;
    comment: any = {};
    language: string;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                public alertCtrl: AlertController,
                public translate: TranslateService,
                public init: InitServiceProvider,
                public modalCtrl: ModalController,
                public social: SocialSharing,
                public utils: UtilsProvider){
        this.utils.language$.subscribe(data => this.language = data);
    }

    sendComment (video, comment) {

        if (this.comment.content !== ''){

            this.comment = this.restangular.restangularizeElement('', {content: comment, user_id: this.user.id, media_id: video.id}, 'comments');

            this.comment.save().subscribe(data =>{
                this.comment.user = this.user;
                video.comments.push(this.comment);
                this.comment[video.id] = '';
            });
        }
    }

    showMore (video){
        video.openedComments = 1;
    }

    // extractHeader (record) {
    //     if (record)
    //         return record.user;
    // }

    onInput(event) {

        if (event.target.value)
            this.filteredVideos = this.videos.filter(video => video.user.name.toLowerCase().indexOf(event.target.value.toLowerCase()) >= 0);
        else
            this.filteredVideos = this.videos;
    }

    onCancel(event) {

        return this.filteredVideos = this.videos;

    }

    goToUser(id) {

        let modal = this.modalCtrl.create('UserPage', {user_id: id});
        modal.present();

        modal.onWillDismiss(data => {

            if (data.length !== 0){

                for (let update of data){

                    for (let item of this.videos){

                        if (item.user.id === update.id){

                            item.user.followed = update.status;
                            // this.extractHeader(item);

                        }

                    }

                }

            }

        })

    }

    share (video) {
        let message = '';
        if (this.language === 'en'){
            message = video.user.name + ' uploaded video ' + video.title + ' at ' + video.region + ' via YTravel application';
        } else {
            message = video.user.name + ' צילם סרטון ' + video.title + ' ב' + video.region + ' דרך אפליקצית YTravel';
        }
        this.social.share(message, video.title, '', this.utils.baseUrl + 'video?id=' + video.id + '&type=user&lang=' + this.language);
    }

    goToFriends (id: number): void {

        this.navCtrl.push('FeedPage', {user_id: id});

    }

    follow(id: number): void {

        this.restangular.all('followers').post({recipient_id: id}).subscribe((data) => {

            this.init.following = data;

            for (let item of this.filteredVideos){

                if (item.user.id === id){

                    item.user.followed = 1;
                    // this.extractHeader(item);

                }

            }

            for (let item of this.videos){

                if (item.user.id === id){

                    item.user.followed = 1;
                    // this.extractHeader(item);

                }

            }

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ADDED"), buttons: ['OK']});
            alert.present();

        });

    }

    unfollow(id: number): void {

        this.restangular.one('followers', id).remove().subscribe((data) => {

            this.init.following = data;

            for (let item of this.filteredVideos){

                if (item.user.id === id){

                    item.user.followed = 0;
                    // this.extractHeader(item);

                }

            }

            for (let item of this.videos){

                if (item.user.id === id){

                    item.user.followed = 0;
                    // this.extractHeader(item);

                }

            }

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.DONE"), buttons: ['OK']});
            alert.present();

        });

    }

    async ionViewWillEnter() {
        try {
            let data = await this.restangular.all('followers').customGET('', {page: this.pageNumber}).toPromise();
            for (let item of data.videos){
                item.openedComments = 0;
            }
            this.videos = data.videos;
            this.filteredVideos = data.videos;
            this.pageNumber += 1;
            this.lastPage = data.info.current_page === data.info.last_page;
        } catch (err){
            console.log(err);
        }
    }

    async getMoreVideos (infiniteScroll) {
        try {
            let data = await this.restangular.all('followers').customGET('', {page: this.pageNumber}).toPromise();
            for (let item of data.videos){
                item.openedComments = 0;
                this.videos.push(item);
                this.filteredVideos.push(item);
            }
            this.pageNumber += 1;
            this.lastPage = data.info.current_page === data.info.last_page;
        } catch (err){
            console.log(err);
        } finally {
            infiniteScroll.complete();
        }
    }


}
