import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {UsersPage} from './users';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        UsersPage,
    ],
    imports: [
        IonicPageModule.forChild(UsersPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class UsersPageModule {
}
