import {Component} from '@angular/core';
import {NavController, ModalController, IonicPage, AlertController} from 'ionic-angular';
import {Restangular} from "ngx-restangular";
import {UtilsProvider} from "../../providers/utils/utils";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {TranslateService} from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {

    user: any = {avatar: '', videos: [], coupons: []};
    load = this.restangular.one('users', this.auth.id).get();

    constructor(public navCtrl: NavController,
                public restangular: Restangular,
                public utils: UtilsProvider,
                public translate: TranslateService,
                public auth: AuthServiceProvider,
                public init: InitServiceProvider,
                public modalCtrl: ModalController,
                public alertCtrl: AlertController) {}

    goToCoupon(coupon, used, state){

        if (used === 0){
            let modal = this.modalCtrl.create('CouponPage', {coupon: coupon, state: state});
            modal.present();
        }

    }

    showDeletePopup (id) {
        let alert = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.CONFIRMDELETE"),
            message: this.translate.instant("POPUPS.CONFIRMDELETE2"),
            buttons: [
                {text: 'Cancel', role: 'cancel',},
                {
                    text: 'OK',
                    handler: () => {this.deleteVideo(id);}
                }
            ]
        });
        alert.present();
    }

    async deleteVideo (id) {
        try {
            await this.restangular.one('files', id).remove().toPromise();
            this.alertCtrl.create({title: this.translate.instant("POPUPS.DELETED"), buttons: ['OK']}).present();
            this.restangular.one('users', this.auth.id).get().subscribe(data => {

                this.user = data;
                this.init.user = data;
                this.init.setPoints(data.points);

            });
        } catch (err){
            console.log(err);
        }
    }

    goToEdit () {
        this.navCtrl.push('EditProfilePage')
    }

    ionViewWillEnter () {

        this.load.subscribe(data => {

            this.user = data;
            this.init.user = data;

        });

    }

    ionViewDidLeave () {
        this.load.unsubscribe();
    }

}
