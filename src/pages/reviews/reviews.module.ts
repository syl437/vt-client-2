import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {ReviewsPage} from './reviews';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        ReviewsPage,
    ],
    imports: [
        IonicPageModule.forChild(ReviewsPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class ReviewsPageModule {
}
