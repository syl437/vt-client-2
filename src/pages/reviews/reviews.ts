import {Component} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {UtilsProvider} from '../../providers/utils/utils';
import {Restangular} from 'ngx-restangular';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {TranslateService} from '@ngx-translate/core';
import {SocialSharing} from '@ionic-native/social-sharing';

@IonicPage()
@Component({
    selector: 'page-reviews',
    templateUrl: 'reviews.html',
})
export class ReviewsPage {

    company_category_id: number = this.navParams.get('company_category_id');
    region_id: number = this.navParams.get('region_id');
    videos = [];
    user = this.init.user;
    pageNumber:number = 1;
    lastPage: boolean = false;
    language: string;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                public alertCtrl: AlertController,
                public translate: TranslateService,
                public init: InitServiceProvider,
                public modalCtrl: ModalController,
                public social: SocialSharing,
                public utils: UtilsProvider) {
        this.utils.language$.subscribe(data => this.language = data);
    }

    async ionViewWillEnter () {
        try {
            let data = await this.restangular
                .all('companies')
                .customGET('reviews', {
                    region_id: this.region_id,
                    company_category_id: this.company_category_id,
                    page: this.pageNumber})
                .toPromise();
            this.videos = data.user_reviews;
            this.pageNumber += 1;
            this.lastPage = data.info.current_page === data.info.last_page;
        } catch (err){
            console.log(err);
        }
    }

    async getMoreVideos (infiniteScroll) {
        try {
            let data = await this.restangular
                .all('companies')
                .customGET('reviews', {
                    region_id: this.region_id,
                    company_category_id: this.company_category_id,
                    page: this.pageNumber})
                .toPromise();
            for (let item of data.user_reviews){
                this.videos.push(item);
            }
            this.pageNumber += 1;
            this.lastPage = data.info.current_page === data.info.last_page;
        } catch (err){
            console.log(err);
        } finally {
            infiniteScroll.complete();
        }
    }

    share (video) {
        let message = '';
        if (this.language === 'en'){
            message = video.user.name + ' uploaded video ' + video.title + ' at ' + video.region + ' via YTravel application';
        } else {
            message = video.user.name + ' צילם סרטון ' + video.title + ' ב' + video.region + ' דרך אפליקצית YTravel';
        }
        this.social.share(message, video.title, '', this.utils.baseUrl + 'video?id=' + video.id + '&type=user&lang=' + this.language);
    }

    goToUser(id) {

        let modal = this.modalCtrl.create('UserPage', {user_id: id});
        modal.present();

        modal.onWillDismiss(data => {

            if (data.length !== 0){

                for (let update of data){

                    for (let item of this.videos){

                        if (item.user.id === update.id){

                            item.user.followed = update.status;

                        }

                    }

                }

            }

        })

    }

    follow(id: number): void {

        this.restangular.all('followers').post({recipient_id: id}).subscribe((data) => {

            this.init.following = data;

            for (let item of this.videos){

                if (item.user.id === id){

                    item.user.followed = 1;

                }

            }

            for (let item of this.videos){

                if (item.user.id === id){

                    item.user.followed = 1;

                }

            }

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ADDED"), buttons: ['OK']});
            alert.present();

        });

    }

    unfollow(id: number): void {

        this.restangular.one('followers', id).remove().subscribe((data) => {

            this.init.following = data;

            for (let item of this.videos){

                if (item.user.id === id){

                    item.user.followed = 0;

                }

            }

            for (let item of this.videos){

                if (item.user.id === id){

                    item.user.followed = 0;

                }

            }

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.DONE"), buttons: ['OK']});
            alert.present();

        });

    }

}
