import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {InitServiceProvider} from '../../providers/init-service/init-service';

@IonicPage()
@Component({
    selector: 'page-faq',
    templateUrl: 'faq.html',
})
export class FaqPage {

    faq;

    constructor(public navCtrl: NavController, public init: InitServiceProvider) {
        this.init.faq$.subscribe(faq => this.faq = faq);
    }

    openItem (item) {
        item.state = item.state === 'closed' ? 'expanded' : 'closed';
    }

    ionViewWillEnter () {
        for (let item of this.faq){
            item.state = 'closed';
        }
    }

}
