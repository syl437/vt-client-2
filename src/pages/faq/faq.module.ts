import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {FaqPage} from './faq';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        FaqPage,
    ],
    imports: [
        IonicPageModule.forChild(FaqPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class FaqPageModule {
}
