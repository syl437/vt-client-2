import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {Restangular} from "ngx-restangular";

@IonicPage()
@Component({
    selector: 'page-rate',
    templateUrl: 'rate.html',
})
export class RatePage {

    company = this.navParams.get('company');
    stars = [false, false, false, false, false];
    user = this.init.user;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public viewCtrl: ViewController,
                public init: InitServiceProvider,
                public restangular: Restangular) {
    }

    closeModal() {this.viewCtrl.dismiss();}

    rate (index) {
        for (var i = 0; i < this.stars.length; i++){
            if (i <= index){
                this.stars[i] = true;
            }
            else if (i > index && i < this.stars.length) {
                this.stars[i] = false;
            }
        }
    }

    sendRate () {

        let rate = {user_id: this.user.id, company_id: this.company.id, mark: 0};

        for (let star of this.stars){
            if (star === true)
                rate.mark += 1;
        }

        if (rate.mark > 0){

            this.restangular.all('rates').post(rate).subscribe(data => {
                this.viewCtrl.dismiss({company: data});
            });

        }

    }

}
