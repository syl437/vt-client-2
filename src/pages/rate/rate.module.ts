import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {RatePage} from './rate';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    declarations: [
        RatePage,
    ],
    imports: [
        IonicPageModule.forChild(RatePage),
        ComponentsModule,
        TranslateModule,
    ],
})
export class RatePageModule {
}
