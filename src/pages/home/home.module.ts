import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {HomePage} from './home';
import {DirectivesModule} from '../../directives/directives.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    declarations: [
        HomePage,
    ],
    imports: [
        IonicPageModule.forChild(HomePage),
        ComponentsModule,
        DirectivesModule,
        TranslateModule
    ],
})
export class HomePageModule {}
