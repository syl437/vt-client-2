import {Component, NgZone} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController} from 'ionic-angular';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {UtilsProvider} from '../../providers/utils/utils';
import {Storage} from '@ionic/storage';
import {TranslateService} from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    company_categories: Array<object>;
    regions: Array<object>;
    search: string;
    region: number = 0;
    category: number = 0;
    language: any;
    banners;
    banner = null;

    constructor(public navCtrl: NavController,
                public init: InitServiceProvider,
                public alertCtrl: AlertController,
                public modalCtrl: ModalController,
                public translate: TranslateService,
                public utils: UtilsProvider,
                public zone: NgZone,
                public storage: Storage) {
        this.zone.run(() => {
            this.utils.language$.subscribe(data => this.language = data);
        });

        this.init.company_categories$.subscribe(company_categories => this.company_categories = company_categories);
        this.init.regions$.subscribe(regions => this.regions = regions);
        this.init.banners$.subscribe(banners => {
            this.banners = banners;
            this.banner = this.banners && this.banners.length && this.banners[Math.floor(Math.random() * this.banners.length)];
        });
    }

    goToCompanies (company_category_id, region_id) {
console.log(company_category_id, region_id);
        if (company_category_id === 0){

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.PLEASECHOOSE"), buttons: ['OK']});
            alert.present();
            return;
        }

        this.navCtrl.push('CompaniesPage', {company_category_id: company_category_id, region_id: region_id});
        this.region = 0;
        this.category = 0;

    }

    goToCoupons () {
        this.navCtrl.push('CouponsPage', {company_id: 0});
    }

    goToMap() {
        if (this.init.coordinates.lat !== 0 && this.init.coordinates.lng !== 0){
            this.navCtrl.push('MapPage');
        } else {
            this.init.getLocation().subscribe(data => this.navCtrl.push('MapPage'));
        }
    }

    goToCode () {
        let modal = this.modalCtrl.create('FacebookPage', {entity: 'code'});
        modal.present();
        // modal.onDidDismiss(() => {});
    }

    goToUpload() {
        this.navCtrl.push('UploadPage');
    }

    goToNetwork () {
        this.navCtrl.push('UsersPage');
    }

    goToUser (id) {
        this.navCtrl.push('UserPage', {user_id: id});
    }

    goToBanner() {
        if (this.banner.type === 'explanation'){
            let modal = this.modalCtrl.create('FacebookPage', {entity: 'banner'});
            modal.present();
        } else {
            this.navCtrl.push('CompanyPage', {company_id: this.banner.company_id});
        }
    }

    ionViewWillEnter () {
        this.storage.get('explanation_displayed').then(data => {
            if (data == null){
                let modal = this.modalCtrl.create('FacebookPage', {entity: 'explanation'});
                modal.present();
                this.storage.set('explanation_displayed', true);
            }
        });
    }

}
