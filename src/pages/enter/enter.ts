import {Component} from '@angular/core';
import {IonicPage, AlertController, NavController, Platform, Events} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {TranslateService} from "@ngx-translate/core";
import {UtilsProvider} from "../../providers/utils/utils";

@IonicPage()
@Component({
    selector: 'page-enter',
    templateUrl: 'enter.html',
})
export class EnterPage {

    language: string;

    constructor(public navCtrl: NavController,
                public storage: Storage,
                public alertCtrl: AlertController,
                public translate: TranslateService,
                public utils: UtilsProvider,
                public events: Events,
                public platform: Platform) {
        this.utils.language$.subscribe(value => this.language = value);
    }

    selectLanguage() {

        let alert = this.alertCtrl.create();
        alert.setTitle(this.translate.instant("POPUPS.SELECTLANGUAGE"));

        alert.addInput({type: 'radio', label: 'English', value: 'en', checked: this.language === 'en'});
        alert.addInput({type: 'radio', label: 'עברית', value: 'he', checked: this.language === 'he'});

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {
                this.utils.setLanguage(data).then(() => {
                    this.events.publish('language:hebrew');
                })
            }});
        alert.present();

    }

    goToRegister () {
        this.storage.set('entered', 1).then(() => {
            this.navCtrl.setRoot('RegisterPage');
        })
    }
}
