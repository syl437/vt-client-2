import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {EnterPage} from './enter';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    declarations: [EnterPage,],
    imports: [IonicPageModule.forChild(EnterPage),
    TranslateModule
    ]
})
export class EnterPageModule {
}
