import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {EditProfilePage} from './edit-profile';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        EditProfilePage,
    ],
    imports: [
        IonicPageModule.forChild(EditProfilePage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class EditProfilePageModule {
}
