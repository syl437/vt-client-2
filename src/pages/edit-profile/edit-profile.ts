import {Component, NgZone} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Restangular} from "ngx-restangular";
import {Storage} from "@ionic/storage";
import {UtilsProvider} from "../../providers/utils/utils";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {TranslateService} from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-edit-profile',
    templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

    user: any = {avatar: '', videos: [], coupons: [], country: ''};
    passwords: object = {old_password: '', new_password: ''};
    localProgress: boolean = false;
    process: boolean = this.utils.uploadProcess;
    progress: number;
    newAvatar: any;
    load = this.restangular.one('users', this.auth.id).get();
    countries = [];
    language: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                public storage: Storage,
                public utils: UtilsProvider,
                public alertCtrl: AlertController,
                private zone: NgZone,
                public transfer: FileTransfer,
                public translate: TranslateService,
                public auth: AuthServiceProvider,
                public init: InitServiceProvider) {

        this.utils.progress$.subscribe(val => {

            this.zone.run(() => {this.progress = val;});

        });

        this.utils.language$.subscribe(data => this.language = data);

    }

    update () {

        console.log(this.user);

        if (this.user.name === '' || this.user.phone === '' || this.user.country === ''){

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK']});
            alert.present();

            return;

        }

        this.user.patch().subscribe(data => {

            this.user = data;
            console.log(data);

            this.setInit(data);

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.CHANGED"), buttons: ['OK']});
            alert.present();

        });

    }

    updatePassword () {

        if (!this.utils.isAnyFieldEmpty(this.passwords)){

            this.restangular.one('users', this.user.id).all('password').post(this.passwords).subscribe(data => {

                this.passwords = {old_password: '', new_password: ''};

                let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.CHANGED"), buttons: ['OK']});
                alert.present();

            });

        }



    }

    deleteAvatar () {

        for (let file of this.user.medias){

            if (file.pivot.tag === 'avatar'){

                this.restangular.one('files', file.id).remove().subscribe(data => {

                    console.log(data);

                    let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.DELETED"), buttons: ['OK']});
                    alert.present();

                    this.restangular.one('users', this.user.id).get().subscribe(data => {

                        this.user = data;
                        this.setInit(data);

                    });

                });

            }

        }

    }

    setAvatar(avatar: any) {

        console.log('Avatar received', avatar);
        this.newAvatar = avatar.url;

        this.localProgress = true;
        this.utils.setUploadProcess(true);

        const fileTransfer: FileTransferObject = this.transfer.create();

        let options: FileUploadOptions = {
            fileKey: 'avatar',
            mimeType: avatar.type,
            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
            headers: {Authorization: `Bearer ${this.auth.getToken()}`},
            params: {media_key: 'avatar', entity_type: 'avatar', 'entity_id': this.user.id}
        };

        fileTransfer.onProgress((event: ProgressEvent) => {

            if (event.lengthComputable) {

                let progress = Math.round((event.loaded / event.total) * 100);

                console.log('Progress', progress);

                this.utils.setProgress(progress);

            }

        });

        console.log(options);

        fileTransfer.upload(this.newAvatar, this.utils.baseUrl + 'api/v1/files', options)
            .then((data) => {

                console.log(data);

                let newData = JSON.parse(data.response);
                this.user = this.restangular.restangularizeElement('', newData.data, 'users');
                this.setInit(this.user);

                let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.UPLOADED"), buttons: ['OK']});
                alert.present();

                this.localProgress = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            }, (err) => {

                console.log(err);

                let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ERROR"), buttons: ['OK']});
                alert.present();

                this.localProgress = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            })
    }

    updateAvatar(avatar: any) {

        console.log('Avatar received', avatar);
        this.newAvatar = avatar.url;

        this.localProgress = true;
        this.utils.setUploadProcess(true);

        const fileTransfer: FileTransferObject = this.transfer.create();

        let avatar_id = 0;

        for (let file of this.user.medias){

            if (file.pivot.tag === 'avatar')
                avatar_id = file.id;

        }

        let options: FileUploadOptions = {
            fileKey: 'avatar',
            mimeType: avatar.type,
            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
            headers: {Authorization: `Bearer ${this.auth.getToken()}`},
            params: {media_key: 'avatar', entity_type: 'avatar', entity_id: this.user.id}
        };

        fileTransfer.onProgress((event: ProgressEvent) => {

            if (event.lengthComputable) {

                let progress = Math.round((event.loaded / event.total) * 100);

                console.log('Progress', progress);

                this.utils.setProgress(progress);

            }

        });

        console.log(options);

        fileTransfer.upload(this.newAvatar, this.utils.baseUrl + 'api/v1/files/' + avatar_id + '?_method=PATCH', options)
            .then((data) => {

                console.log(data);
                let newData = JSON.parse(data.response);
                this.user = this.restangular.restangularizeElement('', newData.data, 'users');
                this.setInit(this.user);

                let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.CHANGED"), buttons: ['OK']});
                alert.present();

                this.localProgress = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            }, (err) => {

                console.log(err);
                let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ERROR"), buttons: ['OK']});
                alert.present();

                this.localProgress = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            })
    }


    ionViewWillEnter () {

        this.load.subscribe(data => {

            this.restangular.all('init/countries').getList().subscribe(data => this.countries = data);

            this.user = data;
            this.init.user = data;
            this.setInit(data);

        });

    }

    setInit (data) {

        this.init.setPoints(data.points);
        this.init.setName(data.name);
        this.init.setAvatar(data.avatar);

    }
}
