import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {CouponPage} from './coupon';
import {TranslateModule} from '@ngx-translate/core';
import {QRCodeModule} from 'angular2-qrcode';

@NgModule({
    declarations: [
        CouponPage,
    ],
    imports: [
        IonicPageModule.forChild(CouponPage),
        ComponentsModule,
        TranslateModule,
        QRCodeModule
    ],
})
export class CouponPageModule {
}
