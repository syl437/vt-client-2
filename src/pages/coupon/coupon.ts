import {Component} from '@angular/core';
import {NavController, NavParams, ViewController, Platform, IonicPage} from 'ionic-angular';
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {UtilsProvider} from "../../providers/utils/utils";

@IonicPage()
@Component({
    selector: 'page-coupon',
    templateUrl: 'coupon.html',
})
export class CouponPage {

    state: string = this.navParams.get('state');
    coupon = {image: '', title: '', price: 0, qr: '', description: '', company: {logo: '', title: ''}};

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public init: InitServiceProvider,
                public viewCtrl: ViewController,
                public platform: Platform,
                public utils: UtilsProvider) {

        this.coupon = this.navParams.get('coupon');
        console.log(this.coupon);
    }

    closeModal () {this.viewCtrl.dismiss()}

}
