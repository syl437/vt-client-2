import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {UploadPage} from './upload';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        UploadPage,
    ],
    imports: [
        IonicPageModule.forChild(UploadPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class UploadPageModule {
}
