import {Component, NgZone} from '@angular/core';
import {AlertController, IonicPage, LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {Restangular} from "ngx-restangular";
import {UtilsProvider} from "../../providers/utils/utils";
import {MapsAPILoader} from "@agm/core";
import {File} from '@ionic-native/file';
import {TranslateService} from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-upload',
    templateUrl: 'upload.html',
})
export class UploadPage {

    video: any;
    user_id: number = this.auth.id;
    src: string;
    title: string = '';
    place: string = '';
    company_id: number = this.navParams.get('company_id') || 0;
    company_categories: Array<any>;
    regions: Array<any>;
    category_id: number = 0;
    subcategory_id: number = 0;
    region_id: number = 0;
    address: any;
    formatted_address: Array<any>;
    companies: Array<any> = [];
    company: number = 0;
    process: boolean = this.utils.uploadProcess;
    progress: number;
    fileTransfer: FileTransferObject = this.transfer.create();
    language: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private transfer: FileTransfer,
                private alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public auth: AuthServiceProvider,
                public init: InitServiceProvider,
                public restangular: Restangular,
                public utils: UtilsProvider,
                public loader: MapsAPILoader,
                private zone: NgZone,
                public file: File,
                public translate: TranslateService,
                public modalCtrl: ModalController) {

        this.utils.progress$.subscribe(val => {
            this.zone.run(() => {this.progress = val;});
        });
        this.utils.language$.subscribe(data => this.language = data);

        this.init.company_categories$.subscribe(company_categories => this.company_categories = company_categories);
        this.init.regions$.subscribe(regions => this.regions = regions);
    }

    goToExplanation () {
        let modal = this.modalCtrl.create('FacebookPage', {entity: 'explanation_video'});
        modal.present();
    }

    getCompanies (entity) {

        if (entity === 'category'){
            this.subcategory_id = 0;
            this.companies = [];
            this.company = 0;
        }

        if (this.subcategory_id !== 0){

            this.companies = [];
            this.company = 0;

            let loading = this.loadingCtrl.create({
                content: 'Please wait...',
                spinner: 'ios',
            });

            loading.present();

            this.restangular.all('companies/sorted').customPOST({company_subcategory_id: this.subcategory_id, region_id: this.region_id}).subscribe(data => {

                loading.dismiss();
                this.companies = data;
                this.loadMaps();

            });

        }

    }

    async setVideo(video: any) {
        console.log('Video received', video);
        let loading = this.loadingCtrl.create({
            content: 'Please wait while we are checking the file...',
            spinner: 'ios',
        });
        loading.present();
        try {
            console.log(video.fullPath.startsWith('file'));
            let path = video.fullPath.startsWith('file') ? video.fullPath : 'file://' + video.fullPath;
            console.log(path);
            const fileUrl = await this.file.resolveLocalFilesystemUrl(path);
            fileUrl.getMetadata(metadata => {
                loading.dismiss();
                if (metadata.size >= 100000000){
                    this.alertCtrl.create({title: this.translate.instant("POPUPS.TOOBIG"), message: this.translate.instant("POPUPS.TOOBIG2"), buttons: ['OK']}).present();
                    return;
                } else {
                    this.video = video;
                    this.src = video.fullPath;
                }
            });
        } catch (error) {
            loading.dismiss();
            console.log(error);
            this.alertCtrl.create({title: "Problem with video file!", message: "Please try another one", buttons: ['OK']}).present();
        }
    }

    collect() {
        console.log("Collect" , typeof this.video , this.subcategory_id , this.category_id , this.region_id , this.title  , this.place , this.company)
        // console.log(this.title, this.region_id, this.category_id, this.subcategory_id, this.company, this.place);

        if (this.title === '' || this.region_id === 0 || this.category_id === 0 || this.subcategory_id === 0 || typeof this.video === 'undefined'){
            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK']});
            alert.present();
            return;

        }

        if (this.company === 0 && this.region_id !== 0 && this.companies.length != 0){
            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK']});
            alert.present();
            return;

        }
        console.log("Collect1")
        if (this.company === -1 && this.place === ''){
            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK']});
            alert.present();
            return;
        }

        this.process = true;
        this.utils.setUploadProcess(true);
        console.log("Collec2")
        let params:any = {};

        params.entity_type = 'user';
        params.entity_id = this.auth.id;
        params.company_id = this.company;
        params.media_key = 'video';
        params.title = String(this.title);
        console.log('params', this.company);
        if (this.company === -1 || this.company === 0){

            params.address = this.formatted_address[0];
            params.country = this.formatted_address[1];
            params.lat = this.formatted_address[2];
            params.lng = this.formatted_address[3];
            params.company_subcategory_id = this.subcategory_id;
            params.region_id = this.region_id;
            params.place = this.place;

            console.log(JSON.stringify(params));
            this.send(params);

        } else
            this.send(params);

    }

    check() {
        console.log("Check")
        if (typeof this.video === 'undefined' || this.title === ''){
            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK']});
            alert.present();
            return;
        }

        this.process = true;
        this.utils.setUploadProcess(true);

        let params:any = {};

        params.entity_type = 'user';
        params.entity_id = this.user_id;
        params.company_id = this.company_id;
        params.media_key = 'video';
        params.title = this.title;

        this.send(params);

    }

    send(params){
    
        //params {"entity_type":"user","entity_id":14,"company_id":0,"media_key":"video","title":"ויי","address":"אבני איתן","country":"","lat":32.824808,"lng":35.76577700000007,"company_subcategory_id":1,"region_id":1,"place":"אבני"}
        //params {"entity_type":"user","entity_id":14,"company_id":0,"media_key":"video","title":"ויי","address":"אבני חושן, מודיעין מכבים רעות, ישראל","country":"IL","lat":31.9032339,"lng":34.994774699999994,"company_subcategory_id":1,"region_id":1,"place":"אבני"}
        //{bytesSent: 199450, responseCode: 200, response: "{"status":200,"success":true,"data":null}", objectId: ""}
        let that = this;

        console.log('params', JSON.stringify(params));

        // const fileTransfer: FileTransferObject = this.transfer.create();

        let options: FileUploadOptions = {
            fileKey: 'video',
            fileName: this.video.name,
            mimeType: this.video.type,
            headers: {Authorization: `Bearer ${this.auth.getToken()}`},
            params: params
        };

        this.fileTransfer.onProgress((event: ProgressEvent) => {

            if (event.lengthComputable) {

                let progress = Math.round((event.loaded / event.total) * 100);

                console.log('Progress', progress);

                that.utils.setProgress(progress);

            }

        });
console.log(that.utils);
            this.fileTransfer.upload(this.video.fullPath, that.utils.baseUrl + 'api/v1/files', options)
            .then((data) => {

                console.log(data);

                let modal = this.modalCtrl.create('FacebookPage', {entity: '3dollars'});
                modal.present();

                this.video = null;
                this.src = null;
                this.title = '';
                this.place = '';
                this.category_id = 0;
                this.subcategory_id = 0;
                this.region_id = 0;
                this.companies = [];
                this.company = 0;

                this.process = false;
                that.utils.setUploadProcess(false);
                that.utils.setProgress(0);

            }, (err) => {

                console.log(JSON.stringify(err));
                // loading.dismiss();

                this.process = false;
                that.utils.setUploadProcess(false);

                that.utils.setProgress(0);

                if (err.code === 4){
                    let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ABORTED"), buttons: ['OK']});
                    alert.present();
                } else {
                    let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ERROR"), buttons: ['OK']});
                    alert.present();
                }

            });

    }

    abort () {
        this.fileTransfer.abort();
    }

    loadMaps () {

        console.log('loadMaps', this.companies.length, this.subcategory_id, this.company);

        if ((this.companies.length == 0 && this.subcategory_id != 0) || this.company == -1){

            this.loader.load().then(() => {

                let address = document.getElementById('address').getElementsByTagName('input')[0];
                let autocomplete = new google.maps.places.Autocomplete(address);

                autocomplete.addListener("place_changed", () => {

                    let place = autocomplete.getPlace();
                    console.log("pl : " , place);
                    let countryComponent = '';

                    for (let item of place.address_components){

                        if (item.types[0] === 'country')
                            countryComponent = item.short_name;

                    }
                    
                    //shay added
                    if(countryComponent == "")
                        countryComponent =  "IL";
                    
                    this.formatted_address = [place.formatted_address, countryComponent, place.geometry.location.lat(), place.geometry.location.lng()];
                    console.log(this.formatted_address);

                });

            })

        }

    }

}
