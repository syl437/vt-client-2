import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Restangular} from "ngx-restangular";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {SocialSharing} from '@ionic-native/social-sharing';
import {UtilsProvider} from '../../providers/utils/utils';

@IonicPage()
@Component({
    selector: 'page-user',
    templateUrl: 'user.html',
})
export class UserPage {

    user_id: number = this.navParams.get('user_id') || this.auth.id;
    user: any = {avatar: '', videos: [], coupons: []};
    load = this.restangular.one('users', this.user_id).get();
    updates = [];
    language: string;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                public auth: AuthServiceProvider,
                public init: InitServiceProvider,
                public social: SocialSharing,
                public utils: UtilsProvider,
                public viewCtrl: ViewController) {
        this.utils.language$.subscribe(data => this.language = data);
    }

    closeModal () {this.viewCtrl.dismiss(this.updates);}

    share (video) {
        console.log(video);
        let message = '';
        if (this.language === 'en'){
            message = this.user.name + ' uploaded video ' + video.pivot.tag + ' at ' + video.region + ' via YTravel application';
        } else {
            message = this.user.name + ' צילם סרטון ' + video.pivot.tag + ' ב' + video.region + ' דרך אפליקצית YTravel';
        }
        this.social.share(message, video.title, '', this.utils.baseUrl + 'video?id=' + video.id + '&type=user&lang=' + this.language);
    }

    goTo() {

        if (this.user.id === this.user_id){
            this.navCtrl.push('ProfilePage');
        } else {
            this.navCtrl.push(UserPage);
        }
    }


    follow (id: number): void {

        this.restangular.all('followers').post({recipient_id: id}).subscribe((data) => {

            this.init.following = data;
            this.user.followed = 1;
            this.updates.push({id: this.user.id, status: 1});

        });

    }

    unfollow (id: number): void {

        this.restangular.one('followers', id).remove().subscribe((data) => {

            this.init.following = data;
            this.user.followed = 0;
            this.updates.push({id: this.user.id, status: 0});

        });

    }

    ionViewWillEnter () {

        this.load.subscribe(data => {

            this.user = data;
            this.user_id = data.id;

        });

    }

    ionViewDidLeave () {
        this.load.unsubscribe();
    }

}
