import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {UserPage} from './user';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        UserPage,
    ],
    imports: [
        IonicPageModule.forChild(UserPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class UserPageModule {
}
