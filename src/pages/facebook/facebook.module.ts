import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {FacebookPage} from './facebook';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        FacebookPage,
    ],
    imports: [
        IonicPageModule.forChild(FacebookPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class FacebookPageModule {
}
