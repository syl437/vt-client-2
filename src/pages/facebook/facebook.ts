import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {SocialSharing} from "@ionic-native/social-sharing";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {Restangular} from "ngx-restangular";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {UtilsProvider} from "../../providers/utils/utils";
import {DomSanitizer} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {Facebook} from '@ionic-native/facebook';

@IonicPage()
@Component({
    selector: 'page-facebook',
    templateUrl: 'facebook.html',
})

export class FacebookPage {

    company_id: number = this.navParams.get('company_id');
    company: any;
    entity: string = this.navParams.get('entity');
    code: string = '';
    reward: number;
    userReward: number;
    code_id: number;
    language: any;
    param: any;
    youtubeVideos = {
        explanation: {url: 'https://www.youtube.com/embed/693htHBw6S0', sanitizedUrl: this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/693htHBw6S0')},
        banner: {url: 'https://www.youtube.com/embed/5Q6Z5ZzYmNc', sanitizedUrl: this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/5Q6Z5ZzYmNc')},
        explanation_video: {url: 'https://www.youtube.com/embed/lDBwPu_wy3o', sanitizedUrl: this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/lDBwPu_wy3o')},
    };
    points: number;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public viewCtrl: ViewController,
                public social: SocialSharing,
                public init: InitServiceProvider,
                public alertCtrl: AlertController,
                public translate: TranslateService,
                public restangular: Restangular,
                public auth: AuthServiceProvider,
                private domSanitizer: DomSanitizer,
                public fb: Facebook,
                public utils: UtilsProvider) {

        this.init.points$.subscribe(value => this.points = value);

        this.utils.language$.subscribe(data => this.language = data);
    }

    // explanation

    showExplanationVideo () {
        this.entity = 'explanation';
    }

    closeModalForVideo () {
        if (this.entity === 'explanation' || this.entity === 'banner' || this.entity === 'explanation_video'){
            this.viewCtrl.dismiss();
        }
    }

    // all

    closeModal () {this.viewCtrl.dismiss();}

    // coupon

    share() {
        this.social.shareViaFacebook('', '', this.utils.baseUrl + 'video?id=' + this.company.id + '&type=company&lang=' + this.language)
            .then(data => console.log(data))
    }

    // code

    validateCode () {

        if (this.code === ''){

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ENTERCODE"), buttons: ['OK']});
            alert.present();

            return;

        }

        this.restangular.all('codes/validate').post({code: this.code}).subscribe(data => {

            this.param = {value: data.reward};
            this.reward = data.reward;
            this.code_id = data.id;
            this.company = data.company;
            this.entity = 'facebook';
            this.code = '';

        });

    }

    // facebook

    close() {

        let confirm = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.SURE") + "?",
            message: this.translate.instant("POPUPS.IFCLOSE"),
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {}
                },
                {
                    text: 'OK',
                    handler: () => {this.viewCtrl.dismiss();}
                }
            ]
        });

        confirm.present();

    }

    shareCode () {

        this.social.shareViaFacebook('', '', this.utils.baseUrl + 'video?id=' + this.company.id + '&type=company&lang=' + this.language).then(data => {

            console.log(data);

            // TODO: https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin/issues/864

            if (data === 'OK'){

                this.restangular.one('codes', this.code_id).one('users', this.auth.id).customGET('bind')
                    .subscribe(response => {

                        setTimeout(() => {

                            this.userReward = response.reward;
                            this.entity = 'code_succeed';
                            this.init.setPoints(this.points + response.reward);

                        }, 7000);

                    });

            }

        }).catch(err => {

            console.log(err);
            let facebookExists = true;

            if (Array.isArray(err)){
                facebookExists = (<Array<string>>err).indexOf("com.facebook.katana") > -1;
            } else if (err === 'cancelled'){
                facebookExists = false;
            }

            if (!facebookExists){
                let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.NOFACEBOOK"), buttons: ['OK']});
                alert.present();
                return;
            }

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.WRONG"), buttons: ['OK']});
            alert.present();
            return;

        });

    }

    goToMap(){
        if (this.init.coordinates.lat !== 0 && this.init.coordinates.lng !== 0){
            this.navCtrl.push('MapPage');
        } else {
            this.init.getLocation().subscribe(data => this.navCtrl.push('MapPage'));
        }
    }

    goToShop(){
        this.navCtrl.push('CouponsPage', {company_id: 0});
    }

    ionViewWillEnter () {

        if (this.entity === 'coupon') {
            this.company = this.navParams.get('company');
        }
    }

}
