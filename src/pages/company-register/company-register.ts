import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {Storage} from "@ionic/storage";
import {UtilsProvider} from "../../providers/utils/utils";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {MapsAPILoader} from "@agm/core";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import { google } from "google-maps";


@IonicPage()
@Component({
    selector: 'page-company-register',
    templateUrl: 'company-register.html',
})
export class CompanyRegisterPage {

    company_categories: Array<any> = [];
    regions: Array<any>;
    formatted_address: Array<any>;
    company = this.restangular.restangularizeElement('', {email: '', password: '', title_en: '', title_he: '', phone: '',
        address: '', description_he: '', description_en: '', company_category_id: '', company_subcategory_id: '', region_id: ''}, 'companies');
    language: any;

    constructor(public navCtrl: NavController,
                private restangular: Restangular,
                private storage: Storage,
                private utils: UtilsProvider,
                public init: InitServiceProvider,
                public loader: MapsAPILoader,
                public auth: AuthServiceProvider) {

        this.utils.language$.subscribe(data => this.language = data);
        this.init.regions$.subscribe(regions => this.regions = regions);
        this.init.company_categories$.subscribe(company_categories => {
            this.company_categories = company_categories.slice(1);
        });
    }

    submit () {

        if (!this.utils.isAnyFieldEmpty(this.company.plain())){

            this.storage.get('id').then(val => {

                this.company.address = this.formatted_address[0];
                this.company.country = this.formatted_address[1];
                this.company.lat = this.formatted_address[2];
                this.company.lng = this.formatted_address[3];

                console.log(this.company.plain());

                this.company.save().subscribe(data => {

                    this.navCtrl.setRoot('CompanyRegisterAdditionalPage', {company_id: data.id});

                })
            });

        }

    }

    ionViewDidEnter() {

        this.loader.load().then(() => {

            let address = document.getElementById('company_address').getElementsByTagName('input')[0];
            let autocomplete = new google.maps.places.Autocomplete(address);

            autocomplete.addListener("place_changed", () => {

                let place = autocomplete.getPlace();
                console.log(place);
                let countryComponent = '';

                for (let item of place.address_components){

                    if (item.types[0] === 'country')
                        countryComponent = item.short_name;

                }

                this.formatted_address = [place.formatted_address, countryComponent, place.geometry.location.lat(), place.geometry.location.lng()];
                console.log(this.formatted_address);

            });

        })

    }

}
