import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {CompanyRegisterPage} from './company-register';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    declarations: [
        CompanyRegisterPage,
    ],
    imports: [
        IonicPageModule.forChild(CompanyRegisterPage),
        ComponentsModule,
        TranslateModule
    ],
})
export class CompanyRegisterPageModule {
}
