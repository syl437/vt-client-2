import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {LeadPage} from './lead';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    declarations: [
        LeadPage,
    ],
    imports: [
        IonicPageModule.forChild(LeadPage),
        ComponentsModule,
        TranslateModule,
    ],
})
export class LeadPageModule {
}
