import {Component} from '@angular/core';
import {Restangular} from "ngx-restangular";
import {AlertController, IonicPage, NavParams} from 'ionic-angular';
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {UtilsProvider} from "../../providers/utils/utils";
import {TranslateService} from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-lead',
    templateUrl: 'lead.html',
})

export class LeadPage {

    is_hotel = this.navParams.get('is_hotel');
    company_id = this.navParams.get('company_id');
    lead = this.restangular.restangularizeElement('', {quantity: '', start: '', end: '', comments: ''}, 'leads');
    language:any;

    constructor(public navParams: NavParams,
                public restangular: Restangular,
                public alertCtrl: AlertController,
                public auth: AuthServiceProvider,
                public translate: TranslateService,
                public utils: UtilsProvider) {

        this.utils.language$.subscribe(data => this.language = data);

    }

    send(): void {

        console.log(this.lead.plain());

        if (this.lead.quantity === '' || this.lead.start === ''){

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK']});
            alert.present();
            return;

        }

        this.lead.user_id = this.auth.id;
        this.lead.company_id = this.company_id;

        this.lead.save().subscribe(() => {

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.REQUESTRECEIVED"), buttons: ['OK']});
            alert.present();

            this.lead = this.restangular.restangularizeElement('', {quantity: '', start: '', end: '', comments: ''}, 'leads');

        });

    }

}
