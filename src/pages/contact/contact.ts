import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UtilsProvider} from '../../providers/utils/utils';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {TranslateService} from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-contact',
    templateUrl: 'contact.html',
})
export class ContactPage {

    language: string;
    dir: string;
    form: FormGroup = this.fb.group({
        title: ['', Validators.required],
        content: ['', Validators.required],
    });

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public fb: FormBuilder,
                public restangular: Restangular,
                public alertCtrl: AlertController,
                public translate: TranslateService,
                public utils: UtilsProvider) {
        this.utils.language$.subscribe(data => {
            this.language = data;
            this.dir = data === 'he' ? 'rtl' : 'ltr';
        });
    }

    async submit() {
        if (this.form.invalid){
            return;
        }

        try {
            await this.restangular.all('messages').customPOST({title: this.form.value.title, content: this.form.value.content}).toPromise();
            this.alertCtrl.create({title: this.translate.instant("POPUPS.CONTACT"), buttons: ['OK']}).present();
            this.navCtrl.setRoot('HomePage');
            this.form.reset();
        } catch (err){
            console.log(err);
        }
    }

    ionViewDidLeave() {
        this.form.reset();
    }

}
