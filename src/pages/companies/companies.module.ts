import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CompaniesPage} from './companies';
import {ComponentsModule} from '../../components/components.module';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        CompaniesPage,
    ],
    imports: [
        IonicPageModule.forChild(CompaniesPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class CompaniesPageModule {
}
