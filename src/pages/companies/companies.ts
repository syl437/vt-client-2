import {Component, NgZone} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {UtilsProvider} from '../../providers/utils/utils';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';

@IonicPage()
@Component({
    selector: 'page-companies',
    templateUrl: 'companies.html',
})

export class CompaniesPage {

    company_category_id: number = this.navParams.get('company_category_id');
    company_subcategory_id: number = 0;
    region_id: number = this.navParams.get('region_id');
    companies: any = [];
    company_categories: Array<object> = [];
    regions: Array<object> = [];
    filteredCompanies: any = [];
    language: any;
    user = this.init.user;
    load = this.restangular.all('companies').customGET('', {region_id: this.region_id, company_category_id: this.company_category_id});

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                public init: InitServiceProvider,
                public utils: UtilsProvider,
                public zone: NgZone,
                public auth: AuthServiceProvider,
                public modalCtrl: ModalController) {

        this.init.company_categories$.subscribe(company_categories => this.company_categories = company_categories);
        this.utils.language$.subscribe(data => this.language = data);
        this.init.regions$.subscribe(regions => this.regions = regions);

    }

    filterCompanies () {

        if (this.company_subcategory_id === 0 && this.region_id === 0){
            return this.filteredCompanies = this.companies;
        }

        if (this.region_id === 0){
            return this.filteredCompanies = this.companies.filter(company => company.company_subcategory_id === this.company_subcategory_id);
        }

        if (this.company_subcategory_id === 0){
            return this.filteredCompanies = this.companies.filter(company => company.region_id === this.region_id);
        }

        return this.filteredCompanies = this.companies.filter(company => company.company_subcategory_id === this.company_subcategory_id && company.region_id === this.region_id);

    }

    goToCompany(id) {
        this.navCtrl.push('CompanyPage', {company_id: id})
    }

    goToUserReviews () {
        this.navCtrl.push('ReviewsPage', {company_category_id: this.company_category_id, region_id: this.region_id});
    }

    goToUser(id) {

        let modal = this.modalCtrl.create('UserPage', {user_id: id});
        modal.present();

    }

    ngOnInit () {
        this.load.subscribe(data => {
            console.log('ngOnInit', data);
            this.companies = data;
            this.filteredCompanies = data;
        });
    }

    ionViewDidLeave () {
        this.load.unsubscribe();
    }

}
