import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';

@IonicPage()
@Component({
    selector: 'page-notifications',
    templateUrl: 'notifications.html',
})
export class NotificationsPage {

    notifications: Array<any> = [];

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular) {}

    async ionViewWillEnter () {
        try {
            this.notifications = await this.restangular.all('notifications').getList().toPromise();
        } catch (err) {
            console.log(err);
        }
    }


}
