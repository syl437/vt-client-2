import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {SocialSharing} from '@ionic-native/social-sharing';
import {UtilsProvider} from '../../providers/utils/utils';

@IonicPage()
@Component({
    selector: 'page-feed',
    templateUrl: 'feed.html',
})
export class FeedPage {

    videos: Array<any> = [];
    user_id = this.navParams.get('user_id') || this.auth.id;
    user = this.init.user;
    pageNumber: number = 1;
    lastPage: boolean = false;
    comment: any = {};
    language: string;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                public auth: AuthServiceProvider,
                public init: InitServiceProvider,
                public social: SocialSharing,
                public utils: UtilsProvider,
                public modalCtrl: ModalController) {
        this.utils.language$.subscribe(data => this.language = data);
    }

    share (video) {
        let message = '';
        if (this.language === 'en'){
            message = video.user.name + ' uploaded video ' + video.title + ' at ' + video.region + ' via YTravel application';
        } else {
            message = video.user.name + ' צילם סרטון ' + video.title + ' ב' + video.region + ' דרך אפליקצית YTravel';
        }
        this.social.share(message, video.title, '', this.utils.baseUrl + 'video?id=' + video.id + '&type=user&lang=' + this.language);
    }

    sendComment (video, comment) {

        if (this.comment.content !== ''){

            this.comment = this.restangular.restangularizeElement('', {content: comment, user_id: this.user.id, media_id: video.id}, 'comments');

            this.comment.save().subscribe(data =>{

                this.comment.user = this.user;
                video.comments.push(this.comment);
                this.comment = {};
                this.comment[video.id] = '';

            });
        }
    }

    // extractHeader (record) {
    //     if (record)
    //         return record.user;
    // }

    unfollow (id: number): void {

        this.restangular.one('followers', id).remove().subscribe((data) => {

            this.init.following = data;

            for (let item of this.videos){

                if (item.user.id === id){

                    item.user.followed = 0;
                    // this.extractHeader(item);

                }

            }

        });

    }

    follow (id: number): void {

        this.restangular.all('followers').post({recipient_id: id}).subscribe((data) => {

            this.init.following = data;

            for (let item of this.videos){

                if (item.user.id === id){

                    item.user.followed = 1;
                    // this.extractHeader(item);

                }

            }

        });

    }

    goToUser(id) {

        let modal = this.modalCtrl.create('UserPage', {user_id: id});
        modal.present();

        modal.onWillDismiss(data => {

            if (data.length !== 0){

                for (let update of data){

                    for (let item of this.videos){

                        if (item.user.id === update.id){

                            item.user.followed = update.status;
                            // this.extractHeader(item);

                        }

                    }

                }

            }

        })

    }

    async ionViewWillEnter () {
        try {
            let data = await this.restangular.one('followers', this.user_id).customGET('', {page: this.pageNumber}).toPromise();
            this.videos = data.videos;
            this.pageNumber += 1;
            this.lastPage = data.info.current_page === data.info.last_page;
        } catch (err) {
            console.log(err);
        }
    }

    async getMoreVideos (infiniteScroll) {
        try {
            let data = await this.restangular.one('followers', this.user_id).customGET('', {page: this.pageNumber}).toPromise();
            for (let item of data.videos){
                this.videos.push(item);
            }
            this.pageNumber += 1;
            this.lastPage = data.info.current_page === data.info.last_page;
        } catch (err){
            console.log(err);
        } finally {
            infiniteScroll.complete();
        }
    }

}
