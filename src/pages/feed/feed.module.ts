import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {FeedPage} from './feed';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        FeedPage,
    ],
    imports: [
        IonicPageModule.forChild(FeedPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class FeedPageModule {
}
