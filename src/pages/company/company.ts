import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {Restangular} from "ngx-restangular";
import {CallNumber} from '@ionic-native/call-number';
import {LaunchNavigator, LaunchNavigatorOptions} from '@ionic-native/launch-navigator';
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {SocialSharing} from "@ionic-native/social-sharing";
import 'rxjs/add/operator/takeUntil';
import {UtilsProvider} from '../../providers/utils/utils';

@IonicPage()
@Component({
    selector: 'page-company',
    templateUrl: 'company.html',
})
export class CompanyPage {

    company_id: number = this.navParams.get('company_id') || 4;
    company: any = {logo: null, main_video: null, videos: []};
    coordinates = this.init.coordinates;
    load = this.restangular.one('companies', this.company_id).get();
    openedDescription: boolean = false;
    language: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                private callNumber: CallNumber,
                private launchNavigator: LaunchNavigator,
                public init: InitServiceProvider,
                public utils: UtilsProvider,
                public social: SocialSharing,
                public modalCtrl: ModalController) {
        this.utils.language$.subscribe(data => this.language = data);
    }

    showRatePopup() {

        this.restangular.one('users', this.init.user.id).one('companies', this.company.id).customGET('check').subscribe(data => {

            let modal = this.modalCtrl.create('RatePage', {company: this.company});
            modal.present();

            modal.onWillDismiss(data => {

                if (data != null){

                    this.company.mark = data.company.mark;
                    this.company.stars = data.company.stars;

                }

            })

        });

    }

    call() {
        if (this.company.company_category_id === 1)
            this.callNumber.callNumber(this.init.phone, false);
        else
            this.callNumber.callNumber(this.company.phone, false);
    }

    navigate() {
        let options: LaunchNavigatorOptions = {
            start: [this.coordinates.lat, this.coordinates.lng],
            app: this.launchNavigator.APP.USER_SELECT
        };
        this.launchNavigator.navigate([this.company.lat, this.company.lng], options);
    }

    goToCoupons() {
        this.navCtrl.push('CouponsPage', {company_id: this.company_id});
    }

    goToUpload() {
        this.navCtrl.push('UploadPage', {company_id: this.company_id});
    }

    goToLead () {
        let is_hotel = this.company.company_category_id === 1 ? 1 : 0;
        this.navCtrl.push('LeadPage', {company_id: this.company_id, is_hotel: is_hotel});
    }

    shareVideo () {
        this.social.share(this.company.facebook, this.company.title, '', this.utils.baseUrl + 'video?id=' + this.company.id + '&type=company&lang=' + this.language);
    }

    ngOnInit() {
        this.load.subscribe(data => this.company = data);
    }

    ionViewDidLeave () {
        this.load.unsubscribe();
    }

}
