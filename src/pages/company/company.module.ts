import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CompanyPage} from './company';
import {ComponentsModule} from '../../components/components.module';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';
import {PipesModule} from '../../pipes/pipes.module';

@NgModule({
    declarations: [
        CompanyPage,
    ],
    imports: [
        IonicPageModule.forChild(CompanyPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule,
        PipesModule
    ],
})
export class CompanyPageModule {
}
