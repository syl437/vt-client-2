import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {MapPage} from './map';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';
import {AgmCoreModule} from '@agm/core';

@NgModule({
    declarations: [
        MapPage,
    ],
    imports: [
        IonicPageModule.forChild(MapPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule,
        AgmCoreModule
    ],
})
export class MapPageModule {
}
