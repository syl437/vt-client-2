import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController} from 'ionic-angular';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {UtilsProvider} from '../../providers/utils/utils';
import {TranslateService} from '@ngx-translate/core';
import {Restangular} from 'ngx-restangular';

@IonicPage()
@Component({
    selector: 'page-map',
    templateUrl: 'map.html',
})


export class MapPage {

    companies: Array<any> = [];
    filteredCompanies: Array<any> = [];
    coordinates: object = this.init.coordinates;
    zoom: number = 16;
    popup: string;
    state: string = 'business';

    constructor(public navCtrl: NavController,
                public init: InitServiceProvider,
                public restangular: Restangular,
                public alertCtrl: AlertController,
                public translate: TranslateService,
                public utils: UtilsProvider) {
        this.filterCompanies();
    }

    async ionViewWillEnter () {
        try {
            this.companies = await this.restangular.all('companies/sorted').customPOST().toPromise();
            this.filterCompanies();
        } catch (err){
            console.log(err);
        }
    }

    filterCompanies () {

        if (this.state === 'business'){
            return this.filteredCompanies = this.companies.filter(company => company.from_user === 0);
        }

        if (this.state === 'place'){
            return this.filteredCompanies = this.companies.filter(company => company.from_user === 1);
        }


        return this.filteredCompanies = this.companies;

    }

    clickedMarker (company){
        let alert = this.alertCtrl.create({
            title: company.title,
            subTitle: this.translate.instant("POPUPS.WHERE"),
            buttons: [
                {text: this.translate.instant("POPUPS.PROFILE"), handler: () => {this.navCtrl.push('CompanyPage', {company_id: company.id})}},
                {text: this.translate.instant("POPUPS.SHOP"), handler: () => {this.navCtrl.push('CouponsPage', {company_id: company.id});}}
            ]
        });
        alert.present();
    }


}
