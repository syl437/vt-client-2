import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {Restangular} from "ngx-restangular";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {UtilsProvider} from "../../providers/utils/utils";

@IonicPage()
@Component({
    selector: 'page-coupons',
    templateUrl: 'coupons.html',
})
export class CouponsPage {

    coupons: any;
    companies: Array<any> = [];
    company_id: number = this.navParams.get('company_id') || 0;
    company_categories: Array<any>;
    regions: Array<object>;
    region:number = 0;
    category: number = 0;
    filteredCompanies: Array<any> = [];

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                public init: InitServiceProvider,
                public auth: AuthServiceProvider,
                public modalCtrl: ModalController,
                public utils: UtilsProvider) {
        this.init.company_categories$.subscribe(company_categories => this.company_categories = company_categories);
        this.init.regions$.subscribe(regions => this.regions = regions);
    }

    filterCoupons () {

        if (this.category === 0 && this.region === 0){
            return this.filteredCompanies = this.companies;
        }

        if (this.category === 0){
            return this.filteredCompanies = this.companies.filter(company => company.region_id === this.region);
        }

        if (this.region === 0){
            return this.filteredCompanies = this.companies.filter(company => company.company_category_id === this.category);
        }

        this.filteredCompanies = this.companies.filter(company => company.company_category_id === this.category && company.region_id === this.region);

    }

    useCoupon (company, coupon, price) {

        if (this.init.points < price){

            let modal = this.modalCtrl.create('FacebookPage', {entity: 'points'});
            modal.present();
            return;

        }

        this.restangular.one('coupons', coupon.id).one('users', this.auth.id).customGET('bind')
            .subscribe(data => {

                console.log(data);
                this.showPopup(company, coupon);

                this.getCompanies();

                this.init.user = data;
                this.init.setPoints(data.points);

            });

    }

    showPopup (company, coupon) {

        let modal = this.modalCtrl.create('FacebookPage', {company: company, entity: 'coupon'});
        modal.present();

        modal.onDidDismiss(() => {

            coupon.company = company;

            // let modal = this.modalCtrl.create('CouponPage', {coupon: coupon, from: 'profile'});
            // modal.present();

        })

    }

    goToCoupon(coupon, used, company){

        coupon.company = company;

        let modal = this.modalCtrl.create('CouponPage', {coupon: coupon, state: 'content'});
        modal.present();

    }

    goToCompany (id){console.log(5);
        this.navCtrl.push('CompanyPage', {company_id: id})
    }

    getCompanies() {

        this.companies = [];
        this.filteredCompanies = [];

        this.restangular.all('coupons').getList({company_id: this.company_id}).subscribe(data => {

            for (let item of data){

                if ((item.coupons && item.coupons.length > 0) || item.coupon){
                    this.companies.push(item);
                    this.filteredCompanies.push(item);
                }

            }

        });

    }


    ionViewWillEnter() {
        this.getCompanies();
    }

}
