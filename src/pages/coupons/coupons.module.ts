import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {CouponsPage} from './coupons';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        CouponsPage,
    ],
    imports: [
        IonicPageModule.forChild(CouponsPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class CouponsPageModule {
}
