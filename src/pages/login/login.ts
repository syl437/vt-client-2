import {Component} from '@angular/core';
import {AlertController, IonicPage, MenuController, NavController, Platform} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {Storage} from '@ionic/storage';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {UtilsProvider} from '../../providers/utils/utils';
import {PushService} from '../../providers/push-service/push-service';
import {OneSignal} from '@ionic-native/onesignal';
import {TranslateService} from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})

export class LoginPage {

    user = {email: '', password: '', push_id: ''};
    // user = {email: 'evgeniagofman@gmail.com', password: '123456', push_id: ''};
    language: any;

    constructor(public navCtrl: NavController,
                private restangular: Restangular,
                public alertCtrl:AlertController,
                private auth: AuthServiceProvider,
                private storage: Storage,
                public init: InitServiceProvider,
                public utils: UtilsProvider,
                public translate: TranslateService,
                private menu: MenuController,
                public push: PushService,
                public platform: Platform,
                public onesignal: OneSignal) {

        this.utils.language$.subscribe(data => this.language = data);
    }

    authenticate () {

        if (this.user.email === '' || this.user.password === ''){

            let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK']});
            alert.present();
            return;

        }

        if (this.platform.is('cordova')){

            this.onesignal.getIds().then(ids => {

                this.user.push_id = ids['userId'];
                this.restangular.all('tokens/users').post(this.user).subscribe(data => {

                    this.auth.setToken(data.api_token);

                    this.storage.set('id', data.id).then(() => {

                        this.auth.initialize().then(() => {

                            this.init.onInitApp().then(success => {
                                this.push.init();
                                this.navCtrl.setRoot('HomePage');
                            })

                        })
                    })

                })
            });

        } else {

            this.user.push_id = null;
            this.restangular.all('tokens/users').post(this.user).subscribe(data => {

                this.auth.setToken(data.api_token);

                this.storage.set('id', data.id).then(() => {

                    this.auth.initialize().then(() => {

                        this.init.onInitApp().then(success => {
                            this.push.init();
                            this.navCtrl.setRoot('HomePage');
                        })

                    })


                })
            })
        }
    }

    goToRegister() {
        this.navCtrl.push('RegisterPage');
    }

    showForgotPopup () {

        let prompt = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.FORGOT"),
            message: this.translate.instant("POPUPS.ENTEREMAIL"),
            inputs: [
                {
                    name: 'email',
                    placeholder: this.translate.instant("POPUPS.EMAIL2"),
                },
            ],
            buttons: [
                {text: 'Cancel'},
                {
                    text: 'Submit',
                    handler: data => {

                        if (data.email !== ''){

                            this.restangular.all('tokens/password').customPOST({email: data.email}).subscribe(data => {

                                let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.PASSWORDSENT"), buttons: ['OK']});
                                alert.present();

                            });

                        }

                    }
                }
            ]
        });
        prompt.present();

    }

    ionViewDidEnter() {
        this.menu.swipeEnable(false);
    }

    ionViewDidLeave() {
        this.menu.swipeEnable(true);
    }

}
