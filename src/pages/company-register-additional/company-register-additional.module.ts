import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComponentsModule} from '../../components/components.module';
import {CompanyRegisterAdditionalPage} from './company-register-additional';
import {TranslateModule} from '@ngx-translate/core';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
    declarations: [
        CompanyRegisterAdditionalPage,
    ],
    imports: [
        IonicPageModule.forChild(CompanyRegisterAdditionalPage),
        ComponentsModule,
        TranslateModule,
        DirectivesModule
    ],
})
export class CompanyRegisterAdditionalPageModule {
}
