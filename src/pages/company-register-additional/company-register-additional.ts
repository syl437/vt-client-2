import {Component} from '@angular/core';
import {FileTransfer, FileUploadOptions, FileTransferObject} from '@ionic-native/file-transfer';
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {NavController, AlertController, LoadingController, NavParams, IonicPage} from 'ionic-angular';
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {Restangular} from "ngx-restangular";
import {Storage} from "@ionic/storage";
import {UtilsProvider} from "../../providers/utils/utils";
import {TranslateService} from '@ngx-translate/core';

@IonicPage()
@Component({
    selector: 'page-company-register-additional',
    templateUrl: 'company-register-additional.html',
})

export class CompanyRegisterAdditionalPage {

    company: object;
    video: any;
    src: string;
    company_id: number = this.navParams.get('company_id') || 1;
    logo: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private transfer: FileTransfer,
                private auth: AuthServiceProvider,
                private alertCtrl: AlertController,
                public translate: TranslateService,
                public loadingCtrl: LoadingController,
                public init: InitServiceProvider,
                public restangular: Restangular,
                public storage: Storage,
                public utils: UtilsProvider) {}

    setLogo(logo: any) {

        console.log('Logo received', logo);
        this.logo = logo;
    }

    setVideo(video: any) {

        console.log('Video received', video);
        this.video = video;
        this.src = video.fullPath;
    }

    send() {

        console.log(this.video, this.logo, this.company_id);

        let promises = [];

        if (typeof this.video !== 'undefined') {

            promises.push(this.sendVideo());

        }

        if (typeof this.logo !== 'undefined') {

            promises.push(this.sendLogo());

        }

        Promise.all(promises).then(() => this.alertUser())

    }

    sendLogo () {

        return new Promise((resolve, reject) => {

            let progress = 0;

            let loading = this.loadingCtrl.create({
                content: "Please wait...",
                spinner: 'ios',
            });

            loading.present();

            const fileTransfer: FileTransferObject = this.transfer.create();

            let options: FileUploadOptions = {
                fileKey: 'logo',
                mimeType: this.logo.type,
                fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
                headers: {Authorization: `Bearer ${this.auth.getToken()}`},
                params: {media_key: 'logo', entity_type: 'company', 'entity_id': this.company_id}
            };

            console.log(options);

            fileTransfer.onProgress((event: ProgressEvent) => {

                if (event.lengthComputable) {

                    progress = Math.round((event.loaded / event.total) * 100);

                }

            });

            fileTransfer.upload(this.logo.url, this.utils.baseUrl + 'api/v1/files', options)
                .then((data) => {

                    console.log(data);
                    loading.dismiss();

                    let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.UPLOADED"), buttons: ['OK']});
                    alert.present();

                    resolve();

                }, (err) => {

                    console.log(err);
                    loading.dismiss();

                    let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ERROR"), buttons: ['OK']});
                    alert.present();

                    reject();

                })

        })

    }

    sendVideo () {

        return new Promise((resolve, reject) => {

            let progress = 0;

            let loading = this.loadingCtrl.create({
                content: this.translate.instant("POPUPS.PLEASEWAIT"),
                spinner: 'ios',
            });

            loading.present();

            const fileTransfer: FileTransferObject = this.transfer.create();

            let options: FileUploadOptions = {
                fileKey: 'main_video',
                mimeType: this.video.type,
                fileName: this.video.name,
                headers: {Authorization: `Bearer ${this.auth.getToken()}`},
                params: {media_key: 'main_video', entity_type: 'company', 'entity_id': this.company_id}
            };

            fileTransfer.onProgress((event: ProgressEvent) => {

                if (event.lengthComputable) {

                    progress = Math.round((event.loaded / event.total) * 100);

                }

            });

            fileTransfer.upload(this.video.fullPath, this.utils.baseUrl + 'api/v1/files', options)
                .then((data) => {

                    console.log(data);
                    loading.dismiss();

                    let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.UPLOADED"), buttons: ['OK']});
                    alert.present();

                    resolve();

                }, (err) => {

                    console.log(err);
                    loading.dismiss();

                    let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.ERROR"), buttons: ['OK']});
                    alert.present();

                    reject();

                })

        })

    }

    alertUser () {

        let alert = this.alertCtrl.create({title: this.translate.instant("POPUPS.APPROVAL"), buttons: ['OK']});
        alert.present().then(() => {

            this.storage.set('approved', 0).then(() => this.navCtrl.setRoot('HomePage'));

        })

    }

    ngOnInit () {
        this.restangular.one('companies', this.company_id).get().subscribe(data => this.company = data);
    }

}
