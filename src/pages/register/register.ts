import {Component} from '@angular/core';
import {IonicPage, MenuController, NavController} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {Storage} from '@ionic/storage';
import {UtilsProvider} from '../../providers/utils/utils';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PushService} from '../../providers/push-service/push-service';
import {OneSignal} from '@ionic-native/onesignal';

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage {

    form: FormGroup;
    countries = [];
    language: any;

    constructor(public navCtrl: NavController,
                public restangular: Restangular,
                public storage: Storage,
                public utils: UtilsProvider,
                public init: InitServiceProvider,
                public auth: AuthServiceProvider,
                public fb: FormBuilder,
                private menu: MenuController,
                public push: PushService,
                public onesignal: OneSignal) {

        this.form = fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            password_confirmation: ['', Validators.required],
            name:  ['', Validators.required],
            phone: ['', [Validators.required, Validators.minLength(9)]],
            country: ['IL', Validators.required]
        }, {validator: this.matchingPasswords('password', 'password_confirmation')});

        this.menu.swipeEnable(false);

        this.utils.language$.subscribe(data => this.language = data);

    }

    matchingPasswords(passwordKey: string, confirmPasswordKey: string) {

        return (group: FormGroup): {[key: string]: any} => {

            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];

            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        }
    }

    submit () {

        let user = this.restangular.restangularizeElement('', {
            email: this.form.value.email,
            password: this.form.value.password,
            name: this.form.value.name,
            phone : this.form.value.phone,
            country: this.form.value.country,
            push_id: ''
        }, 'users');

        this.onesignal.getIds().then(ids => {

            user.push_id = ids['userId'];
            console.log(user);
            console.log(this.form);

            user.save().subscribe( data => {

                this.push.init();
                this.storage.set('token', data.api_token).then(() => {

                    this.auth.setToken(data.api_token);

                    this.storage.set('id', data.id).then(() => {

                        this.auth.initialize().then(() => {

                            this.init.onInitApp().then(() => {this.navCtrl.setRoot('HomePage');});

                        })

                    });

                })

            })
        })

            .catch((error: any) => console.log(error));

    }

    goToLogin () {
        this.navCtrl.push('LoginPage')
    }

    ngOnInit () {
        this.restangular.all('init/countries').getList().subscribe(data => this.countries = data);
        this.menu.swipeEnable(false);
    }

    ionViewDidLeave() {
        this.menu.swipeEnable(true);
    }

}
