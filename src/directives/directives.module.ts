import { NgModule } from '@angular/core';
import {AddVideoDirective} from './add-video/add-video';
import {AddPictureDirective} from './add-picture/add-picture';
import {BackgroundImageDirective} from './background-image/background-image';
import {MyVideoDirective} from './my-video/my-video';

@NgModule({
	declarations: [AddVideoDirective, AddPictureDirective, BackgroundImageDirective, MyVideoDirective],
	imports: [],
	exports: [AddVideoDirective, AddPictureDirective, BackgroundImageDirective, MyVideoDirective]
})
export class DirectivesModule {}
