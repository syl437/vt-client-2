import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { Platform } from 'ionic-angular';
import { File, FileEntry } from '@ionic-native/file';
import {Md5} from 'ts-md5';

@Directive({
    selector: '[my-video]' // Attribute selector
})
export class MyVideoDirective implements OnChanges {

    @Input('source')
    source: string;

    video: HTMLVideoElement = this.el.nativeElement as HTMLVideoElement;

    constructor(public el: ElementRef, public file: File, public platform: Platform, public transfer: FileTransfer) {}

    ngOnInit() {}

    ngOnChanges(changes: SimpleChanges) {
        for (let change in changes) {
            if (change == 'source' && this.platform.is('android')) {
                this.downloadVideo(changes[change].currentValue);
            } else if (change == 'source' && !this.platform.is('android')){
                this.video.src = changes[change].currentValue;
            }
        }
    }

    downloadVideo(url: string) {
        this.platform.ready().then(async () => {
            const fileTransfer: FileTransferObject = this.transfer.create();
            fileTransfer.download(url, this.file.cacheDirectory + '/' + Md5.hashStr(url))
                .then(d => {
                    let fileEntry = d as FileEntry;
                    this.video.src = fileEntry.toInternalURL();
                });
        });
    }
}