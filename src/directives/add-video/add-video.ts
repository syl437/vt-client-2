import {Directive, EventEmitter, HostListener, Output} from '@angular/core';
import {AlertController} from "ionic-angular";
import { Camera, CameraOptions } from '@ionic-native/camera';
import {Diagnostic} from '@ionic-native/diagnostic';
import {MediaCapture, MediaFile, CaptureError} from '@ionic-native/media-capture';
import {UtilsProvider} from "../../providers/utils/utils";
import {TranslateService} from '@ngx-translate/core';


@Directive({
    selector: '[add-video]' // Attribute selector
})

export class AddVideoDirective {

    @Output('targetVideo')
    targetVideo: EventEmitter<object> = new EventEmitter();

    constructor(public alertCtrl: AlertController,
                public camera: Camera,
                public diagnostic: Diagnostic,
                public mediaCapture: MediaCapture,
                public translate: TranslateService,
                public utils: UtilsProvider) {}

    @HostListener('click', ['$event'])

    onClick(event: Event) {

        let alert = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.CHOOSESOURCE"),
            buttons: [
                {
                    text: 'Folder',
                    handler: () => {this.selectVideo();}
                },
                {
                    text: 'Camera',
                    handler: () => {this.checkPermissions();}
                }
            ]
        });
        alert.present();
    }

    selectVideo () {

        let options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            mediaType: this.camera.MediaType.VIDEO,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };

        this.camera.getPicture(options).then((imageData) => {

            console.log(imageData);

            let construct = {fullPath: '', name: '', type: ''};
            construct.fullPath = imageData;
            construct.name = imageData.substr(imageData.lastIndexOf('/') + 1);
            construct.type = 'video/mp4';

            this.targetVideo.emit(construct);

        }, (err) => {

            console.log('err', err);

        });

    }

    checkPermissions() {

        this.diagnostic.isCameraAuthorized(true).then((isAuthorized) => {

            if (!isAuthorized) {
                this.diagnostic.requestCameraAuthorization(true).then(() => this.record());
            } else {
                this.record();
            }

        }).catch((error) => console.log('Error in camera authorization', error));

    }


    record() {

        let options = {limit: 1, duration: 30};

        this.mediaCapture.captureVideo(options).then(
            (data: MediaFile[]) => {
                console.log(data);
                this.targetVideo.emit(data[0]);
            },

            (err: CaptureError) => console.error(err)
        );

    }


}
