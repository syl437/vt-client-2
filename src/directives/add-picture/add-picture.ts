import {Directive, EventEmitter, HostListener, Output} from '@angular/core';
import {AlertController} from "ionic-angular";
import { Camera, CameraOptions } from '@ionic-native/camera';
import {UtilsProvider} from "../../providers/utils/utils";
import {TranslateService} from '@ngx-translate/core';


@Directive({
    selector: '[add-picture]' // Attribute selector
})
export class AddPictureDirective {

    @Output('targetLogo')
    targetLogo: EventEmitter<object> = new EventEmitter();

    constructor(public alertCtrl: AlertController,
                public camera: Camera,
                public translate: TranslateService,
                public utils: UtilsProvider) {}

    @HostListener('click', ['$event'])

    onClick(event: Event) {

        let alert = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.CHOOSESOURCE"),
            buttons: [
                {
                    text: 'Folder',
                    handler: () => {
                        this.makeLogo(0);
                    }
                },
                {
                    text: 'Camera',
                    handler: () => {
                        this.makeLogo(1);
                    }
                }
            ]
        });
        alert.present();
    }

    makeLogo(x: number) {

        let options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: x === 0 ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA,
            targetWidth: 600,
            targetHeight: 600,
        };

        this.camera.getPicture(options).then((imageData) => {

            console.log(imageData);
            // this.logo = imageData;
            this.targetLogo.emit({url: imageData, type: 'image/jpeg'});

        }, (err) => {

            console.log('err', err);

        });


    }


}
