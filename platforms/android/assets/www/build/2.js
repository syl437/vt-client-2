webpackJsonp([2],{

/***/ 855:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersPageModule", function() { return UsersPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__users__ = __webpack_require__(881);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var UsersPageModule = /** @class */ (function () {
    function UsersPageModule() {
    }
    UsersPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__users__["a" /* UsersPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__users__["a" /* UsersPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], UsersPageModule);
    return UsersPageModule;
}());

//# sourceMappingURL=users.module.js.map

/***/ }),

/***/ 881:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(436);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var UsersPage = /** @class */ (function () {
    function UsersPage(navCtrl, navParams, restangular, alertCtrl, translate, init, modalCtrl, social, utils) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restangular = restangular;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.init = init;
        this.modalCtrl = modalCtrl;
        this.social = social;
        this.utils = utils;
        this.videos = [];
        this.filteredVideos = [];
        this.search = '';
        this.pageNumber = 1;
        this.lastPage = false;
        this.user = this.init.user;
        this.comment = {};
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    UsersPage.prototype.sendComment = function (video, comment) {
        var _this = this;
        if (this.comment.content !== '') {
            this.comment = this.restangular.restangularizeElement('', { content: comment, user_id: this.user.id, media_id: video.id }, 'comments');
            this.comment.save().subscribe(function (data) {
                _this.comment.user = _this.user;
                video.comments.push(_this.comment);
                _this.comment[video.id] = '';
            });
        }
    };
    UsersPage.prototype.showMore = function (video) {
        video.openedComments = 1;
    };
    // extractHeader (record) {
    //     if (record)
    //         return record.user;
    // }
    UsersPage.prototype.onInput = function (event) {
        if (event.target.value)
            this.filteredVideos = this.videos.filter(function (video) { return video.user.name.toLowerCase().indexOf(event.target.value.toLowerCase()) >= 0; });
        else
            this.filteredVideos = this.videos;
    };
    UsersPage.prototype.onCancel = function (event) {
        return this.filteredVideos = this.videos;
    };
    UsersPage.prototype.goToUser = function (id) {
        var _this = this;
        var modal = this.modalCtrl.create('UserPage', { user_id: id });
        modal.present();
        modal.onWillDismiss(function (data) {
            if (data.length !== 0) {
                for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                    var update = data_1[_i];
                    for (var _a = 0, _b = _this.videos; _a < _b.length; _a++) {
                        var item = _b[_a];
                        if (item.user.id === update.id) {
                            item.user.followed = update.status;
                            // this.extractHeader(item);
                        }
                    }
                }
            }
        });
    };
    UsersPage.prototype.share = function (video) {
        var message = '';
        if (this.language === 'en') {
            message = video.user.name + ' uploaded video ' + video.title + ' at ' + video.region + ' via YTravel application';
        }
        else {
            message = video.user.name + ' צילם סרטון ' + video.title + ' ב' + video.region + ' דרך אפליקצית YTravel';
        }
        this.social.share(message, video.title, '', this.utils.baseUrl + 'video?id=' + video.id + '&type=user&lang=' + this.language);
    };
    UsersPage.prototype.goToFriends = function (id) {
        this.navCtrl.push('FeedPage', { user_id: id });
    };
    UsersPage.prototype.follow = function (id) {
        var _this = this;
        this.restangular.all('followers').post({ recipient_id: id }).subscribe(function (data) {
            _this.init.following = data;
            for (var _i = 0, _a = _this.filteredVideos; _i < _a.length; _i++) {
                var item = _a[_i];
                if (item.user.id === id) {
                    item.user.followed = 1;
                    // this.extractHeader(item);
                }
            }
            for (var _b = 0, _c = _this.videos; _b < _c.length; _b++) {
                var item = _c[_b];
                if (item.user.id === id) {
                    item.user.followed = 1;
                    // this.extractHeader(item);
                }
            }
            var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.ADDED"), buttons: ['OK'] });
            alert.present();
        });
    };
    UsersPage.prototype.unfollow = function (id) {
        var _this = this;
        this.restangular.one('followers', id).remove().subscribe(function (data) {
            _this.init.following = data;
            for (var _i = 0, _a = _this.filteredVideos; _i < _a.length; _i++) {
                var item = _a[_i];
                if (item.user.id === id) {
                    item.user.followed = 0;
                    // this.extractHeader(item);
                }
            }
            for (var _b = 0, _c = _this.videos; _b < _c.length; _b++) {
                var item = _c[_b];
                if (item.user.id === id) {
                    item.user.followed = 0;
                    // this.extractHeader(item);
                }
            }
            var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.DONE"), buttons: ['OK'] });
            alert.present();
        });
    };
    UsersPage.prototype.ionViewWillEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, _i, _a, item, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.restangular.all('followers').customGET('', { page: this.pageNumber }).toPromise()];
                    case 1:
                        data = _b.sent();
                        for (_i = 0, _a = data.videos; _i < _a.length; _i++) {
                            item = _a[_i];
                            item.openedComments = 0;
                        }
                        this.videos = data.videos;
                        this.filteredVideos = data.videos;
                        this.pageNumber += 1;
                        this.lastPage = data.info.current_page === data.info.last_page;
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _b.sent();
                        console.log(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UsersPage.prototype.getMoreVideos = function (infiniteScroll) {
        return __awaiter(this, void 0, void 0, function () {
            var data, _i, _a, item, err_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, 3, 4]);
                        return [4 /*yield*/, this.restangular.all('followers').customGET('', { page: this.pageNumber }).toPromise()];
                    case 1:
                        data = _b.sent();
                        for (_i = 0, _a = data.videos; _i < _a.length; _i++) {
                            item = _a[_i];
                            item.openedComments = 0;
                            this.videos.push(item);
                            this.filteredVideos.push(item);
                        }
                        this.pageNumber += 1;
                        this.lastPage = data.info.current_page === data.info.last_page;
                        return [3 /*break*/, 4];
                    case 2:
                        err_2 = _b.sent();
                        console.log(err_2);
                        return [3 /*break*/, 4];
                    case 3:
                        infiniteScroll.complete();
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    UsersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-users',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\users\users.html"*/'<ion-header>\n\n\n\n    <common-header title=\'{{"MENU.SOCIALNETWORK" | translate}}\'></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-row class="search-row">\n\n\n\n        <ion-searchbar\n\n            [(ngModel)]="search"\n\n            [showCancelButton]="shouldShowCancel"\n\n            (ionInput)="onInput($event)"\n\n            (ionCancel)="onCancel($event)"\n\n            placeholder=\'{{"ALL.SEARCHUSER" | translate}}\'\n\n            class="customPlaceholder">\n\n        </ion-searchbar>\n\n\n\n    </ion-row>\n\n\n\n    <!--Old approach with virtualScroll-->\n\n\n\n    <!--<div [virtualScroll]="filteredVideos" [headerFn]="extractHeader" approxItemHeight="400px">-->\n\n\n\n        <!--<div *virtualHeader="let header" approxHeaderWidth="100%">-->\n\n            <!--<ion-item>-->\n\n                <!--<ion-avatar item-start (click)="goToUser(header.id)">-->\n\n                    <!--<img [src]="header.avatar" *ngIf="header.avatar !== \'\'">-->\n\n                    <!--<img src="assets/images/avatar.jpg" *ngIf="header.avatar === \'\'">-->\n\n                <!--</ion-avatar>-->\n\n                <!--<div style="padding-right: 15px;">-->\n\n                    <!--<span (click)="goToUser(header.id)">{{header.name}}</span>-->\n\n                    <!--<span (click)="goToFriends(header.id);"><ion-icon name="people"></ion-icon></span>-->\n\n                <!--</div>-->\n\n                <!--<div item-end fixed *ngIf="header.id !== user.id">-->\n\n                    <!--<button ion-button color="primary" (click)="follow(header.id)" *ngIf="header.followed === 0">{{"FEED.FOLLOW" | translate}}</button>-->\n\n                    <!--<button ion-button color="light" (click)="unfollow(header.id)" *ngIf="header.followed === 1">{{"FEED.UNFOLLOW" | translate}}</button>-->\n\n                <!--</div>-->\n\n            <!--</ion-item>-->\n\n        <!--</div>-->\n\n\n\n        <!--<div *virtualItem="let video" text-center style="width: 100%;">-->\n\n\n\n            <!--<ion-row>-->\n\n                <!--<video my-video [source]="video.url" controls controlsList="nodownload" poster="{{video.thumbnail}}"></video>-->\n\n            <!--</ion-row>-->\n\n\n\n            <!--<ion-row style="padding: 5px;"><b>{{video.title}}</b></ion-row>-->\n\n\n\n            <!--<ion-row (click)="showMore(video)" class="previous-comments" *ngIf="video.openedComments === 0 && video.comments.length > 2" text-center><ion-col>Show more</ion-col></ion-row>-->\n\n            <!--<div *ngIf="video.openedComments === 1 && video.comments.length > 2">-->\n\n                <!--<div *ngFor="let comment of video.comments">-->\n\n                    <!--<ion-row>-->\n\n                        <!--<ion-col col-2 text-center>-->\n\n                            <!--<div class="avatar" background-image [source]="comment.user.avatar"></div>-->\n\n                        <!--</ion-col>-->\n\n                        <!--<ion-col col-10>-->\n\n                            <!--<div text-start style="padding-top: 5px;">{{comment.content}}</div>-->\n\n                        <!--</ion-col>-->\n\n                    <!--</ion-row>-->\n\n                <!--</div>-->\n\n            <!--</div>-->\n\n            <!--<div *ngIf="video.openedComments === 0 && filteredVideos.length > 0">-->\n\n                <!--<div *ngFor="let comment of video.comments; let i = index">-->\n\n                    <!--<ion-row *ngIf="i == (video.comments.length - 1) || i == (video.comments.length - 2)">-->\n\n                        <!--<ion-col col-2 text-center>-->\n\n                            <!--<div class="avatar" background-image [source]="comment.user.avatar"></div>-->\n\n                        <!--</ion-col>-->\n\n                        <!--<ion-col col-10>-->\n\n                            <!--<div text-start style="padding-top: 5px;">{{comment.content}}</div>-->\n\n                        <!--</ion-col>-->\n\n                    <!--</ion-row>-->\n\n                <!--</div>-->\n\n            <!--</div>-->\n\n\n\n            <!--<ion-row *ngIf="filteredVideos.length > 0">-->\n\n                <!--<ion-col col-10>-->\n\n                    <!--<ion-input type="text" [(ngModel)]="comment[video.id]" placeholder=\'{{"FEED.COMMENT" | translate}}\' class="comment-input"></ion-input>-->\n\n                <!--</ion-col>-->\n\n                <!--<ion-col col-2>-->\n\n                    <!--<button small ion-button (click)="sendComment(video, comment[video.id])">{{"ALL.SEND" | translate}}</button>-->\n\n                <!--</ion-col>-->\n\n            <!--</ion-row>-->\n\n        <!--</div>-->\n\n\n\n    <!--</div>-->\n\n\n\n    <!--Temporary solution-->\n\n\n\n    <div *ngFor="let video of filteredVideos">\n\n\n\n        <div>\n\n            <ion-item>\n\n                <ion-avatar item-start (click)="goToUser(video.user.id)">\n\n                    <img [src]="video.user.avatar" *ngIf="video && video.user && video.user.avatar !== \'\'">\n\n                    <img src="assets/images/avatar.jpg" *ngIf="video && video.user && video.user.avatar === \'\'">\n\n                </ion-avatar>\n\n                <div style="padding-right: 15px;">\n\n                    <span (click)="goToUser(video.user.id)">{{video.user.name}}</span>\n\n                    <span (click)="goToFriends(video.user.id);"><ion-icon name="people"></ion-icon></span>\n\n                </div>\n\n                <div item-end fixed *ngIf="video.user.id !== user.id">\n\n                    <button ion-button color="primary" (click)="follow(video.user.id)" *ngIf="video.user.followed === 0">{{"FEED.FOLLOW" | translate}}</button>\n\n                    <button ion-button color="light" (click)="unfollow(video.user.id)" *ngIf="video.user.followed === 1">{{"FEED.UNFOLLOW" | translate}}</button>\n\n                </div>\n\n            </ion-item>\n\n        </div>\n\n\n\n        <div text-center style="width: 100%;">\n\n\n\n            <ion-row>\n\n                <video my-video [source]="video.url" controls controlsList="nodownload" poster="{{video.thumbnail}}"></video>\n\n            </ion-row>\n\n\n\n            <ion-row align-items-center>\n\n                <ion-col col-10 text-start>\n\n                    <div><b>{{video.title}}</b></div>\n\n                    <div>{{video.company_category}} / {{video.company_subcategory}} / {{video.region}}</div>\n\n                </ion-col>\n\n                <ion-col col-2 text-center>\n\n                    <ion-icon name="share-alt" class="share-icon" (click)="share(video)"></ion-icon>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row (click)="showMore(video)" class="previous-comments" *ngIf="video.openedComments === 0 && video.comments.length > 2" text-center><ion-col>Show more</ion-col></ion-row>\n\n            <div *ngIf="video.openedComments === 1 && video.comments.length > 2">\n\n                <div *ngFor="let comment of video.comments">\n\n                    <ion-row>\n\n                        <ion-col col-2 text-center>\n\n                            <div class="avatar" background-image [source]="comment.user.avatar"></div>\n\n                        </ion-col>\n\n                        <ion-col col-10>\n\n                            <div text-start style="padding-top: 5px;">{{comment.content}}</div>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </div>\n\n            </div>\n\n            <div *ngIf="video.openedComments === 0 && filteredVideos.length > 0">\n\n                <div *ngFor="let comment of video.comments; let i = index">\n\n                    <ion-row *ngIf="i == (video.comments.length - 1) || i == (video.comments.length - 2)">\n\n                        <ion-col col-2 text-center>\n\n                            <div class="avatar" background-image [source]="comment.user.avatar"></div>\n\n                        </ion-col>\n\n                        <ion-col col-10>\n\n                            <div text-start style="padding-top: 5px;">{{comment.content}}</div>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </div>\n\n            </div>\n\n\n\n            <ion-row *ngIf="filteredVideos.length > 0">\n\n                <ion-col col-10>\n\n                    <ion-input type="text" [(ngModel)]="comment[video.id]" placeholder=\'{{"FEED.COMMENT" | translate}}\' class="comment-input"></ion-input>\n\n                </ion-col>\n\n                <ion-col col-2>\n\n                    <button small ion-button (click)="sendComment(video, comment[video.id])">{{"ALL.SEND" | translate}}</button>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n\n\n    </div>\n\n\n\n    <ion-infinite-scroll (ionInfinite)="getMoreVideos($event)" *ngIf="!lastPage">\n\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <common-footer></common-footer>\n\n</ion-footer>'/*ion-inline-end:"G:\github\vt-client-2\src\pages\users\users.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */]])
    ], UsersPage);
    return UsersPage;
}());

//# sourceMappingURL=users.js.map

/***/ })

});
//# sourceMappingURL=2.js.map