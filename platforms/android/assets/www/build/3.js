webpackJsonp([3],{

/***/ 854:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPageModule", function() { return UserPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user__ = __webpack_require__(880);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var UserPageModule = /** @class */ (function () {
    function UserPageModule() {
    }
    UserPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__user__["a" /* UserPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__user__["a" /* UserPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], UserPageModule);
    return UserPageModule;
}());

//# sourceMappingURL=user.module.js.map

/***/ }),

/***/ 880:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UserPage = /** @class */ (function () {
    function UserPage(navCtrl, navParams, restangular, auth, init, social, utils, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restangular = restangular;
        this.auth = auth;
        this.init = init;
        this.social = social;
        this.utils = utils;
        this.viewCtrl = viewCtrl;
        this.user_id = this.navParams.get('user_id') || this.auth.id;
        this.user = { avatar: '', videos: [], coupons: [] };
        this.load = this.restangular.one('users', this.user_id).get();
        this.updates = [];
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    UserPage_1 = UserPage;
    UserPage.prototype.closeModal = function () { this.viewCtrl.dismiss(this.updates); };
    UserPage.prototype.share = function (video) {
        console.log(video);
        var message = '';
        if (this.language === 'en') {
            message = this.user.name + ' uploaded video ' + video.pivot.tag + ' at ' + video.region + ' via YTravel application';
        }
        else {
            message = this.user.name + ' צילם סרטון ' + video.pivot.tag + ' ב' + video.region + ' דרך אפליקצית YTravel';
        }
        this.social.share(message, video.title, '', this.utils.baseUrl + 'video?id=' + video.id + '&type=user&lang=' + this.language);
    };
    UserPage.prototype.goTo = function () {
        if (this.user.id === this.user_id) {
            this.navCtrl.push('ProfilePage');
        }
        else {
            this.navCtrl.push(UserPage_1);
        }
    };
    UserPage.prototype.follow = function (id) {
        var _this = this;
        this.restangular.all('followers').post({ recipient_id: id }).subscribe(function (data) {
            _this.init.following = data;
            _this.user.followed = 1;
            _this.updates.push({ id: _this.user.id, status: 1 });
        });
    };
    UserPage.prototype.unfollow = function (id) {
        var _this = this;
        this.restangular.one('followers', id).remove().subscribe(function (data) {
            _this.init.following = data;
            _this.user.followed = 0;
            _this.updates.push({ id: _this.user.id, status: 0 });
        });
    };
    UserPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.load.subscribe(function (data) {
            _this.user = data;
            _this.user_id = data.id;
        });
    };
    UserPage.prototype.ionViewDidLeave = function () {
        this.load.unsubscribe();
    };
    UserPage = UserPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-user',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\user\user.html"*/'<ion-header>\n\n\n\n    <ion-navbar color="primary" text-center>\n\n        <ion-title>{{user.name}}</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button (click)="closeModal()">\n\n                {{"ALL.CLOSE" | translate}}\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-row margin-top align-items-center>\n\n        <ion-col col-6>\n\n            <ion-row>\n\n                <ion-col col-4>\n\n                    <div *ngIf="user.avatar !== \'\'" class="user-avatar" background-image [source]="user.avatar" height="50"></div>\n\n                    <div *ngIf="user.avatar === \'\'" class="user-avatar" background-image [source]="\'assets/images/avatar.jpg\'" height="50"></div>\n\n                </ion-col>\n\n                <ion-col col-8>\n\n                    <div>{{user.name}}</div>\n\n                    <div dir="ltr">{{user.phone}}</div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-col>\n\n        <ion-col col-6>\n\n            <ion-row text-center ion-text color="primary">\n\n                <ion-col col-3>\n\n                    <div>{{user.followers}}</div>\n\n                    <div class="small-font">{{"ALL.FOLLOWERS" | translate}}</div>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <div>{{user.videos.length}}</div>\n\n                    <div class="small-font">{{"ALL.VIDEOS" | translate}}</div>\n\n                </ion-col>\n\n                <ion-col col-5>\n\n                    <div *ngIf="user.id !== auth.id">\n\n                        <button ion-button small color="primary" (click)="follow(user.id)" *ngIf="user.followed === 0">{{"FEED.FOLLOW" | translate}}</button>\n\n                        <button ion-button small color="light" (click)="unfollow(user.id)" *ngIf="user.followed === 1">{{"FEED.UNFOLLOW" | translate}}</button>\n\n                    </div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-col>\n\n        <div class="vertical-line"></div>\n\n    </ion-row>\n\n\n\n    <div *ngFor="let video of user.videos" text-center padding-top>\n\n\n\n        <video my-video [source]="video.url" controls controlsList="nodownload" poster="{{video.thumbnail}}"></video>\n\n        <ion-row align-items-center>\n\n            <ion-col col-10 text-start>\n\n                <div><b>{{video.pivot.tag}}</b></div>\n\n                <div>{{video.company_category}} / {{video.company_subcategory}} / {{video.region}}</div>\n\n            </ion-col>\n\n            <ion-col col-2 text-center>\n\n                <ion-icon name="share-alt" class="share-icon" (click)="share(video)"></ion-icon>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\user\user.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
    ], UserPage);
    return UserPage;
    var UserPage_1;
}());

//# sourceMappingURL=user.js.map

/***/ })

});
//# sourceMappingURL=3.js.map