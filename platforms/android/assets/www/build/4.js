webpackJsonp([4],{

/***/ 853:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadPageModule", function() { return UploadPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__upload__ = __webpack_require__(879);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var UploadPageModule = /** @class */ (function () {
    function UploadPageModule() {
    }
    UploadPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__upload__["a" /* UploadPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__upload__["a" /* UploadPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], UploadPageModule);
    return UploadPageModule;
}());

//# sourceMappingURL=upload.module.js.map

/***/ }),

/***/ 879:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__agm_core__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var UploadPage = /** @class */ (function () {
    function UploadPage(navCtrl, navParams, transfer, alertCtrl, loadingCtrl, auth, init, restangular, utils, loader, zone, file, translate, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.transfer = transfer;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.auth = auth;
        this.init = init;
        this.restangular = restangular;
        this.utils = utils;
        this.loader = loader;
        this.zone = zone;
        this.file = file;
        this.translate = translate;
        this.modalCtrl = modalCtrl;
        this.user_id = this.auth.id;
        this.title = '';
        this.place = '';
        this.company_id = this.navParams.get('company_id') || 0;
        this.category_id = 0;
        this.subcategory_id = 0;
        this.region_id = 0;
        this.companies = [];
        this.company = 0;
        this.process = this.utils.uploadProcess;
        this.fileTransfer = this.transfer.create();
        this.utils.progress$.subscribe(function (val) {
            _this.zone.run(function () { _this.progress = val; });
        });
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
        this.init.company_categories$.subscribe(function (company_categories) { return _this.company_categories = company_categories; });
        this.init.regions$.subscribe(function (regions) { return _this.regions = regions; });
    }
    UploadPage.prototype.goToExplanation = function () {
        var modal = this.modalCtrl.create('FacebookPage', { entity: 'explanation_video' });
        modal.present();
    };
    UploadPage.prototype.getCompanies = function (entity) {
        var _this = this;
        if (entity === 'category') {
            this.subcategory_id = 0;
            this.companies = [];
            this.company = 0;
        }
        if (this.subcategory_id !== 0) {
            this.companies = [];
            this.company = 0;
            var loading_1 = this.loadingCtrl.create({
                content: 'Please wait...',
                spinner: 'ios',
            });
            loading_1.present();
            this.restangular.all('companies/sorted').customPOST({ company_subcategory_id: this.subcategory_id, region_id: this.region_id }).subscribe(function (data) {
                loading_1.dismiss();
                _this.companies = data;
                _this.loadMaps();
            });
        }
    };
    UploadPage.prototype.setVideo = function (video) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var loading, path, fileUrl, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('Video received', video);
                        loading = this.loadingCtrl.create({
                            content: 'Please wait while we are checking the file...',
                            spinner: 'ios',
                        });
                        loading.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        console.log(video.fullPath.startsWith('file'));
                        path = video.fullPath.startsWith('file') ? video.fullPath : 'file://' + video.fullPath;
                        console.log(path);
                        return [4 /*yield*/, this.file.resolveLocalFilesystemUrl(path)];
                    case 2:
                        fileUrl = _a.sent();
                        fileUrl.getMetadata(function (metadata) {
                            loading.dismiss();
                            if (metadata.size >= 100000000) {
                                _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.TOOBIG"), message: _this.translate.instant("POPUPS.TOOBIG2"), buttons: ['OK'] }).present();
                                return;
                            }
                            else {
                                _this.video = video;
                                _this.src = video.fullPath;
                            }
                        });
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _a.sent();
                        loading.dismiss();
                        console.log(error_1);
                        this.alertCtrl.create({ title: "Problem with video file!", message: "Please try another one", buttons: ['OK'] }).present();
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    UploadPage.prototype.collect = function () {
        console.log("Collect", typeof this.video, this.subcategory_id, this.category_id, this.region_id, this.title, this.place, this.company);
        // console.log(this.title, this.region_id, this.category_id, this.subcategory_id, this.company, this.place);
        if (this.title === '' || this.region_id === 0 || this.category_id === 0 || this.subcategory_id === 0 || typeof this.video === 'undefined') {
            var alert_1 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK'] });
            alert_1.present();
            return;
        }
        if (this.company === 0 && this.region_id !== 0 && this.companies.length != 0) {
            var alert_2 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK'] });
            alert_2.present();
            return;
        }
        console.log("Collect1");
        if (this.company === -1 && this.place === '') {
            var alert_3 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK'] });
            alert_3.present();
            return;
        }
        this.process = true;
        this.utils.setUploadProcess(true);
        console.log("Collec2");
        var params = {};
        params.entity_type = 'user';
        params.entity_id = this.auth.id;
        params.company_id = this.company;
        params.media_key = 'video';
        params.title = String(this.title);
        console.log('params', this.company);
        if (this.company === -1 || this.company === 0) {
            params.address = this.formatted_address[0];
            params.country = this.formatted_address[1];
            params.lat = this.formatted_address[2];
            params.lng = this.formatted_address[3];
            params.company_subcategory_id = this.subcategory_id;
            params.region_id = this.region_id;
            params.place = this.place;
            console.log(JSON.stringify(params));
            this.send(params);
        }
        else
            this.send(params);
    };
    UploadPage.prototype.check = function () {
        console.log("Check");
        if (typeof this.video === 'undefined' || this.title === '') {
            var alert_4 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK'] });
            alert_4.present();
            return;
        }
        this.process = true;
        this.utils.setUploadProcess(true);
        var params = {};
        params.entity_type = 'user';
        params.entity_id = this.user_id;
        params.company_id = this.company_id;
        params.media_key = 'video';
        params.title = this.title;
        this.send(params);
    };
    UploadPage.prototype.send = function (params) {
        var _this = this;
        //params {"entity_type":"user","entity_id":14,"company_id":0,"media_key":"video","title":"ויי","address":"אבני איתן","country":"","lat":32.824808,"lng":35.76577700000007,"company_subcategory_id":1,"region_id":1,"place":"אבני"}
        //params {"entity_type":"user","entity_id":14,"company_id":0,"media_key":"video","title":"ויי","address":"אבני חושן, מודיעין מכבים רעות, ישראל","country":"IL","lat":31.9032339,"lng":34.994774699999994,"company_subcategory_id":1,"region_id":1,"place":"אבני"}
        //{bytesSent: 199450, responseCode: 200, response: "{"status":200,"success":true,"data":null}", objectId: ""}
        var that = this;
        console.log('params', JSON.stringify(params));
        // const fileTransfer: FileTransferObject = this.transfer.create();
        var options = {
            fileKey: 'video',
            fileName: this.video.name,
            mimeType: this.video.type,
            headers: { Authorization: "Bearer " + this.auth.getToken() },
            params: params
        };
        this.fileTransfer.onProgress(function (event) {
            if (event.lengthComputable) {
                var progress = Math.round((event.loaded / event.total) * 100);
                console.log('Progress', progress);
                that.utils.setProgress(progress);
            }
        });
        console.log(that.utils);
        this.fileTransfer.upload(this.video.fullPath, that.utils.baseUrl + 'api/v1/files', options)
            .then(function (data) {
            console.log(data);
            var modal = _this.modalCtrl.create('FacebookPage', { entity: '3dollars' });
            modal.present();
            _this.video = null;
            _this.src = null;
            _this.title = '';
            _this.place = '';
            _this.category_id = 0;
            _this.subcategory_id = 0;
            _this.region_id = 0;
            _this.companies = [];
            _this.company = 0;
            _this.process = false;
            that.utils.setUploadProcess(false);
            that.utils.setProgress(0);
        }, function (err) {
            console.log(JSON.stringify(err));
            // loading.dismiss();
            _this.process = false;
            that.utils.setUploadProcess(false);
            that.utils.setProgress(0);
            if (err.code === 4) {
                var alert_5 = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.ABORTED"), buttons: ['OK'] });
                alert_5.present();
            }
            else {
                var alert_6 = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.ERROR"), buttons: ['OK'] });
                alert_6.present();
            }
        });
    };
    UploadPage.prototype.abort = function () {
        this.fileTransfer.abort();
    };
    UploadPage.prototype.loadMaps = function () {
        var _this = this;
        console.log('loadMaps', this.companies.length, this.subcategory_id, this.company);
        if ((this.companies.length == 0 && this.subcategory_id != 0) || this.company == -1) {
            this.loader.load().then(function () {
                var address = document.getElementById('address').getElementsByTagName('input')[0];
                var autocomplete = new google.maps.places.Autocomplete(address);
                autocomplete.addListener("place_changed", function () {
                    var place = autocomplete.getPlace();
                    console.log("pl : ", place);
                    var countryComponent = '';
                    for (var _i = 0, _a = place.address_components; _i < _a.length; _i++) {
                        var item = _a[_i];
                        if (item.types[0] === 'country')
                            countryComponent = item.short_name;
                    }
                    //shay added
                    if (countryComponent == "")
                        countryComponent = "IL";
                    _this.formatted_address = [place.formatted_address, countryComponent, place.geometry.location.lat(), place.geometry.location.lng()];
                    console.log(_this.formatted_address);
                });
            });
        }
    };
    UploadPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-upload',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\upload\upload.html"*/'<ion-header>\n\n\n\n    <common-header title=\'{{"ALL.UPLOAD" | translate}}\'></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-row>\n\n        <button class="user-reviews-button" margin block ion-button color="primary" (click)="goToExplanation()">\n\n            {{"UPLOAD.EXPLANATION" | translate}}\n\n            <ion-icon name="ios-arrow-forward" [ngClass]="{\'icon-arrow1\' : language === \'he\', \'icon-arrow2\' : language === \'en\'}"></ion-icon>\n\n        </button>\n\n    </ion-row>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <ion-list no-lines>\n\n\n\n            <ion-item class="deleteLeftPadding" margin-top>\n\n                <ion-input text-center placeholder=\'{{"UPLOAD.FEWWORDS" | translate}}\' type="text" [(ngModel)]="title"></ion-input>\n\n            </ion-item>\n\n\n\n            <div *ngIf="company_id === 0">\n\n                <ion-item margin-top class="deleteLeftPadding customSelect">\n\n                    <ion-label color="primary">{{"ALL.REGION" | translate}}</ion-label>\n\n                    <ion-select [(ngModel)]="region_id" (ionBlur)="getCompanies(\'region\')">\n\n                        <ion-option *ngFor="let region of regions" [value]="region.id">{{region.title}}</ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding customSelect">\n\n                    <ion-label color="primary">{{"ALL.CATEGORY" | translate}}</ion-label>\n\n                    <ion-select [(ngModel)]="category_id" (ionBlur)="getCompanies(\'category\')">\n\n                        <ion-option *ngFor="let category of company_categories" [value]="category.id">{{category.title}}\n\n                        </ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n\n\n                <ion-item *ngIf="category_id != 0" margin-top class="deleteLeftPadding customSelect">\n\n                    <ion-label color="primary">{{"ALL.SUBCATEGORY" | translate}}</ion-label>\n\n                    <ion-select [(ngModel)]="subcategory_id" (ionBlur)="getCompanies(\'subcategory\')">\n\n                        <ion-option\n\n                                *ngFor="let subcategory of company_categories[category_id - 1].company_subcategories"\n\n                                [value]="subcategory.id">{{subcategory.title}}\n\n                        </ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n\n\n                <ion-item *ngIf="companies.length > 0" margin-top class="deleteLeftPadding customSelect">\n\n                    <ion-label color="primary">{{"UPLOAD.EXISTINGPLACES" | translate}}</ion-label>\n\n                    <ion-select [(ngModel)]="company" (ionBlur)="loadMaps()">\n\n                        <ion-option *ngFor="let company of companies" [value]="company.id">{{company.title}}</ion-option>\n\n                        <ion-option [value]="-1">{{"UPLOAD.ADDNEWPLACE" | translate}}</ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n\n\n                <div *ngIf="(companies.length == 0 && subcategory_id != 0) || company == -1">\n\n\n\n                    <div margin-vertical *ngIf="companies.length == 0">{{"UPLOAD.NOCOMPANIES" | translate}}</div>\n\n\n\n                    <ion-item margin-top class="deleteLeftPadding">\n\n                        <ion-input text-center placeholder=\'{{"UPLOAD.NAME" | translate}}\' type="text" [(ngModel)]="place"></ion-input>\n\n                    </ion-item>\n\n\n\n                    <ion-item margin-top class="deleteLeftPadding">\n\n                        <ion-input placeholder=\'{{"COMPANYREGISTER.ADDRESS" | translate}}\' text-center type="text" [(ngModel)]="address" id="address"></ion-input>\n\n                    </ion-item>\n\n\n\n                </div>\n\n\n\n            </div>\n\n\n\n            <div *ngIf="process === false" margin-top>\n\n                <video *ngIf="src" [src]="src" controls controlsList="nodownload"></video>\n\n            </div>\n\n\n\n            <div *ngIf="process === true" class="videoUpload" margin-top>\n\n                <ion-spinner name="ios"></ion-spinner>\n\n                <div>{{"ALL.UPLOADINGINPROGRESS" | translate}}: {{progress}}% </div>\n\n                <button ion-button color="primary" small (click)="abort()" class="cancel-button">{{"ALL.CANCEL" | translate}}</button>\n\n            </div>\n\n\n\n            <div margin-top style="color: grey; font-size: 12px;" text-start>\n\n                {{"UPLOAD.MAXIMUM" | translate}}\n\n            </div>\n\n\n\n            <div margin-top>\n\n                <img *ngIf="utils.language === \'he\'" src="assets/images/upload_he.jpg" style="width: 100%;" add-video (targetVideo)="setVideo($event)">\n\n                <img *ngIf="utils.language === \'en\'" src="assets/images/upload_en.jpg" style="width: 100%;" add-video (targetVideo)="setVideo($event)">\n\n            </div>\n\n\n\n            <div margin-top *ngIf="language === \'he\'">\n\n\n\n                <button class="button-no-border" ion-button outline no-border no-margin color="primary" [disabled]="process" *ngIf=\'company_id == 0\' (click)="collect()">\n\n                    {{"ALL.SUBMIT" | translate}}&nbsp;<img src="assets/images/arrows_left.png" height="25">\n\n                </button>\n\n\n\n                <button class="button-no-border" ion-button outline no-border no-margin color="primary" [disabled]="process" *ngIf=\'company_id > 0\' (click)="check()">\n\n                    {{"ALL.SUBMIT" | translate}}&nbsp;<img src="assets/images/arrows_left.png" height="25">\n\n                </button>\n\n\n\n            </div>\n\n\n\n            <div margin-top *ngIf="language === \'en\'">\n\n\n\n                <button class="button-no-border" ion-button outline no-border no-margin color="primary" [disabled]="process" *ngIf=\'company_id == 0\' (click)="collect()">\n\n                    {{"ALL.UPLOAD" | translate}}&nbsp;<img src="assets/images/arrows_right.png" height="25">\n\n                </button>\n\n\n\n                <button class="button-no-border" ion-button outline no-border no-margin color="primary" [disabled]="process" *ngIf=\'company_id > 0\' (click)="check()">\n\n                    {{"ALL.UPLOAD" | translate}}&nbsp;<img src="assets/images/arrows_right.png" height="25">\n\n                </button>\n\n\n\n            </div>\n\n\n\n        </ion-list>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <common-footer></common-footer>\n\n</ion-footer>'/*ion-inline-end:"G:\github\vt-client-2\src\pages\upload\upload.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_7__agm_core__["b" /* MapsAPILoader */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], UploadPage);
    return UploadPage;
}());

//# sourceMappingURL=upload.js.map

/***/ })

});
//# sourceMappingURL=4.js.map