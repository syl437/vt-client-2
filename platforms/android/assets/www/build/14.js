webpackJsonp([14],{

/***/ 842:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedPageModule", function() { return FeedPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__feed__ = __webpack_require__(868);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var FeedPageModule = /** @class */ (function () {
    function FeedPageModule() {
    }
    FeedPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__feed__["a" /* FeedPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__feed__["a" /* FeedPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], FeedPageModule);
    return FeedPageModule;
}());

//# sourceMappingURL=feed.module.js.map

/***/ }),

/***/ 868:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var FeedPage = /** @class */ (function () {
    function FeedPage(navCtrl, navParams, restangular, auth, init, social, utils, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restangular = restangular;
        this.auth = auth;
        this.init = init;
        this.social = social;
        this.utils = utils;
        this.modalCtrl = modalCtrl;
        this.videos = [];
        this.user_id = this.navParams.get('user_id') || this.auth.id;
        this.user = this.init.user;
        this.pageNumber = 1;
        this.lastPage = false;
        this.comment = {};
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    FeedPage.prototype.share = function (video) {
        var message = '';
        if (this.language === 'en') {
            message = video.user.name + ' uploaded video ' + video.title + ' at ' + video.region + ' via YTravel application';
        }
        else {
            message = video.user.name + ' צילם סרטון ' + video.title + ' ב' + video.region + ' דרך אפליקצית YTravel';
        }
        this.social.share(message, video.title, '', this.utils.baseUrl + 'video?id=' + video.id + '&type=user&lang=' + this.language);
    };
    FeedPage.prototype.sendComment = function (video, comment) {
        var _this = this;
        if (this.comment.content !== '') {
            this.comment = this.restangular.restangularizeElement('', { content: comment, user_id: this.user.id, media_id: video.id }, 'comments');
            this.comment.save().subscribe(function (data) {
                _this.comment.user = _this.user;
                video.comments.push(_this.comment);
                _this.comment = {};
                _this.comment[video.id] = '';
            });
        }
    };
    // extractHeader (record) {
    //     if (record)
    //         return record.user;
    // }
    FeedPage.prototype.unfollow = function (id) {
        var _this = this;
        this.restangular.one('followers', id).remove().subscribe(function (data) {
            _this.init.following = data;
            for (var _i = 0, _a = _this.videos; _i < _a.length; _i++) {
                var item = _a[_i];
                if (item.user.id === id) {
                    item.user.followed = 0;
                    // this.extractHeader(item);
                }
            }
        });
    };
    FeedPage.prototype.follow = function (id) {
        var _this = this;
        this.restangular.all('followers').post({ recipient_id: id }).subscribe(function (data) {
            _this.init.following = data;
            for (var _i = 0, _a = _this.videos; _i < _a.length; _i++) {
                var item = _a[_i];
                if (item.user.id === id) {
                    item.user.followed = 1;
                    // this.extractHeader(item);
                }
            }
        });
    };
    FeedPage.prototype.goToUser = function (id) {
        var _this = this;
        var modal = this.modalCtrl.create('UserPage', { user_id: id });
        modal.present();
        modal.onWillDismiss(function (data) {
            if (data.length !== 0) {
                for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                    var update = data_1[_i];
                    for (var _a = 0, _b = _this.videos; _a < _b.length; _a++) {
                        var item = _b[_a];
                        if (item.user.id === update.id) {
                            item.user.followed = update.status;
                            // this.extractHeader(item);
                        }
                    }
                }
            }
        });
    };
    FeedPage.prototype.ionViewWillEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.restangular.one('followers', this.user_id).customGET('', { page: this.pageNumber }).toPromise()];
                    case 1:
                        data = _a.sent();
                        this.videos = data.videos;
                        this.pageNumber += 1;
                        this.lastPage = data.info.current_page === data.info.last_page;
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    FeedPage.prototype.getMoreVideos = function (infiniteScroll) {
        return __awaiter(this, void 0, void 0, function () {
            var data, _i, _a, item, err_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, 3, 4]);
                        return [4 /*yield*/, this.restangular.one('followers', this.user_id).customGET('', { page: this.pageNumber }).toPromise()];
                    case 1:
                        data = _b.sent();
                        for (_i = 0, _a = data.videos; _i < _a.length; _i++) {
                            item = _a[_i];
                            this.videos.push(item);
                        }
                        this.pageNumber += 1;
                        this.lastPage = data.info.current_page === data.info.last_page;
                        return [3 /*break*/, 4];
                    case 2:
                        err_2 = _b.sent();
                        console.log(err_2);
                        return [3 /*break*/, 4];
                    case 3:
                        infiniteScroll.complete();
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    FeedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-feed',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\feed\feed.html"*/'<ion-header>\n\n\n\n    <common-header title=\'{{"MENU.FEED" | translate}}\'></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div *ngFor="let video of videos">\n\n\n\n        <ion-row>\n\n            <ion-item>\n\n                <ion-avatar item-start (click)="goToUser(video.user.id)">\n\n                    <img [src]="video.user.avatar">\n\n                </ion-avatar>\n\n                <div style="padding-right: 15px;" (click)="goToUser(video.user.id)">{{video.user.name}}</div>\n\n                <div item-end fixed *ngIf="video.user.id !== auth.id">\n\n                    <button ion-button color="primary" (click)="follow(video.user.id)" *ngIf="video.user.followed === 0">{{"FEED.FOLLOW" | translate}}</button>\n\n                    <button ion-button color="light" (click)="unfollow(video.user.id)" *ngIf="video.user.followed === 1">{{"FEED.UNFOLLOW" | translate}}</button>\n\n                </div>\n\n            </ion-item>\n\n        </ion-row>\n\n\n\n        <div text-center style="width: 100%;">\n\n\n\n            <ion-row style="height: 210px; margin: 0 auto;">\n\n                <video my-video [source]="video.url" controls controlsList="nodownload" poster="{{video.thumbnail}}"></video>\n\n            </ion-row>\n\n\n\n            <ion-row align-items-center>\n\n                <ion-col col-10 text-start>\n\n                    <div><b>{{video.title}}</b></div>\n\n                    <div>{{video.company_category}} / {{video.company_subcategory}} / {{video.region}}</div>\n\n                </ion-col>\n\n                <ion-col col-2 text-center>\n\n                    <ion-icon name="share-alt" class="share-icon" (click)="share(video)"></ion-icon>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row style="width: 100%;">\n\n                <ion-list style="width: 100%;" no-margin>\n\n                    <ion-item *ngFor="let comment of video.comments">\n\n                        <ion-avatar item-start>\n\n                            <img [src]="comment.user.avatar">\n\n                        </ion-avatar>\n\n                        <div>{{comment.content}}</div>\n\n                    </ion-item>\n\n                </ion-list>\n\n            </ion-row>\n\n\n\n            <ion-row>\n\n                <ion-col col-10>\n\n                    <ion-input type="text" [(ngModel)]="comment[video.id]" placeholder=\'{{"FEED.COMMENT" | translate}}\' class="comment-input"></ion-input>\n\n                </ion-col>\n\n                <ion-col col-2>\n\n                    <button small ion-button (click)="sendComment(video, comment[video.id])">{{"ALL.SEND" | translate}}</button>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n        </div>\n\n\n\n    </div>\n\n\n\n    <ion-infinite-scroll (ionInfinite)="getMoreVideos($event)" *ngIf="!lastPage">\n\n        <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <common-footer></common-footer>\n\n</ion-footer>'/*ion-inline-end:"G:\github\vt-client-2\src\pages\feed\feed.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], FeedPage);
    return FeedPage;
}());

//# sourceMappingURL=feed.js.map

/***/ })

});
//# sourceMappingURL=14.js.map