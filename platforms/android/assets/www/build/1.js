webpackJsonp([1],{

/***/ 837:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyPageModule", function() { return CompanyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__company__ = __webpack_require__(863);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pipes_pipes_module__ = __webpack_require__(856);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var CompanyPageModule = /** @class */ (function () {
    function CompanyPageModule() {
    }
    CompanyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__company__["a" /* CompanyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__company__["a" /* CompanyPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_6__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], CompanyPageModule);
    return CompanyPageModule;
}());

//# sourceMappingURL=company.module.js.map

/***/ }),

/***/ 856:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__truncate_truncate__ = __webpack_require__(857);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__truncate_truncate__["a" /* TruncatePipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__truncate_truncate__["a" /* TruncatePipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 857:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TruncatePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TruncatePipe = /** @class */ (function () {
    function TruncatePipe() {
    }
    TruncatePipe.prototype.transform = function (value, limit, completeWords, ellipsis) {
        if (limit === void 0) { limit = 55; }
        if (completeWords === void 0) { completeWords = false; }
        if (ellipsis === void 0) { ellipsis = '...'; }
        if (completeWords) {
            limit = value.substr(0, 13).lastIndexOf(' ');
        }
        return "" + value.substr(0, limit) + ellipsis;
    };
    TruncatePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'truncate'
        })
    ], TruncatePipe);
    return TruncatePipe;
}());

//# sourceMappingURL=truncate.js.map

/***/ }),

/***/ 863:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__ = __webpack_require__(439);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_launch_navigator__ = __webpack_require__(440);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_takeUntil__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_takeUntil___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_takeUntil__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_utils_utils__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var CompanyPage = /** @class */ (function () {
    function CompanyPage(navCtrl, navParams, restangular, callNumber, launchNavigator, init, utils, social, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restangular = restangular;
        this.callNumber = callNumber;
        this.launchNavigator = launchNavigator;
        this.init = init;
        this.utils = utils;
        this.social = social;
        this.modalCtrl = modalCtrl;
        this.company_id = this.navParams.get('company_id') || 4;
        this.company = { logo: null, main_video: null, videos: [] };
        this.coordinates = this.init.coordinates;
        this.load = this.restangular.one('companies', this.company_id).get();
        this.openedDescription = false;
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    CompanyPage.prototype.showRatePopup = function () {
        var _this = this;
        this.restangular.one('users', this.init.user.id).one('companies', this.company.id).customGET('check').subscribe(function (data) {
            var modal = _this.modalCtrl.create('RatePage', { company: _this.company });
            modal.present();
            modal.onWillDismiss(function (data) {
                if (data != null) {
                    _this.company.mark = data.company.mark;
                    _this.company.stars = data.company.stars;
                }
            });
        });
    };
    CompanyPage.prototype.call = function () {
        if (this.company.company_category_id === 1)
            this.callNumber.callNumber(this.init.phone, false);
        else
            this.callNumber.callNumber(this.company.phone, false);
    };
    CompanyPage.prototype.navigate = function () {
        var options = {
            start: [this.coordinates.lat, this.coordinates.lng],
            app: this.launchNavigator.APP.USER_SELECT
        };
        this.launchNavigator.navigate([this.company.lat, this.company.lng], options);
    };
    CompanyPage.prototype.goToCoupons = function () {
        this.navCtrl.push('CouponsPage', { company_id: this.company_id });
    };
    CompanyPage.prototype.goToUpload = function () {
        this.navCtrl.push('UploadPage', { company_id: this.company_id });
    };
    CompanyPage.prototype.goToLead = function () {
        var is_hotel = this.company.company_category_id === 1 ? 1 : 0;
        this.navCtrl.push('LeadPage', { company_id: this.company_id, is_hotel: is_hotel });
    };
    CompanyPage.prototype.shareVideo = function () {
        this.social.share(this.company.facebook, this.company.title, '', this.utils.baseUrl + 'video?id=' + this.company.id + '&type=company&lang=' + this.language);
    };
    CompanyPage.prototype.ngOnInit = function () {
        var _this = this;
        this.load.subscribe(function (data) { return _this.company = data; });
    };
    CompanyPage.prototype.ionViewDidLeave = function () {
        this.load.unsubscribe();
    };
    CompanyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-company',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\company\company.html"*/'<ion-header>\n\n\n\n    <ion-navbar color="primary" text-center dir="rtl">\n\n        <ion-buttons right>\n\n            <button ion-button (click)="goToUpload()">\n\n                <img src="assets/images/upload_movie.png" height="30">\n\n            </button>\n\n        </ion-buttons>\n\n        <ion-title>{{company.title}}</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-grid no-padding>\n\n\n\n        <ion-row *ngIf="company.main_video" dir="ltr">\n\n            <!--<video [src]="company.main_video" controls controlsList="nodownload" poster="{{company.thumbnail}}"></video>-->\n\n            <video my-video [source]="company.main_video" controls controlsList="nodownload" poster="{{company.thumbnail}}"></video>\n\n        </ion-row>\n\n\n\n        <ion-row justify-content-center align-items-center>\n\n            <ion-col col-6 ion-button color="primary" block (click)="goToLead()">{{"COMPANY.PRICEPROPOSAL" | translate}}</ion-col>\n\n            <ion-col col-6>\n\n                <ion-row margin-vertical>\n\n                    <ion-col col-3 text-center (click)="navigate()">\n\n                        <img src="assets/images/company_icon_4.png" height="30">\n\n                    </ion-col>\n\n                    <ion-col col-3 text-center (click)="shareVideo()">\n\n                        <ion-icon name="share" style="font-size: 33px; color: #690079"></ion-icon>\n\n                    </ion-col>\n\n                    <ion-col col-3 text-center (click)="call()">\n\n                        <img src="assets/images/company_icon_2.png" height="30">\n\n                    </ion-col>\n\n                    <ion-col col-3 text-center (click)="goToCoupons()">\n\n                        <img src="assets/images/company_icon_3.png" height="30">\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-item class="itemBack">\n\n            <ion-label fixed no-margin>\n\n                <img [src]="company.logo" *ngIf="company.logo">\n\n                <img src="assets/images/logo.png" *ngIf="!(company.logo)">\n\n            </ion-label>\n\n            <div item-content padding-vertical>\n\n                <div>\n\n                    <b>{{company.title}}</b>\n\n                    <ion-icon *ngFor="let star of company.stars" (click)="showRatePopup();" name="ios-star" [ngClass]="{\'yellow-star\': star === true, \'grey-star\': star === false}"></ion-icon>\n\n                </div>\n\n                <div class="smallText">{{company.address}}</div>\n\n                <div class="smallText" padding-top (click)="openedDescription = !openedDescription">\n\n                    <div *ngIf="company.description && !openedDescription">{{company.description | truncate }}</div>\n\n                    <div *ngIf="company.description && openedDescription">{{company.description}}</div>\n\n                </div>\n\n            </div>\n\n        </ion-item>\n\n\n\n        <ion-row ion-text color="primary" padding-horizontal><h5>{{"COMPANY.VISITORREVIEWS" | translate}}</h5></ion-row>\n\n\n\n        <div *ngFor="let video of company.videos">\n\n        <!--<div [virtualScroll]="company.videos">-->\n\n\n\n            <ion-row justify-content-start>\n\n            <!--<ion-row justify-content-start *virtualItem="let video">-->\n\n\n\n                <ion-col col-12 class="small-video">\n\n                    <div><video my-video [source]="video.url" controls controlsList="nodownload" poster="{{video.thumbnail}}"></video></div>\n\n                    <ion-row>\n\n                        <ion-col col-6 text-start><b>{{video.user.name}}</b></ion-col>\n\n                        <ion-col col-6 text-end>{{video.date}}</ion-col>\n\n                    </ion-row>\n\n                    <ion-row><ion-col col-12><b>{{video.pivot.tag}}</b></ion-col></ion-row>\n\n                </ion-col>\n\n\n\n            </ion-row>\n\n\n\n        </div>\n\n\n\n    </ion-grid>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <common-footer></common-footer>\n\n</ion-footer>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\company\company.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
            __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], CompanyPage);
    return CompanyPage;
}());

//# sourceMappingURL=company.js.map

/***/ })

});
//# sourceMappingURL=1.js.map