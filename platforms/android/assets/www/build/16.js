webpackJsonp([16],{

/***/ 841:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FacebookPageModule", function() { return FacebookPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__facebook__ = __webpack_require__(867);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var FacebookPageModule = /** @class */ (function () {
    function FacebookPageModule() {
    }
    FacebookPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__facebook__["a" /* FacebookPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__facebook__["a" /* FacebookPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], FacebookPageModule);
    return FacebookPageModule;
}());

//# sourceMappingURL=facebook.module.js.map

/***/ }),

/***/ 867:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FacebookPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__ = __webpack_require__(441);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var FacebookPage = /** @class */ (function () {
    function FacebookPage(navCtrl, navParams, viewCtrl, social, init, alertCtrl, translate, restangular, auth, domSanitizer, fb, utils) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.social = social;
        this.init = init;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.restangular = restangular;
        this.auth = auth;
        this.domSanitizer = domSanitizer;
        this.fb = fb;
        this.utils = utils;
        this.company_id = this.navParams.get('company_id');
        this.entity = this.navParams.get('entity');
        this.code = '';
        this.youtubeVideos = {
            explanation: { url: 'https://www.youtube.com/embed/693htHBw6S0', sanitizedUrl: this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/693htHBw6S0') },
            banner: { url: 'https://www.youtube.com/embed/5Q6Z5ZzYmNc', sanitizedUrl: this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/5Q6Z5ZzYmNc') },
            explanation_video: { url: 'https://www.youtube.com/embed/lDBwPu_wy3o', sanitizedUrl: this.domSanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/lDBwPu_wy3o') },
        };
        this.init.points$.subscribe(function (value) { return _this.points = value; });
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    // explanation
    FacebookPage.prototype.showExplanationVideo = function () {
        this.entity = 'explanation';
    };
    FacebookPage.prototype.closeModalForVideo = function () {
        if (this.entity === 'explanation' || this.entity === 'banner' || this.entity === 'explanation_video') {
            this.viewCtrl.dismiss();
        }
    };
    // all
    FacebookPage.prototype.closeModal = function () { this.viewCtrl.dismiss(); };
    // coupon
    FacebookPage.prototype.share = function () {
        this.social.shareViaFacebook('', '', this.utils.baseUrl + 'video?id=' + this.company.id + '&type=company&lang=' + this.language)
            .then(function (data) { return console.log(data); });
    };
    // code
    FacebookPage.prototype.validateCode = function () {
        var _this = this;
        if (this.code === '') {
            var alert_1 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.ENTERCODE"), buttons: ['OK'] });
            alert_1.present();
            return;
        }
        this.restangular.all('codes/validate').post({ code: this.code }).subscribe(function (data) {
            _this.param = { value: data.reward };
            _this.reward = data.reward;
            _this.code_id = data.id;
            _this.company = data.company;
            _this.entity = 'facebook';
            _this.code = '';
        });
    };
    // facebook
    FacebookPage.prototype.close = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.SURE") + "?",
            message: this.translate.instant("POPUPS.IFCLOSE"),
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () { }
                },
                {
                    text: 'OK',
                    handler: function () { _this.viewCtrl.dismiss(); }
                }
            ]
        });
        confirm.present();
    };
    FacebookPage.prototype.shareCode = function () {
        var _this = this;
        this.social.shareViaFacebook('', '', this.utils.baseUrl + 'video?id=' + this.company.id + '&type=company&lang=' + this.language).then(function (data) {
            console.log(data);
            // TODO: https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin/issues/864
            if (data === 'OK') {
                _this.restangular.one('codes', _this.code_id).one('users', _this.auth.id).customGET('bind')
                    .subscribe(function (response) {
                    setTimeout(function () {
                        _this.userReward = response.reward;
                        _this.entity = 'code_succeed';
                        _this.init.setPoints(_this.points + response.reward);
                    }, 7000);
                });
            }
        }).catch(function (err) {
            console.log(err);
            var facebookExists = true;
            if (Array.isArray(err)) {
                facebookExists = err.indexOf("com.facebook.katana") > -1;
            }
            else if (err === 'cancelled') {
                facebookExists = false;
            }
            if (!facebookExists) {
                var alert_2 = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.NOFACEBOOK"), buttons: ['OK'] });
                alert_2.present();
                return;
            }
            var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.WRONG"), buttons: ['OK'] });
            alert.present();
            return;
        });
    };
    FacebookPage.prototype.goToMap = function () {
        var _this = this;
        if (this.init.coordinates.lat !== 0 && this.init.coordinates.lng !== 0) {
            this.navCtrl.push('MapPage');
        }
        else {
            this.init.getLocation().subscribe(function (data) { return _this.navCtrl.push('MapPage'); });
        }
    };
    FacebookPage.prototype.goToShop = function () {
        this.navCtrl.push('CouponsPage', { company_id: 0 });
    };
    FacebookPage.prototype.ionViewWillEnter = function () {
        if (this.entity === 'coupon') {
            this.company = this.navParams.get('company');
        }
    };
    FacebookPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-facebook',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\facebook\facebook.html"*/'<ion-content [ngClass]="{\'picture-background\' : entity !== \'explanation\' && entity !== \'banner\' && entity !== \'explanation_video\'}">\n\n\n\n    <div class="close" *ngIf="entity !== \'facebook\' && entity !== \'explanation\' && entity !== \'banner\' && entity !== \'explanation_video\'" (click)="closeModal()">\n\n        <img src="assets/images/x.png" height="20">\n\n    </div>\n\n\n\n    <div class="close" *ngIf="entity === \'facebook\' && entity !== \'explanation\' && entity !== \'banner\' && entity !== \'explanation_video\'" (click)="close()">\n\n        <img src="assets/images/x.png" height="20">\n\n    </div>\n\n\n\n    <!--Coupon-->\n\n\n\n    <div class="popup-content" *ngIf="entity === \'coupon\'">\n\n\n\n        <div class="big-text coupon-text">{{"FACEBOOK.THANKYOU" | translate}}</div>\n\n        <div class="big-text">{{"FACEBOOK.FORUSINGIT" | translate}}!</div>\n\n        <div class="coupon-text">{{"FACEBOOK.PLEASESHARE" | translate}}</div>\n\n\n\n        <button ion-button class="share-button" small (click)="share(); $event.stopPropagation();">\n\n            <img src="assets/images/share.png">\n\n        </button>\n\n\n\n    </div>\n\n\n\n    <!--Code-->\n\n\n\n    <div class="popup-content" text-center *ngIf="entity === \'code\'" (click)="$event.stopPropagation();">\n\n\n\n        <div class="average-text">{{"FACEBOOK.PLEASEENTERCODE" | translate}}</div>\n\n\n\n        <div>\n\n            <ion-input text-center placeholder=\'{{"FACEBOOK.CODE" | translate}}\' class="code-input" type="text" [(ngModel)]="code"></ion-input>\n\n        </div>\n\n\n\n        <button class="confirm-button" *ngIf="language === \'he\'" ion-button small (click)="validateCode(); $event.stopPropagation();">\n\n            {{"ALL.SUBMIT" | translate}}&nbsp;<img src="assets/images/arrows_left.png" height="15">\n\n        </button>\n\n\n\n        <button class="confirm-button" *ngIf="language === \'en\'" ion-button small (click)="validateCode(); $event.stopPropagation();">\n\n            {{"ALL.SUBMIT" | translate}}&nbsp;<img src="assets/images/arrows_right.png" height="15">\n\n        </button>\n\n\n\n    </div>\n\n\n\n    <!--Facebook-->\n\n\n\n    <div class="popup-content" *ngIf="entity === \'facebook\'" padding-top>\n\n\n\n        <div margin-top>{{"FACEBOOK.SHAREUS" | translate}}</div>\n\n        <div>{{"FACEBOOK.GETPOINTS" | translate:param }}</div>\n\n\n\n        <button margin-top ion-button small class="share-button" (click)="shareCode(); $event.stopPropagation();">\n\n            <img src="assets/images/share.png">\n\n        </button>\n\n\n\n    </div>\n\n\n\n    <div class="popup-content" *ngIf="entity === \'code_succeed\'" padding-top>\n\n\n\n        <div class="big-text">{{"POPUPS.RECEIVED" | translate}}</div>\n\n        <div class="big-text">{{userReward}} {{"POPUPS.POINTS" | translate }}</div>\n\n\n\n        <div>\n\n            <button class="confirm-button" ion-button small (click)="goToMap(); $event.stopPropagation();">\n\n                {{"POPUPS.MAP" | translate}}\n\n            </button>\n\n            <button class="confirm-button" ion-button small (click)="goToShop(); $event.stopPropagation();">\n\n                {{"POPUPS.SHOP" | translate}}\n\n            </button>\n\n        </div>\n\n    </div>\n\n\n\n    <!--Points-->\n\n\n\n    <div class="popup-content" *ngIf="entity === \'points\'" padding-top>\n\n\n\n        <div margin-top>{{"FACEBOOK.YOUHAVEONLY" | translate}}</div>\n\n        <div>{{init.user.points}} {{"ALL.POINTS" | translate}}.</div>\n\n        <div>{{"FACEBOOK.GET1" | translate}}</div>\n\n        <div>{{"FACEBOOK.GET2" | translate}}</div>\n\n\n\n        <button class="confirm-button" *ngIf="language === \'he\'" ion-button small (click)="showExplanationVideo(); $event.stopPropagation();">\n\n            {{"FACEBOOK.WATCH" | translate}}\n\n        </button>\n\n\n\n        <button class="confirm-button" *ngIf="language === \'en\'" ion-button small (click)="showExplanationVideo(); $event.stopPropagation();">\n\n            {{"FACEBOOK.WATCH" | translate}}\n\n        </button>\n\n    </div>\n\n\n\n    <div class="popup-content-block" *ngIf="entity === \'3dollars\'" padding-top>\n\n\n\n        <div class="average-text">\n\n            {{"POPUPS.3DOLLARS" | translate}}\n\n            <img src="assets/images/coin.png" height="14">\n\n            {{"POPUPS.3DOLLARS2" | translate}}\n\n        </div>\n\n\n\n    </div>\n\n\n\n    <!--Explanation-->\n\n\n\n    <div *ngIf="entity === \'explanation\'">\n\n\n\n        <div class="video-content">\n\n            <div class="close-video-button" (click)="closeModalForVideo()">\n\n                <img src="assets/images/x.png" height="20">\n\n            </div>\n\n            <div class="video-preloader">\n\n                <div><ion-spinner name="ios"></ion-spinner></div>\n\n                <div>{{"FACEBOOK.LOADING" | translate}}</div>\n\n            </div>\n\n            <div style="z-index: 100;">\n\n                <iframe class="video-frame" [src]="youtubeVideos.explanation.sanitizedUrl" frameborder="0" allowfullscreen></iframe>\n\n            </div>\n\n        </div>\n\n\n\n    </div>\n\n\n\n    <!--Banner-->\n\n\n\n    <div *ngIf="entity === \'banner\'">\n\n\n\n        <div class="video-content">\n\n            <div class="close-video-button" (click)="closeModalForVideo()">\n\n                <img src="assets/images/x.png" height="20">\n\n            </div>\n\n            <div class="video-preloader">\n\n                <div><ion-spinner name="ios"></ion-spinner></div>\n\n                <div>{{"FACEBOOK.LOADING" | translate}}</div>\n\n            </div>\n\n            <div style="z-index: 100;">\n\n                <iframe class="video-frame" [src]="youtubeVideos.banner.sanitizedUrl" frameborder="0" allowfullscreen></iframe>\n\n            </div>\n\n        </div>\n\n\n\n    </div>\n\n\n\n    <!--Explanation video-->\n\n\n\n    <div *ngIf="entity === \'explanation_video\'">\n\n\n\n        <div class="video-content">\n\n            <div class="close-video-button" (click)="closeModalForVideo()">\n\n                <img src="assets/images/x.png" height="20">\n\n            </div>\n\n            <div class="video-preloader">\n\n                <div><ion-spinner name="ios"></ion-spinner></div>\n\n                <div>{{"FACEBOOK.LOADING" | translate}}</div>\n\n            </div>\n\n            <div style="z-index: 100;">\n\n                <iframe class="video-frame" [src]="youtubeVideos.explanation_video.sanitizedUrl" frameborder="0" allowfullscreen></iframe>\n\n            </div>\n\n        </div>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\facebook\facebook.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__["a" /* UtilsProvider */]])
    ], FacebookPage);
    return FacebookPage;
}());

//# sourceMappingURL=facebook.js.map

/***/ })

});
//# sourceMappingURL=16.js.map