webpackJsonp([24],{

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectivesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_video_add_video__ = __webpack_require__(800);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_picture_add_picture__ = __webpack_require__(801);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__background_image_background_image__ = __webpack_require__(802);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__my_video_my_video__ = __webpack_require__(803);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var DirectivesModule = /** @class */ (function () {
    function DirectivesModule() {
    }
    DirectivesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__add_video_add_video__["a" /* AddVideoDirective */], __WEBPACK_IMPORTED_MODULE_2__add_picture_add_picture__["a" /* AddPictureDirective */], __WEBPACK_IMPORTED_MODULE_3__background_image_background_image__["a" /* BackgroundImageDirective */], __WEBPACK_IMPORTED_MODULE_4__my_video_my_video__["a" /* MyVideoDirective */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__add_video_add_video__["a" /* AddVideoDirective */], __WEBPACK_IMPORTED_MODULE_2__add_picture_add_picture__["a" /* AddPictureDirective */], __WEBPACK_IMPORTED_MODULE_3__background_image_background_image__["a" /* BackgroundImageDirective */], __WEBPACK_IMPORTED_MODULE_4__my_video_my_video__["a" /* MyVideoDirective */]]
        })
    ], DirectivesModule);
    return DirectivesModule;
}());

//# sourceMappingURL=directives.module.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_onesignal__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__init_service_init_service__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var PushService = /** @class */ (function () {
    function PushService(auth, restangular, oneSignal, initService, modalCtrl, platform) {
        this.auth = auth;
        this.restangular = restangular;
        this.oneSignal = oneSignal;
        this.initService = initService;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
    }
    PushService.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                if (this.platform.is('cordova')) {
                    this.oneSignal.startInit('e4ed69d6-511b-4601-ab3b-0f5b615f817f', '595323574268');
                    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
                    this.oneSignal.handleNotificationReceived().subscribe(function (data) {
                        console.log('handleNotificationReceived', JSON.stringify(data));
                        if (data && data.payload && data.payload.additionalData && data.payload.additionalData.topic) {
                            if (data.payload.additionalData.topic == 'points') {
                                _this.initService.getUser();
                            }
                        }
                    });
                    this.oneSignal.handleNotificationOpened().subscribe(function (data) {
                        console.log('handleNotificationOpened', JSON.stringify(data));
                        if (data && data.notification && data.notification.payload && data.notification.payload.additionalData && data.notification.payload.additionalData.topic) {
                            if (data.notification.payload.additionalData.topic == 'registration') {
                                var modal = _this.modalCtrl.create('FacebookPage', { entity: 'banner' });
                                modal.present();
                            }
                        }
                    });
                    this.oneSignal.endInit();
                }
                return [2 /*return*/];
            });
        });
    };
    PushService.prototype.logout = function () {
        this.restangular.one('users', this.auth.id).customGET('push').subscribe(function (data) { return console.log(data); });
    };
    PushService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_5__init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["n" /* Platform */]])
    ], PushService);
    return PushService;
}());

//# sourceMappingURL=push-service.js.map

/***/ }),

/***/ 227:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 227;

/***/ }),

/***/ 271:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/companies/companies.module": [
		833,
		23
	],
	"../pages/company-register-additional/company-register-additional.module": [
		832,
		22
	],
	"../pages/company-register/company-register.module": [
		834,
		21
	],
	"../pages/company/company.module": [
		837,
		1
	],
	"../pages/contact/contact.module": [
		835,
		0
	],
	"../pages/coupon/coupon.module": [
		836,
		20
	],
	"../pages/coupons/coupons.module": [
		838,
		19
	],
	"../pages/edit-profile/edit-profile.module": [
		839,
		18
	],
	"../pages/enter/enter.module": [
		840,
		17
	],
	"../pages/facebook/facebook.module": [
		841,
		16
	],
	"../pages/faq/faq.module": [
		844,
		15
	],
	"../pages/feed/feed.module": [
		842,
		14
	],
	"../pages/home/home.module": [
		843,
		13
	],
	"../pages/lead/lead.module": [
		845,
		12
	],
	"../pages/login/login.module": [
		847,
		11
	],
	"../pages/map/map.module": [
		846,
		10
	],
	"../pages/notifications/notifications.module": [
		848,
		9
	],
	"../pages/profile/profile.module": [
		849,
		8
	],
	"../pages/rate/rate.module": [
		851,
		7
	],
	"../pages/register/register.module": [
		850,
		6
	],
	"../pages/reviews/reviews.module": [
		852,
		5
	],
	"../pages/upload/upload.module": [
		853,
		4
	],
	"../pages/user/user.module": [
		854,
		3
	],
	"../pages/users/users.module": [
		855,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 271;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var UtilsProvider = /** @class */ (function () {
    // baseUrl:string = 'http://vt.kartisim.co.il/';
    function UtilsProvider(alertCtrl, diagnostic, translate, storage, platform) {
        this.alertCtrl = alertCtrl;
        this.diagnostic = diagnostic;
        this.translate = translate;
        this.storage = storage;
        this.platform = platform;
        this.progress = 0;
        this._progress = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.progress$ = this._progress.asObservable();
        this.uploadProcess = false;
        this._uploadProcess = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.uploadProcess$ = this._uploadProcess.asObservable();
        this._language = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"]('en');
        this.language$ = this._language.asObservable();
        this.baseUrl = 'http://app.y-travel.net/';
    }
    Object.defineProperty(UtilsProvider.prototype, "language", {
        get: function () { return this._language.getValue(); },
        enumerable: true,
        configurable: true
    });
    ;
    UtilsProvider.prototype.setLanguage = function (value) {
        var _this = this;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.set('language', value)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this._language.next(value)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.translate.use(value)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.platform.setDir(value === 'en' ? 'ltr' : 'rtl', true)];
                    case 4:
                        _a.sent();
                        resolve();
                        return [2 /*return*/];
                }
            });
        }); });
    };
    UtilsProvider.prototype.isAnyFieldEmpty = function (x) {
        var keys = Object.getOwnPropertyNames(x);
        for (var key in keys) {
            if (x[keys[key]] === '' || typeof x[keys[key]] === 'undefined' || x[keys[key]] === null) {
                var alert_1 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK'] });
                alert_1.present();
                return true;
            }
        }
        return false;
    };
    UtilsProvider.prototype.isLocationAvailable = function () {
        var _this = this;
        return new __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"](function (observer) {
            _this.diagnostic.isLocationAuthorized().then(function (isAuthorized) {
                console.log('isLocationAuthorized', isAuthorized);
                if (!isAuthorized) {
                    _this.diagnostic.requestLocationAuthorization().then(function (status) {
                        if (status === "GRANTED") {
                            _this.diagnostic.isLocationEnabled().then(function (isEnabled) {
                                console.log('isLocationEnabled', isEnabled);
                                if (isEnabled) {
                                    observer.next({ status: 'success' });
                                    observer.complete();
                                }
                                else {
                                    observer.next({ status: 'error', message: "Location is disabled" });
                                    observer.complete();
                                }
                            });
                        }
                        else {
                            observer.next({ status: 'error', message: "Location is not authorized" });
                            observer.complete();
                        }
                    });
                }
                else {
                    _this.diagnostic.isLocationEnabled().then(function (isEnabled) {
                        console.log('isLocationEnabled', isEnabled);
                        if (isEnabled) {
                            observer.next({ status: 'success' });
                            observer.complete();
                        }
                        else {
                            observer.next({ status: 'error', message: "Location is disabled" });
                            observer.complete();
                        }
                    });
                }
            });
        });
    };
    UtilsProvider.prototype.setProgress = function (value) {
        this.progress = value;
        this._progress.next(value);
    };
    UtilsProvider.prototype.setUploadProcess = function (value) {
        this.uploadProcess = value;
        this._uploadProcess.next(value);
    };
    UtilsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */]])
    ], UtilsProvider);
    return UtilsProvider;
}());

//# sourceMappingURL=utils.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__picture_header_picture_header__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_header_common_header__ = __webpack_require__(798);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_footer_common_footer__ = __webpack_require__(799);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__picture_header_picture_header__["a" /* PictureHeaderComponent */], __WEBPACK_IMPORTED_MODULE_3__common_header_common_header__["a" /* CommonHeaderComponent */], __WEBPACK_IMPORTED_MODULE_4__common_footer_common_footer__["a" /* CommonFooterComponent */]],
            // imports: [DirectivesModule, TranslateModule],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild([
                    __WEBPACK_IMPORTED_MODULE_2__picture_header_picture_header__["a" /* PictureHeaderComponent */],
                    __WEBPACK_IMPORTED_MODULE_3__common_header_common_header__["a" /* CommonHeaderComponent */],
                    __WEBPACK_IMPORTED_MODULE_4__common_footer_common_footer__["a" /* CommonFooterComponent */],
                ]),
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__picture_header_picture_header__["a" /* PictureHeaderComponent */], __WEBPACK_IMPORTED_MODULE_3__common_header_common_header__["a" /* CommonHeaderComponent */], __WEBPACK_IMPORTED_MODULE_4__common_footer_common_footer__["a" /* CommonFooterComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 442:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(443);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(447);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export initializeApplication */
/* unused harmony export RestangularConfigFactory */
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(827);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_media_capture__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__agm_core__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_transfer__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_diagnostic__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_call_number__ = __webpack_require__(439);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_launch_navigator__ = __webpack_require__(440);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_social_sharing__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_angular2_qrcode__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_screen_orientation__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ngx_translate_http_loader__ = __webpack_require__(828);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_native_geocoder__ = __webpack_require__(830);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_file__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_push_service_push_service__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_onesignal__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__angular_common_http__ = __webpack_require__(831);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__directives_directives_module__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_facebook__ = __webpack_require__(441);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
































function initializeApplication(storage, utils) {
    var _this = this;
    return function () {
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var language, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, 3, 4]);
                        return [4 /*yield*/, storage.get('language')];
                    case 1:
                        language = _a.sent();
                        if (language == null) {
                            language = 'en';
                        }
                        utils.setLanguage(language);
                        return [3 /*break*/, 4];
                    case 2:
                        err_1 = _a.sent();
                        console.log('initializeApplicationError', err_1);
                        return [3 /*break*/, 4];
                    case 3:
                        resolve();
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
}
function RestangularConfigFactory(RestangularProvider, App, LoadingController, AlertController, AuthServiceProvider, UtilsProvider) {
    var loading;
    RestangularProvider.addFullRequestInterceptor(function (element, operation, path, url, headers, params) {
        loading = LoadingController.create({ content: 'Please wait...' });
        loading.present();
        UtilsProvider.language$.subscribe(function (language) {
            params.lang = language;
            return { params: params, headers: headers, element: element };
        });
    });
    // RestangularProvider.setBaseUrl('http://vt.test/api/v1');
    // RestangularProvider.setBaseUrl('http://vt.kartisim.co.il/api/v1');
    RestangularProvider.setBaseUrl('https://app.y-travel.net/api/v1');
    // Set initial default header
    RestangularProvider.setDefaultHeaders({ Authorization: "Bearer " + AuthServiceProvider.getToken() });
    // Subscribe to token change
    AuthServiceProvider.token$.subscribe(function (token) { return RestangularProvider.setDefaultHeaders({ Authorization: "Bearer " + token }); });
    RestangularProvider.addResponseInterceptor(function (data, operation, what, url, response) {
        loading.dismiss();
        if (data.data) {
            console.log(url, data.data);
        }
        var extractedData = data.data;
        if (extractedData === null) {
            extractedData = [];
        }
        return extractedData;
    });
    RestangularProvider.addErrorInterceptor(function (response, subject, responseHandler) {
        loading.dismiss();
        console.log(response);
        if (response.error && response.error.message) {
            AlertController.create({ title: response.error.message, buttons: ['OK'] }).present();
            return true;
        }
        if (response.data) {
            var title = response.data.message ? response.data.message : 'Server error';
            var message = '';
            if (response.data.error && response.data.error.message) {
                message += response.data.error.message;
            }
            if (response.data.error && response.data.error.fields) {
                for (var errors in response.data.error.fields) {
                    for (var error in response.data.error.fields[errors]) {
                        message += ' ' + response.data.error.fields[errors][error];
                    }
                }
            }
            AlertController.create({ title: title, message: message, buttons: ['OK'] }).present();
            if (response && response.data && response.data.status && response.data.status == 401) {
                AuthServiceProvider.logout();
                App.getRootNavs()[0].setRoot('LoginPage');
            }
            return true;
        }
        return true;
    });
}
function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_23__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/company-register-additional/company-register-additional.module#CompanyRegisterAdditionalPageModule', name: 'CompanyRegisterAdditionalPage', segment: 'company-register-additional', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/companies/companies.module#CompaniesPageModule', name: 'CompaniesPage', segment: 'companies', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/company-register/company-register.module#CompanyRegisterPageModule', name: 'CompanyRegisterPage', segment: 'company-register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/coupon/coupon.module#CouponPageModule', name: 'CouponPage', segment: 'coupon', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/company/company.module#CompanyPageModule', name: 'CompanyPage', segment: 'company', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/coupons/coupons.module#CouponsPageModule', name: 'CouponsPage', segment: 'coupons', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/edit-profile/edit-profile.module#EditProfilePageModule', name: 'EditProfilePage', segment: 'edit-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/enter/enter.module#EnterPageModule', name: 'EnterPage', segment: 'enter', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/facebook/facebook.module#FacebookPageModule', name: 'FacebookPage', segment: 'facebook', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feed/feed.module#FeedPageModule', name: 'FeedPage', segment: 'feed', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/faq/faq.module#FaqPageModule', name: 'FaqPage', segment: 'faq', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lead/lead.module#LeadPageModule', name: 'LeadPage', segment: 'lead', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/map/map.module#MapPageModule', name: 'MapPage', segment: 'map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notifications/notifications.module#NotificationsPageModule', name: 'NotificationsPage', segment: 'notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/rate/rate.module#RatePageModule', name: 'RatePage', segment: 'rate', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reviews/reviews.module#ReviewsPageModule', name: 'ReviewsPage', segment: 'reviews', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/upload/upload.module#UploadPageModule', name: 'UploadPage', segment: 'upload', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user/user.module#UserPageModule', name: 'UserPage', segment: 'user', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/users/users.module#UsersPageModule', name: 'UsersPage', segment: 'users', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                    name: "__mydb",
                    driverOrder: ["sqlite", "websql", "indexeddb"]
                }),
                __WEBPACK_IMPORTED_MODULE_7_ngx_restangular__["RestangularModule"].forRoot([__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_18__providers_auth_service_auth_service__["a" /* AuthServiceProvider */], __WEBPACK_IMPORTED_MODULE_17__providers_utils_utils__["a" /* UtilsProvider */]], RestangularConfigFactory),
                __WEBPACK_IMPORTED_MODULE_10__agm_core__["a" /* AgmCoreModule */].forRoot({ apiKey: "AIzaSyA4uXPidpv7gDsUIXwS30CMQNs5M-t6DOs", libraries: ["places"] }),
                __WEBPACK_IMPORTED_MODULE_20_angular2_qrcode__["a" /* QRCodeModule */],
                __WEBPACK_IMPORTED_MODULE_28__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_29__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_30__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_22__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_22__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_28__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_media_capture__["a" /* MediaCapture */],
                __WEBPACK_IMPORTED_MODULE_17__providers_utils_utils__["a" /* UtilsProvider */],
                __WEBPACK_IMPORTED_MODULE_18__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_diagnostic__["a" /* Diagnostic */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
                __WEBPACK_IMPORTED_MODULE_19__providers_init_service_init_service__["a" /* InitServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_26__providers_push_service_push_service__["a" /* PushService */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_facebook__["a" /* Facebook */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_onesignal__["a" /* OneSignal */],
                {
                    'provide': __WEBPACK_IMPORTED_MODULE_1__angular_core__["APP_INITIALIZER"],
                    'useFactory': initializeApplication,
                    'deps': [__WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_17__providers_utils_utils__["a" /* UtilsProvider */]],
                    'multi': true
                },
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PictureHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PictureHeaderComponent = /** @class */ (function () {
    function PictureHeaderComponent(zone, utils, init, navCtrl) {
        var _this = this;
        this.zone = zone;
        this.utils = utils;
        this.init = init;
        this.navCtrl = navCtrl;
        this.full = 0;
        this.empty = 100;
        this.uploadProcess = false;
        this.avatar = '';
        this.points = 0;
        this.utils.progress$.subscribe(function (full) {
            _this.zone.run(function () {
                _this.full = full;
                _this.empty = 100 - full;
            });
        });
        this.utils.uploadProcess$.subscribe(function (value) { return _this.uploadProcess = value; });
        this.init.avatar$.subscribe(function (value) { return _this.avatar = value; }); // same code here
        this.init.points$.subscribe(function (value) { return _this.points = value; });
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    PictureHeaderComponent.prototype.goToUser = function () {
        this.navCtrl.push('ProfilePage');
    };
    PictureHeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'picture-header',template:/*ion-inline-start:"G:\github\vt-client-2\src\components\picture-header\picture-header.html"*/'<ion-navbar color="primary" text-center dir="rtl">\n\n    <ion-buttons right>\n\n        <button ion-button clear (click)="goToUser()">\n\n            <div *ngIf="avatar !== \'\'" class="avatar" background-image [source]="avatar" height="34"></div>\n\n            <div *ngIf="avatar === \'\'" class="avatar" background-image [source]="\'assets/images/avatar.jpg\'" height="34"></div>\n\n        </button>\n\n    </ion-buttons>\n\n    <ion-title>\n\n        <img src="assets/images/header_logo.png" style="height: 45px; padding-top: 5px;" *ngIf="language === \'he\'">\n\n        <img src="assets/images/header_logo_en.png" style="height: 45px; padding-top: 5px;" *ngIf="language === \'en\'">\n\n    </ion-title>\n\n    <ion-buttons left>\n\n        <div class="points" float-left>\n\n            <div>{{points}}</div>\n\n            <div>$Y</div>\n\n        </div>\n\n        <button ion-button menuToggle right class="menu-button">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n    </ion-buttons>\n\n    <div class="row100" *ngIf="uploadProcess">\n\n        <div class="progress-bar-empty" [style.width]="empty + \'%\'"></div>\n\n        <div class="progress-bar-full" [style.width]="full + \'%\'"></div>\n\n    </div>\n\n</ion-navbar>'/*ion-inline-end:"G:\github\vt-client-2\src\components\picture-header\picture-header.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */]])
    ], PictureHeaderComponent);
    return PictureHeaderComponent;
}());

//# sourceMappingURL=picture-header.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InitServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var InitServiceProvider = /** @class */ (function () {
    function InitServiceProvider(restangular, storage, auth, geolocation, utils, platform) {
        this.restangular = restangular;
        this.storage = storage;
        this.auth = auth;
        this.geolocation = geolocation;
        this.utils = utils;
        this.platform = platform;
        this.coordinates = { lat: 0, lng: 0 };
        this._banners = new __WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.banners$ = this._banners.asObservable();
        this._points = new __WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.points$ = this._points.asObservable();
        this._faq = new __WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.faq$ = this._faq.asObservable();
        this._company_categories = new __WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.company_categories$ = this._company_categories.asObservable();
        this._regions = new __WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.regions$ = this._regions.asObservable();
        this._avatar = new __WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.avatar$ = this._avatar.asObservable();
        this._name = new __WEBPACK_IMPORTED_MODULE_9_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.name$ = this._name.asObservable();
    }
    InitServiceProvider.prototype.onInitApp = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.getLocation().subscribe(function (data) { return console.log(data); });
            if (_this.auth.getToken() !== '') {
                _this.restangular.all('init').customGET().subscribe(function (data) {
                    _this.company_categories = data.company_categories;
                    _this._company_categories.next(data.company_categories);
                    _this.regions = data.regions;
                    _this._regions.next(data.regions);
                    _this.user = data.user;
                    _this.following = data.following;
                    _this.phone = data.phone;
                    _this.countries = data.countries;
                    _this.faq = data.faq;
                    _this._faq.next(data.faq);
                    _this.banners = data.banners;
                    _this._banners.next(data.banners);
                    _this.setPoints(data.user.points);
                    _this.setAvatar(data.user.avatar);
                    _this.setName(data.user.name);
                    resolve();
                });
            }
            else {
                resolve();
            }
        });
    };
    InitServiceProvider.prototype.getUser = function () {
        var _this = this;
        this.restangular.one('users', this.user.id).get().subscribe(function (data) {
            _this.user = data;
            _this.setPoints(data.points);
        });
    };
    InitServiceProvider.prototype.setPoints = function (value) {
        this.points = value;
        this._points.next(value);
    };
    InitServiceProvider.prototype.setAvatar = function (value) {
        this.avatar = value;
        this._avatar.next(value);
    };
    InitServiceProvider.prototype.setName = function (value) {
        this.name = value;
        this._name.next(value);
    };
    InitServiceProvider.prototype.getLocation = function () {
        var _this = this;
        return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
            if (_this.platform.is('cordova')) {
                _this.utils.isLocationAvailable().subscribe(function (response) {
                    console.log(response);
                    if (response.status === 'success') {
                        _this.geolocation.getCurrentPosition().then(function (response) {
                            console.log(response.coords.latitude, response.coords.longitude);
                            _this.coordinates.lat = response.coords.latitude;
                            _this.coordinates.lng = response.coords.longitude;
                            observer.next(_this.coordinates);
                            observer.complete();
                        }).catch(function (error) {
                            observer.next({ status: 'error', message: "Can't get location" });
                            observer.complete();
                        });
                    }
                    else {
                        observer.next({ status: 'error', message: "Location is not available" });
                        observer.complete();
                    }
                });
            }
            else {
                _this.geolocation.getCurrentPosition().then(function (response) {
                    // console.log(response.coords.latitude, response.coords.longitude);
                    _this.coordinates.lat = response.coords.latitude;
                    _this.coordinates.lng = response.coords.longitude;
                    observer.next(_this.coordinates);
                    observer.complete();
                }).catch(function (error) {
                    observer.next({ status: 'error', message: "Can't get location" });
                    observer.complete();
                });
            }
        });
    };
    InitServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_7__utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["n" /* Platform */]])
    ], InitServiceProvider);
    return InitServiceProvider;
}());

//# sourceMappingURL=init-service.js.map

/***/ }),

/***/ 798:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CommonHeaderComponent = /** @class */ (function () {
    function CommonHeaderComponent(zone, utils, init, navCtrl) {
        var _this = this;
        this.zone = zone;
        this.utils = utils;
        this.init = init;
        this.navCtrl = navCtrl;
        this.full = 0;
        this.empty = 100;
        this.uploadProcess = false;
        this.avatar = '';
        this.points = 0;
        this.utils.progress$.subscribe(function (full) {
            _this.zone.run(function () {
                _this.full = full;
                _this.empty = 100 - full;
            });
        });
        this.utils.uploadProcess$.subscribe(function (value) { return _this.uploadProcess = value; });
        this.init.avatar$.subscribe(function (value) { return _this.avatar = value; });
        this.init.points$.subscribe(function (value) { return _this.points = value; });
    }
    CommonHeaderComponent.prototype.goToUser = function () {
        this.navCtrl.push('ProfilePage');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], CommonHeaderComponent.prototype, "title", void 0);
    CommonHeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'common-header',template:/*ion-inline-start:"G:\github\vt-client-2\src\components\common-header\common-header.html"*/'<ion-navbar color="primary" text-center dir="rtl">\n\n    <ion-buttons right>\n\n        <button ion-button clear (click)="goToUser()">\n\n            <div *ngIf="avatar !== \'\'" class="avatar" background-image [source]="avatar" height="34"></div>\n\n            <div *ngIf="avatar === \'\'" class="avatar" background-image [source]="\'assets/images/avatar.jpg\'" height="34"></div>\n\n        </button>\n\n    </ion-buttons>\n\n    <ion-title>{{title}}</ion-title>\n\n    <ion-buttons left>\n\n        <div class="points" float-left>\n\n            <div>{{points}}</div>\n\n            <div>$Y</div>\n\n        </div>\n\n        <button ion-button menuToggle right class="menu-button">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n    </ion-buttons>\n\n    <div class="row100" *ngIf="uploadProcess">\n\n        <div class="progress-bar-empty" [style.width]="empty + \'%\'"></div>\n\n        <div class="progress-bar-full" [style.width]="full + \'%\'"></div>\n\n    </div>\n\n</ion-navbar>'/*ion-inline-end:"G:\github\vt-client-2\src\components\common-header\common-header.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */]])
    ], CommonHeaderComponent);
    return CommonHeaderComponent;
}());

//# sourceMappingURL=common-header.js.map

/***/ }),

/***/ 799:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonFooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CommonFooterComponent = /** @class */ (function () {
    function CommonFooterComponent(navCtrl, init) {
        this.navCtrl = navCtrl;
        this.init = init;
    }
    CommonFooterComponent.prototype.goToHome = function () {
        this.navCtrl.setRoot('HomePage');
    };
    CommonFooterComponent.prototype.goToCoupons = function () {
        this.navCtrl.push('CouponsPage', { company_id: 0 });
    };
    CommonFooterComponent.prototype.goToNetwork = function () {
        this.navCtrl.push('UsersPage');
    };
    CommonFooterComponent.prototype.goToMap = function () {
        var _this = this;
        if (this.init.coordinates.lat !== 0 && this.init.coordinates.lng !== 0) {
            this.navCtrl.push('MapPage');
        }
        else {
            this.init.getLocation().subscribe(function (data) { return _this.navCtrl.push('MapPage'); });
        }
    };
    CommonFooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'common-footer',template:/*ion-inline-start:"G:\github\vt-client-2\src\components\common-footer\common-footer.html"*/'<ion-toolbar no-padding>\n\n    <ion-grid>\n\n        <ion-row text-center>\n\n            <ion-col (click)="goToHome()">\n\n                <div><img src="assets/images/home.png"></div>\n\n                <div ion-text color="primary">{{"MENU.HOMEPAGE" | translate}}</div>\n\n            </ion-col>\n\n            <ion-col (click)="goToCoupons()">\n\n                <div><img src="assets/images/gift.png"></div>\n\n                <div ion-text color="primary">{{"MENU.SHOP" | translate}}</div>\n\n            </ion-col>\n\n            <ion-col (click)="goToMap()">\n\n                <div><img src="assets/images/map.png"></div>\n\n                <div ion-text color="primary">{{"MENU.MAP" | translate}}</div>\n\n            </ion-col>\n\n            <ion-col (click)="goToNetwork()">\n\n                <div><img src="assets/images/social_network.png"></div>\n\n                <div ion-text color="primary">{{"MENU.NETWORK" | translate}}</div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-grid>\n\n</ion-toolbar>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\components\common-footer\common-footer.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__["a" /* InitServiceProvider */]])
    ], CommonFooterComponent);
    return CommonFooterComponent;
}());

//# sourceMappingURL=common-footer.js.map

/***/ }),

/***/ 800:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddVideoDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_media_capture__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AddVideoDirective = /** @class */ (function () {
    function AddVideoDirective(alertCtrl, camera, diagnostic, mediaCapture, translate, utils) {
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.diagnostic = diagnostic;
        this.mediaCapture = mediaCapture;
        this.translate = translate;
        this.utils = utils;
        this.targetVideo = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddVideoDirective.prototype.onClick = function (event) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.CHOOSESOURCE"),
            buttons: [
                {
                    text: 'Folder',
                    handler: function () { _this.selectVideo(); }
                },
                {
                    text: 'Camera',
                    handler: function () { _this.checkPermissions(); }
                }
            ]
        });
        alert.present();
    };
    AddVideoDirective.prototype.selectVideo = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            mediaType: this.camera.MediaType.VIDEO,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            var construct = { fullPath: '', name: '', type: '' };
            construct.fullPath = imageData;
            construct.name = imageData.substr(imageData.lastIndexOf('/') + 1);
            construct.type = 'video/mp4';
            _this.targetVideo.emit(construct);
        }, function (err) {
            console.log('err', err);
        });
    };
    AddVideoDirective.prototype.checkPermissions = function () {
        var _this = this;
        this.diagnostic.isCameraAuthorized(true).then(function (isAuthorized) {
            if (!isAuthorized) {
                _this.diagnostic.requestCameraAuthorization(true).then(function () { return _this.record(); });
            }
            else {
                _this.record();
            }
        }).catch(function (error) { return console.log('Error in camera authorization', error); });
    };
    AddVideoDirective.prototype.record = function () {
        var _this = this;
        var options = { limit: 1, duration: 30 };
        this.mediaCapture.captureVideo(options).then(function (data) {
            console.log(data);
            _this.targetVideo.emit(data[0]);
        }, function (err) { return console.error(err); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetVideo'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], AddVideoDirective.prototype, "targetVideo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], AddVideoDirective.prototype, "onClick", null);
    AddVideoDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[add-video]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_media_capture__["a" /* MediaCapture */],
            __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__["a" /* UtilsProvider */]])
    ], AddVideoDirective);
    return AddVideoDirective;
}());

//# sourceMappingURL=add-video.js.map

/***/ }),

/***/ 801:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPictureDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddPictureDirective = /** @class */ (function () {
    function AddPictureDirective(alertCtrl, camera, translate, utils) {
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.translate = translate;
        this.utils = utils;
        this.targetLogo = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddPictureDirective.prototype.onClick = function (event) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.CHOOSESOURCE"),
            buttons: [
                {
                    text: 'Folder',
                    handler: function () {
                        _this.makeLogo(0);
                    }
                },
                {
                    text: 'Camera',
                    handler: function () {
                        _this.makeLogo(1);
                    }
                }
            ]
        });
        alert.present();
    };
    AddPictureDirective.prototype.makeLogo = function (x) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: x === 0 ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA,
            targetWidth: 600,
            targetHeight: 600,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            // this.logo = imageData;
            _this.targetLogo.emit({ url: imageData, type: 'image/jpeg' });
        }, function (err) {
            console.log('err', err);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetLogo'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], AddPictureDirective.prototype, "targetLogo", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], AddPictureDirective.prototype, "onClick", null);
    AddPictureDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[add-picture]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */]])
    ], AddPictureDirective);
    return AddPictureDirective;
}());

//# sourceMappingURL=add-picture.js.map

/***/ }),

/***/ 802:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackgroundImageDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BackgroundImageDirective = /** @class */ (function () {
    function BackgroundImageDirective(el) {
        this.el = el;
    }
    BackgroundImageDirective.prototype.ngOnChanges = function (changes) {
        for (var key in changes) {
            if (key == 'source') {
                if (this.source != null && this.source != undefined) {
                    this.el.nativeElement.style.background = "url('" + this.source + "') no-repeat top left / cover";
                }
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], BackgroundImageDirective.prototype, "source", void 0);
    BackgroundImageDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[background-image]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], BackgroundImageDirective);
    return BackgroundImageDirective;
}());

//# sourceMappingURL=background-image.js.map

/***/ }),

/***/ 803:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyVideoDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_file_transfer__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ts_md5__ = __webpack_require__(804);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ts_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ts_md5__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var MyVideoDirective = /** @class */ (function () {
    function MyVideoDirective(el, file, platform, transfer) {
        this.el = el;
        this.file = file;
        this.platform = platform;
        this.transfer = transfer;
        this.video = this.el.nativeElement;
    }
    MyVideoDirective.prototype.ngOnInit = function () { };
    MyVideoDirective.prototype.ngOnChanges = function (changes) {
        for (var change in changes) {
            if (change == 'source' && this.platform.is('android')) {
                this.downloadVideo(changes[change].currentValue);
            }
            else if (change == 'source' && !this.platform.is('android')) {
                this.video.src = changes[change].currentValue;
            }
        }
    };
    MyVideoDirective.prototype.downloadVideo = function (url) {
        var _this = this;
        this.platform.ready().then(function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            var fileTransfer;
            return __generator(this, function (_a) {
                fileTransfer = this.transfer.create();
                fileTransfer.download(url, this.file.cacheDirectory + '/' + __WEBPACK_IMPORTED_MODULE_4_ts_md5__["Md5"].hashStr(url))
                    .then(function (d) {
                    var fileEntry = d;
                    _this.video.src = fileEntry.toInternalURL();
                });
                return [2 /*return*/];
            });
        }); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('source'),
        __metadata("design:type", String)
    ], MyVideoDirective.prototype, "source", void 0);
    MyVideoDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[my-video]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* Platform */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_file_transfer__["a" /* FileTransfer */]])
    ], MyVideoDirective);
    return MyVideoDirective;
}());

//# sourceMappingURL=my-video.js.map

/***/ }),

/***/ 827:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_screen_orientation__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_push_service_push_service__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ngx_restangular__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, auth, utils, init, screen, storage, translate, alertCtrl, ionicApp, push, events, restangular, modalCtrl) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth = auth;
        this.utils = utils;
        this.init = init;
        this.screen = screen;
        this.storage = storage;
        this.translate = translate;
        this.alertCtrl = alertCtrl;
        this.ionicApp = ionicApp;
        this.push = push;
        this.events = events;
        this.restangular = restangular;
        this.modalCtrl = modalCtrl;
        this.user = { avatar: '', name: '', points: 0 };
        this.initializeApp();
        this.init.points$.subscribe(function (value) { return _this.user.points = value; });
        this.init.avatar$.subscribe(function (value) { return _this.user.avatar = value; });
        this.init.name$.subscribe(function (value) { return _this.user.name = value; });
        this.utils.language$.subscribe(function (value) { return _this.language = value; });
        translate.setDefaultLang('en');
        events.subscribe('language:hebrew', function () { console.log('hebrew'); _this.initialSetup(); });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // close modal on back button
            _this.platform.registerBackButtonAction(function () {
                var activePortal = _this.ionicApp._loadingPortal.getActive() ||
                    _this.ionicApp._modalPortal.getActive() ||
                    _this.ionicApp._toastPortal.getActive() ||
                    _this.ionicApp._overlayPortal.getActive();
                if (activePortal) {
                    activePortal.dismiss();
                }
                else {
                    if (_this.nav.length() == 1) {
                        var alert_1 = _this.alertCtrl.create({
                            title: _this.translate.instant("POPUPS.EXIT"),
                            message: _this.translate.instant("POPUPS.WANTEXIT"),
                            buttons: [
                                {
                                    text: "OK", handler: function () {
                                        _this.platform.exitApp();
                                    }
                                },
                                { text: "Cancel", role: 'cancel' }
                            ]
                        });
                        alert_1.present();
                    }
                    else
                        _this.nav.pop();
                }
            });
            // design settings
            _this.statusBar.overlaysWebView(false);
            _this.statusBar.styleDefault();
            // orientation settings
            if (_this.platform.is('cordova')) {
                _this.screen.lock('portrait');
            }
            // launching
            _this.auth.initialize().then(function () { return __awaiter(_this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!(this.auth.getToken() !== '')) return [3 /*break*/, 2];
                            return [4 /*yield*/, this.initialSetup()];
                        case 1:
                            _a.sent();
                            this.init.onInitApp().then(function () {
                                setTimeout(function () { return _this.splashScreen.hide(); }, 100);
                                _this.push.init();
                                _this.rootPage = 'HomePage';
                            });
                            return [3 /*break*/, 3];
                        case 2:
                            // if it's the first launch
                            this.storage.get('entered').then(function (data) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!(data === null)) return [3 /*break*/, 1];
                                            setTimeout(function () { return _this.splashScreen.hide(); }, 100);
                                            this.rootPage = 'EnterPage';
                                            this.initialSetup();
                                            this.push.init();
                                            return [3 /*break*/, 3];
                                        case 1: 
                                        // if he is unauthorized
                                        return [4 /*yield*/, this.initialSetup()];
                                        case 2:
                                            // if he is unauthorized
                                            _a.sent();
                                            setTimeout(function () { return _this.splashScreen.hide(); }, 100);
                                            this.push.init();
                                            this.rootPage = 'LoginPage';
                                            _a.label = 3;
                                        case 3: return [2 /*return*/];
                                    }
                                });
                            }); });
                            _a.label = 3;
                        case 3: return [2 /*return*/];
                    }
                });
            }); });
        });
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.push(page.component);
    };
    MyApp.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.push.logout();
                        return [4 /*yield*/, this.auth.logout()];
                    case 1:
                        _a.sent();
                        this.nav.setRoot('LoginPage');
                        return [2 /*return*/];
                }
            });
        });
    };
    MyApp.prototype.selectLanguage = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setTitle(this.translate.instant("POPUPS.SELECTLANGUAGE"));
        alert.addInput({ type: 'radio', label: 'English', value: 'en', checked: this.language === 'en' });
        alert.addInput({ type: 'radio', label: 'עברית', value: 'he', checked: this.language === 'he' });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                _this.utils.setLanguage(data).then(function () {
                    _this.initialSetup();
                    _this.init.onInitApp().then(function () { _this.setLanguageOnServer(data); });
                });
            }
        });
        alert.present();
    };
    MyApp.prototype.setLanguageOnServer = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.restangular.one('users', this.auth.id).customGET('locale', { language: data }).toPromise()];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MyApp.prototype.initialSetup = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.translate.get('MENU').subscribe(function (res) {
                _this.titles = res;
                _this.pages = [
                    { title: _this.titles['HOMEPAGE'], component: 'HomePage', icon: 'home' },
                    { title: _this.titles['COMPANYREGISTER'], component: 'CompanyRegisterPage', icon: 'create' },
                    { title: _this.titles['PROFILE'], component: 'ProfilePage', icon: 'person' },
                    { title: _this.titles['SOCIALNETWORK'], component: 'UsersPage', icon: 'contacts' },
                    { title: _this.titles['FEED'], component: 'FeedPage', icon: 'logo-youtube' },
                    { title: _this.titles['SHOP'], component: 'CouponsPage', icon: 'cart' },
                    { title: _this.titles['MAP'], component: 'MapPage', icon: 'map' },
                    { title: _this.titles['UPLOAD'], component: 'UploadPage', icon: 'cloud-upload' },
                    { title: _this.titles['FAQ'], component: 'FaqPage', icon: 'help-circle' },
                    { title: _this.titles['CONTACT'], component: 'ContactPage', icon: 'mail' },
                    { title: _this.titles['NOTIFICATIONS'], component: 'NotificationsPage', icon: 'chatbubbles' },
                ];
                resolve();
            });
        });
    };
    MyApp.prototype.watchVideo = function (entity) {
        var modal = this.modalCtrl.create('FacebookPage', { entity: entity });
        modal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"G:\github\vt-client-2\src\app\app.html"*/'<ion-menu [content]="content" side="right">\n\n    <ion-header>\n\n        <ion-toolbar>\n\n            <ion-row align-items-center>\n\n                <ion-col col-3 text-center>\n\n                    <div *ngIf="user.avatar" class="side-avatar" background-image [source]="user.avatar"\n\n                         height="50"></div>\n\n                    <div *ngIf="!user.avatar" class="side-avatar" background-image\n\n                         [source]="\'assets/images/avatar.jpg\'" height="50"></div>\n\n                </ion-col>\n\n                <ion-col col-9 ion-text color="primary">\n\n                    <div class="title-font">{{user.name}}</div>\n\n                    <div><span class="title-font">{{user.points}}</span> {{"ALL.POINTS" | translate}}</div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-toolbar>\n\n    </ion-header>\n\n\n\n    <ion-content>\n\n        <ion-list>\n\n            <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n                <ion-icon name="{{p.icon}}"></ion-icon> &nbsp; {{p.title}}\n\n            </button>\n\n            <button menuClose ion-item (click)="selectLanguage()">\n\n                <ion-icon name="flag"></ion-icon> &nbsp; <span>Change language</span>\n\n            </button>\n\n            <button menuClose ion-item (click)="watchVideo(\'explanation\')">\n\n                <ion-icon name="book"></ion-icon> &nbsp; <span>{{"MENU.GUIDE" | translate}}</span>\n\n            </button>\n\n            <button menuClose ion-item (click)="watchVideo(\'banner\')">\n\n                <ion-icon name="cash"></ion-icon> &nbsp; <span>{{"MENU.MONEY" | translate}}</span>\n\n            </button>\n\n            <button menuClose ion-item (click)="watchVideo(\'explanation_video\')">\n\n                <ion-icon name="camera"></ion-icon> &nbsp; <span>{{"MENU.VIDEOGUIDE" | translate}}</span>\n\n            </button>\n\n            <button menuClose ion-item (click)="logout()">\n\n                <ion-icon name="exit"></ion-icon> &nbsp; <span>{{"MENU.LOGOUT" | translate}}</span>\n\n            </button>\n\n        </ion-list>\n\n    </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"G:\github\vt-client-2\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicApp */],
            __WEBPACK_IMPORTED_MODULE_10__providers_push_service_push_service__["a" /* PushService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_11_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__(477);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_utils__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthServiceProvider = /** @class */ (function () {
    function AuthServiceProvider(storage, utils) {
        this.storage = storage;
        this.utils = utils;
        this._token = new __WEBPACK_IMPORTED_MODULE_2_rxjs__["BehaviorSubject"]('');
        // token as observable
        this.token$ = this._token.asObservable();
    }
    // Reads token from storage (should return promise)
    AuthServiceProvider.prototype.initialize = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                _this.storage.get('id').then(function (val) {
                    console.log('initialize()', val, token);
                    _this.id = val || null;
                    _this._token.next(token || '');
                    _this.token = token || '';
                    resolve();
                });
            });
        });
    };
    // Getter
    AuthServiceProvider.prototype.getToken = function () {
        return this.token;
    };
    // Setter
    AuthServiceProvider.prototype.setToken = function (token) {
        this.token = token;
        this.storage.set('token', this.token);
        this._token.next(token);
    };
    AuthServiceProvider.prototype.logout = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.storage.clear().then(function () {
                _this.storage.set('entered', 1).then(function () {
                    _this.utils.setLanguage('en');
                    _this.id = 0;
                    _this.token = '';
                    _this._token.next('');
                    resolve();
                });
            });
        });
    };
    AuthServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__utils_utils__["a" /* UtilsProvider */]])
    ], AuthServiceProvider);
    return AuthServiceProvider;
}());

//# sourceMappingURL=auth-service.js.map

/***/ })

},[442]);
//# sourceMappingURL=main.js.map