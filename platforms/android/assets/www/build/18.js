webpackJsonp([18],{

/***/ 839:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProfilePageModule", function() { return EditProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__edit_profile__ = __webpack_require__(865);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var EditProfilePageModule = /** @class */ (function () {
    function EditProfilePageModule() {
    }
    EditProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__edit_profile__["a" /* EditProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__edit_profile__["a" /* EditProfilePage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], EditProfilePageModule);
    return EditProfilePageModule;
}());

//# sourceMappingURL=edit-profile.module.js.map

/***/ }),

/***/ 865:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(navCtrl, navParams, restangular, storage, utils, alertCtrl, zone, transfer, translate, auth, init) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restangular = restangular;
        this.storage = storage;
        this.utils = utils;
        this.alertCtrl = alertCtrl;
        this.zone = zone;
        this.transfer = transfer;
        this.translate = translate;
        this.auth = auth;
        this.init = init;
        this.user = { avatar: '', videos: [], coupons: [], country: '' };
        this.passwords = { old_password: '', new_password: '' };
        this.localProgress = false;
        this.process = this.utils.uploadProcess;
        this.load = this.restangular.one('users', this.auth.id).get();
        this.countries = [];
        this.utils.progress$.subscribe(function (val) {
            _this.zone.run(function () { _this.progress = val; });
        });
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    EditProfilePage.prototype.update = function () {
        var _this = this;
        console.log(this.user);
        if (this.user.name === '' || this.user.phone === '' || this.user.country === '') {
            var alert_1 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK'] });
            alert_1.present();
            return;
        }
        this.user.patch().subscribe(function (data) {
            _this.user = data;
            console.log(data);
            _this.setInit(data);
            var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.CHANGED"), buttons: ['OK'] });
            alert.present();
        });
    };
    EditProfilePage.prototype.updatePassword = function () {
        var _this = this;
        if (!this.utils.isAnyFieldEmpty(this.passwords)) {
            this.restangular.one('users', this.user.id).all('password').post(this.passwords).subscribe(function (data) {
                _this.passwords = { old_password: '', new_password: '' };
                var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.CHANGED"), buttons: ['OK'] });
                alert.present();
            });
        }
    };
    EditProfilePage.prototype.deleteAvatar = function () {
        var _this = this;
        for (var _i = 0, _a = this.user.medias; _i < _a.length; _i++) {
            var file = _a[_i];
            if (file.pivot.tag === 'avatar') {
                this.restangular.one('files', file.id).remove().subscribe(function (data) {
                    console.log(data);
                    var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.DELETED"), buttons: ['OK'] });
                    alert.present();
                    _this.restangular.one('users', _this.user.id).get().subscribe(function (data) {
                        _this.user = data;
                        _this.setInit(data);
                    });
                });
            }
        }
    };
    EditProfilePage.prototype.setAvatar = function (avatar) {
        var _this = this;
        console.log('Avatar received', avatar);
        this.newAvatar = avatar.url;
        this.localProgress = true;
        this.utils.setUploadProcess(true);
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'avatar',
            mimeType: avatar.type,
            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
            headers: { Authorization: "Bearer " + this.auth.getToken() },
            params: { media_key: 'avatar', entity_type: 'avatar', 'entity_id': this.user.id }
        };
        fileTransfer.onProgress(function (event) {
            if (event.lengthComputable) {
                var progress = Math.round((event.loaded / event.total) * 100);
                console.log('Progress', progress);
                _this.utils.setProgress(progress);
            }
        });
        console.log(options);
        fileTransfer.upload(this.newAvatar, this.utils.baseUrl + 'api/v1/files', options)
            .then(function (data) {
            console.log(data);
            var newData = JSON.parse(data.response);
            _this.user = _this.restangular.restangularizeElement('', newData.data, 'users');
            _this.setInit(_this.user);
            var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.UPLOADED"), buttons: ['OK'] });
            alert.present();
            _this.localProgress = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        }, function (err) {
            console.log(err);
            var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.ERROR"), buttons: ['OK'] });
            alert.present();
            _this.localProgress = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        });
    };
    EditProfilePage.prototype.updateAvatar = function (avatar) {
        var _this = this;
        console.log('Avatar received', avatar);
        this.newAvatar = avatar.url;
        this.localProgress = true;
        this.utils.setUploadProcess(true);
        var fileTransfer = this.transfer.create();
        var avatar_id = 0;
        for (var _i = 0, _a = this.user.medias; _i < _a.length; _i++) {
            var file = _a[_i];
            if (file.pivot.tag === 'avatar')
                avatar_id = file.id;
        }
        var options = {
            fileKey: 'avatar',
            mimeType: avatar.type,
            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
            headers: { Authorization: "Bearer " + this.auth.getToken() },
            params: { media_key: 'avatar', entity_type: 'avatar', entity_id: this.user.id }
        };
        fileTransfer.onProgress(function (event) {
            if (event.lengthComputable) {
                var progress = Math.round((event.loaded / event.total) * 100);
                console.log('Progress', progress);
                _this.utils.setProgress(progress);
            }
        });
        console.log(options);
        fileTransfer.upload(this.newAvatar, this.utils.baseUrl + 'api/v1/files/' + avatar_id + '?_method=PATCH', options)
            .then(function (data) {
            console.log(data);
            var newData = JSON.parse(data.response);
            _this.user = _this.restangular.restangularizeElement('', newData.data, 'users');
            _this.setInit(_this.user);
            var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.CHANGED"), buttons: ['OK'] });
            alert.present();
            _this.localProgress = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        }, function (err) {
            console.log(err);
            var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.ERROR"), buttons: ['OK'] });
            alert.present();
            _this.localProgress = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        });
    };
    EditProfilePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.load.subscribe(function (data) {
            _this.restangular.all('init/countries').getList().subscribe(function (data) { return _this.countries = data; });
            _this.user = data;
            _this.init.user = data;
            _this.setInit(data);
        });
    };
    EditProfilePage.prototype.setInit = function (data) {
        this.init.setPoints(data.points);
        this.init.setName(data.name);
        this.init.setAvatar(data.avatar);
    };
    EditProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-edit-profile',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\edit-profile\edit-profile.html"*/'<ion-header>\n\n\n\n    <common-header title=\'{{"ALL.EDIT" | translate}}\'></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center padding-top>\n\n\n\n        <ion-row *ngIf="user.avatar !== \'\'" align-items-center>\n\n\n\n            <ion-col col-4>\n\n\n\n                <img [src]="user.avatar" width="80" *ngIf="localProgress === false">\n\n\n\n                <div *ngIf="localProgress === true">\n\n                    <ion-spinner name="ios"></ion-spinner>\n\n                    <div>{{"ALL.UPLOADINGINPROGRESS" | translate}}: {{progress}}%</div>\n\n                </div>\n\n\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button ion-button small [disabled]="localProgress" color="primary" (click)="deleteAvatar()">{{"ALL.DELETE" | translate}}</button>\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button [disabled]="localProgress" ion-button small color="primary" add-picture (targetLogo)="updateAvatar($event)">{{"ALL.UPDATE" | translate}}</button>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="user.avatar === \'\'">\n\n\n\n            <button [disabled]="localProgress" ion-button block color="primary" add-picture (targetLogo)="setAvatar($event)">{{"EDIT.ADDAVATAR" | translate}}</button>\n\n\n\n        </ion-row>\n\n\n\n        <ion-list no-lines>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input placeholder=\'{{"ALL.NAME" | translate}}\' text-center [(ngModel)]="user.name"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item class="deleteLeftPadding" margin-top>\n\n                <ion-input type="tel" dir="ltr" text-center placeholder=\'{{"ALL.PHONE" | translate}}\' [(ngModel)]="user.phone"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding" text-center>\n\n                <ion-select [(ngModel)]="user.country">\n\n                    <ion-option *ngFor="let country of countries" [value]="country.code">{{country.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top text-center>\n\n                <button class="button-no-border" *ngIf="language === \'he\'" ion-button outline color="primary" (click)="update()">\n\n                    {{"ALL.UPDATE" | translate}}&nbsp; <img src="assets/images/arrows_left.png" height="25">\n\n                </button>\n\n\n\n                <button class="button-no-border" *ngIf="language === \'en\'" ion-button outline color="primary" (click)="update()">\n\n                    {{"ALL.UPDATE" | translate}}&nbsp; <img src="assets/images/arrows_right.png" height="25">\n\n                </button>\n\n            </ion-item>\n\n\n\n            <ion-item class="deleteLeftPadding">\n\n                <ion-input text-center placeholder=\'{{"EDIT.OLDPASSWORD" | translate}}\' type="password" [(ngModel)]="passwords.old_password"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder=\'{{"EDIT.NEWPASSWORD" | translate}}\' type="password" [(ngModel)]="passwords.new_password"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top text-center>\n\n                <button class="button-no-border" *ngIf="language === \'he\'" ion-button outline color="primary" (click)="updatePassword()">\n\n                    {{"EDIT.UPDATEPASSWORD" | translate}}&nbsp;<img src="assets/images/arrows_left.png" height="25">\n\n                </button>\n\n\n\n                <button class="button-no-border" *ngIf="language === \'en\'" ion-button outline color="primary" (click)="updatePassword()">\n\n                    {{"EDIT.UPDATEPASSWORD" | translate}}&nbsp;<img src="assets/images/arrows_right.png" height="25">\n\n                </button>\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <common-footer></common-footer>\n\n</ion-footer>'/*ion-inline-end:"G:\github\vt-client-2\src\pages\edit-profile\edit-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_init_service_init_service__["a" /* InitServiceProvider */]])
    ], EditProfilePage);
    return EditProfilePage;
}());

//# sourceMappingURL=edit-profile.js.map

/***/ })

});
//# sourceMappingURL=18.js.map