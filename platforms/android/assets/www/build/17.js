webpackJsonp([17],{

/***/ 840:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnterPageModule", function() { return EnterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__enter__ = __webpack_require__(866);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var EnterPageModule = /** @class */ (function () {
    function EnterPageModule() {
    }
    EnterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__enter__["a" /* EnterPage */],],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__enter__["a" /* EnterPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */]
            ]
        })
    ], EnterPageModule);
    return EnterPageModule;
}());

//# sourceMappingURL=enter.module.js.map

/***/ }),

/***/ 866:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EnterPage = /** @class */ (function () {
    function EnterPage(navCtrl, storage, alertCtrl, translate, utils, events, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.utils = utils;
        this.events = events;
        this.platform = platform;
        this.utils.language$.subscribe(function (value) { return _this.language = value; });
    }
    EnterPage.prototype.selectLanguage = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setTitle(this.translate.instant("POPUPS.SELECTLANGUAGE"));
        alert.addInput({ type: 'radio', label: 'English', value: 'en', checked: this.language === 'en' });
        alert.addInput({ type: 'radio', label: 'עברית', value: 'he', checked: this.language === 'he' });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                _this.utils.setLanguage(data).then(function () {
                    _this.events.publish('language:hebrew');
                });
            }
        });
        alert.present();
    };
    EnterPage.prototype.goToRegister = function () {
        var _this = this;
        this.storage.set('entered', 1).then(function () {
            _this.navCtrl.setRoot('RegisterPage');
        });
    };
    EnterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-enter',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\enter\enter.html"*/'<ion-content text-center>\n\n\n\n    <div class="welcome">\n\n        <h1>{{"ENTER.WELCOMETO" | translate}}</h1>\n\n        <img src="assets/images/header_logo.png">\n\n    </div>\n\n    <div class="langButton">\n\n        <button ion-button color="secondary" outline (click)="selectLanguage()">{{"ENTER.SELECTLANGUAGE" | translate}}</button>\n\n    </div>\n\n\n\n    <div class="langButton">\n\n        <button ion-button color="secondary" outline (click)="goToRegister()">{{"ENTER.STARTNOW" | translate}}</button>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\enter\enter.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */]])
    ], EnterPage);
    return EnterPage;
}());

//# sourceMappingURL=enter.js.map

/***/ })

});
//# sourceMappingURL=17.js.map