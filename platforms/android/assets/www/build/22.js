webpackJsonp([22],{

/***/ 832:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyRegisterAdditionalPageModule", function() { return CompanyRegisterAdditionalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__company_register_additional__ = __webpack_require__(858);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CompanyRegisterAdditionalPageModule = /** @class */ (function () {
    function CompanyRegisterAdditionalPageModule() {
    }
    CompanyRegisterAdditionalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__company_register_additional__["a" /* CompanyRegisterAdditionalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__company_register_additional__["a" /* CompanyRegisterAdditionalPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], CompanyRegisterAdditionalPageModule);
    return CompanyRegisterAdditionalPageModule;
}());

//# sourceMappingURL=company-register-additional.module.js.map

/***/ }),

/***/ 858:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyRegisterAdditionalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_file_transfer__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var CompanyRegisterAdditionalPage = /** @class */ (function () {
    function CompanyRegisterAdditionalPage(navCtrl, navParams, transfer, auth, alertCtrl, translate, loadingCtrl, init, restangular, storage, utils) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.transfer = transfer;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.loadingCtrl = loadingCtrl;
        this.init = init;
        this.restangular = restangular;
        this.storage = storage;
        this.utils = utils;
        this.company_id = this.navParams.get('company_id') || 1;
    }
    CompanyRegisterAdditionalPage.prototype.setLogo = function (logo) {
        console.log('Logo received', logo);
        this.logo = logo;
    };
    CompanyRegisterAdditionalPage.prototype.setVideo = function (video) {
        console.log('Video received', video);
        this.video = video;
        this.src = video.fullPath;
    };
    CompanyRegisterAdditionalPage.prototype.send = function () {
        var _this = this;
        console.log(this.video, this.logo, this.company_id);
        var promises = [];
        if (typeof this.video !== 'undefined') {
            promises.push(this.sendVideo());
        }
        if (typeof this.logo !== 'undefined') {
            promises.push(this.sendLogo());
        }
        Promise.all(promises).then(function () { return _this.alertUser(); });
    };
    CompanyRegisterAdditionalPage.prototype.sendLogo = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var progress = 0;
            var loading = _this.loadingCtrl.create({
                content: "Please wait...",
                spinner: 'ios',
            });
            loading.present();
            var fileTransfer = _this.transfer.create();
            var options = {
                fileKey: 'logo',
                mimeType: _this.logo.type,
                fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
                headers: { Authorization: "Bearer " + _this.auth.getToken() },
                params: { media_key: 'logo', entity_type: 'company', 'entity_id': _this.company_id }
            };
            console.log(options);
            fileTransfer.onProgress(function (event) {
                if (event.lengthComputable) {
                    progress = Math.round((event.loaded / event.total) * 100);
                }
            });
            fileTransfer.upload(_this.logo.url, _this.utils.baseUrl + 'api/v1/files', options)
                .then(function (data) {
                console.log(data);
                loading.dismiss();
                var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.UPLOADED"), buttons: ['OK'] });
                alert.present();
                resolve();
            }, function (err) {
                console.log(err);
                loading.dismiss();
                var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.ERROR"), buttons: ['OK'] });
                alert.present();
                reject();
            });
        });
    };
    CompanyRegisterAdditionalPage.prototype.sendVideo = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var progress = 0;
            var loading = _this.loadingCtrl.create({
                content: _this.translate.instant("POPUPS.PLEASEWAIT"),
                spinner: 'ios',
            });
            loading.present();
            var fileTransfer = _this.transfer.create();
            var options = {
                fileKey: 'main_video',
                mimeType: _this.video.type,
                fileName: _this.video.name,
                headers: { Authorization: "Bearer " + _this.auth.getToken() },
                params: { media_key: 'main_video', entity_type: 'company', 'entity_id': _this.company_id }
            };
            fileTransfer.onProgress(function (event) {
                if (event.lengthComputable) {
                    progress = Math.round((event.loaded / event.total) * 100);
                }
            });
            fileTransfer.upload(_this.video.fullPath, _this.utils.baseUrl + 'api/v1/files', options)
                .then(function (data) {
                console.log(data);
                loading.dismiss();
                var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.UPLOADED"), buttons: ['OK'] });
                alert.present();
                resolve();
            }, function (err) {
                console.log(err);
                loading.dismiss();
                var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.ERROR"), buttons: ['OK'] });
                alert.present();
                reject();
            });
        });
    };
    CompanyRegisterAdditionalPage.prototype.alertUser = function () {
        var _this = this;
        var alert = this.alertCtrl.create({ title: this.translate.instant("POPUPS.APPROVAL"), buttons: ['OK'] });
        alert.present().then(function () {
            _this.storage.set('approved', 0).then(function () { return _this.navCtrl.setRoot('HomePage'); });
        });
    };
    CompanyRegisterAdditionalPage.prototype.ngOnInit = function () {
        var _this = this;
        this.restangular.one('companies', this.company_id).get().subscribe(function (data) { return _this.company = data; });
    };
    CompanyRegisterAdditionalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-company-register-additional',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\company-register-additional\company-register-additional.html"*/'<ion-header>\n\n\n\n    <common-header title=\'{{"ADDITIONAL.STEP2" | translate}}\'></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90">\n\n\n\n\n\n        <!--<div class="fakeInput" (click)="alertVideo()"> העלה סרטון +</div>-->\n\n\n\n        <div>\n\n            <video *ngIf="src" [src]="src" controls controlsList="nodownload"></video>\n\n        </div>\n\n\n\n        <div  margin-top class="fakeInput" add-video (targetVideo)="setVideo($event)"> {{"ADDITIONAL.UPLOADVIDEO" | translate}} +</div>\n\n\n\n        <!--<div class="fakeInput" (click)="alertLogo()"> העלה לוגו +</div>-->\n\n\n\n        <div margin-top>\n\n            <img *ngIf="logo" [src]="logo.url">\n\n        </div>\n\n\n\n        <div margin-top class="fakeInput" add-picture (targetLogo)="setLogo($event)"> {{"ADDITIONAL.UPLOADLOGO" | translate}} +</div>\n\n\n\n        <div margin-top>\n\n            <button ion-button block color="primary" (click)="send()">{{"ALL.SEND" | translate}}</button>\n\n        </div>\n\n\n\n        <div>\n\n            <button ion-button margin-top block color="light" (click)="alertUser()">{{"ADDITIONAL.SKIP" | translate}}</button>\n\n        </div>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\company-register-additional\company-register-additional.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_8__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__["a" /* UtilsProvider */]])
    ], CompanyRegisterAdditionalPage);
    return CompanyRegisterAdditionalPage;
}());

//# sourceMappingURL=company-register-additional.js.map

/***/ })

});
//# sourceMappingURL=22.js.map