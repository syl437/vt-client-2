webpackJsonp([7],{

/***/ 851:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatePageModule", function() { return RatePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__rate__ = __webpack_require__(877);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var RatePageModule = /** @class */ (function () {
    function RatePageModule() {
    }
    RatePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__rate__["a" /* RatePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__rate__["a" /* RatePage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
            ],
        })
    ], RatePageModule);
    return RatePageModule;
}());

//# sourceMappingURL=rate.module.js.map

/***/ }),

/***/ 877:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_restangular__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RatePage = /** @class */ (function () {
    function RatePage(navCtrl, navParams, viewCtrl, init, restangular) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.init = init;
        this.restangular = restangular;
        this.company = this.navParams.get('company');
        this.stars = [false, false, false, false, false];
        this.user = this.init.user;
    }
    RatePage.prototype.closeModal = function () { this.viewCtrl.dismiss(); };
    RatePage.prototype.rate = function (index) {
        for (var i = 0; i < this.stars.length; i++) {
            if (i <= index) {
                this.stars[i] = true;
            }
            else if (i > index && i < this.stars.length) {
                this.stars[i] = false;
            }
        }
    };
    RatePage.prototype.sendRate = function () {
        var _this = this;
        var rate = { user_id: this.user.id, company_id: this.company.id, mark: 0 };
        for (var _i = 0, _a = this.stars; _i < _a.length; _i++) {
            var star = _a[_i];
            if (star === true)
                rate.mark += 1;
        }
        if (rate.mark > 0) {
            this.restangular.all('rates').post(rate).subscribe(function (data) {
                _this.viewCtrl.dismiss({ company: data });
            });
        }
    };
    RatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-rate',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\rate\rate.html"*/'<ion-content>\n\n\n\n    <div class="popup-block" text-center>\n\n\n\n        <ion-row padding class="popup-header">\n\n            <ion-col>{{company.title}}</ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row padding>\n\n            <ion-col>\n\n                <ion-icon class="big-star" *ngFor="let star of stars, let i = index" (click)="rate(i)" name="ios-star"\n\n                          [ngClass]="{\'yellow-star\': star === true, \'grey-star\': star === false}"></ion-icon>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row text-center>\n\n            <ion-col col-6 no-padding>\n\n                <div padding class="cancel-button" (click)="closeModal()">{{"ALL.CANCEL" | translate}}</div>\n\n            </ion-col>\n\n            <ion-col col-6 no-padding>\n\n                <div padding class="send-button" (click)="sendRate()">{{"ALL.SUBMIT" | translate}}</div>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\rate\rate.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__["Restangular"]])
    ], RatePage);
    return RatePage;
}());

//# sourceMappingURL=rate.js.map

/***/ })

});
//# sourceMappingURL=7.js.map