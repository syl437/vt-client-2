webpackJsonp([20],{

/***/ 836:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CouponPageModule", function() { return CouponPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__coupon__ = __webpack_require__(862);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_qrcode__ = __webpack_require__(438);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CouponPageModule = /** @class */ (function () {
    function CouponPageModule() {
    }
    CouponPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__coupon__["a" /* CouponPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__coupon__["a" /* CouponPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5_angular2_qrcode__["a" /* QRCodeModule */]
            ],
        })
    ], CouponPageModule);
    return CouponPageModule;
}());

//# sourceMappingURL=coupon.module.js.map

/***/ }),

/***/ 862:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CouponPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CouponPage = /** @class */ (function () {
    function CouponPage(navCtrl, navParams, init, viewCtrl, platform, utils) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.init = init;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.utils = utils;
        this.state = this.navParams.get('state');
        this.coupon = { image: '', title: '', price: 0, qr: '', description: '', company: { logo: '', title: '' } };
        this.coupon = this.navParams.get('coupon');
        console.log(this.coupon);
    }
    CouponPage.prototype.closeModal = function () { this.viewCtrl.dismiss(); };
    CouponPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-coupon',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\coupon\coupon.html"*/'<ion-content (click)="closeModal()">\n\n\n\n    <div class="custom-content">\n\n\n\n        <div *ngIf="state === \'qr\'" text-center>\n\n            <qr-code [value]="coupon.qr" [size]="250"></qr-code>\n\n        </div>\n\n\n\n        <div class="coupon-image" *ngIf="state === \'content\'">\n\n            <img [src]="coupon.image" *ngIf="coupon.image !== \'\'">\n\n            <img [src]="coupon.company.logo" *ngIf="coupon.image === \'\'">\n\n            <img src="assets/images/logo.png" *ngIf="coupon.company.logo === \'\' && coupon.image === \'\'">\n\n        </div>\n\n\n\n        <div *ngIf="state === \'content\'">\n\n\n\n            <div class="coupon-title"><b>{{coupon.title}}</b></div>\n\n            <div class="coupon-subtitle"><b>{{coupon.company.title}}</b></div>\n\n\n\n            <div><b>{{"COMPANYREGISTER.ADDRESS" | translate}}</b>: {{coupon.company.address}}</div>\n\n            <div><b>{{"ALL.PHONE"  | translate}}</b>: <span dir="ltr">{{coupon.company.phone}}</span></div>\n\n\n\n            <div class="coupon-description"><b>{{"COUPONS.DESCRIPTION" | translate}}</b>: {{coupon.description}}</div>\n\n\n\n        </div>\n\n\n\n    </div>\n\n\n\n</ion-content>'/*ion-inline-end:"G:\github\vt-client-2\src\pages\coupon\coupon.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */]])
    ], CouponPage);
    return CouponPage;
}());

//# sourceMappingURL=coupon.js.map

/***/ })

});
//# sourceMappingURL=20.js.map