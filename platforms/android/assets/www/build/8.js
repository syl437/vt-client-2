webpackJsonp([8],{

/***/ 849:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile__ = __webpack_require__(875);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__profile__["a" /* ProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__profile__["a" /* ProfilePage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());

//# sourceMappingURL=profile.module.js.map

/***/ }),

/***/ 875:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, restangular, utils, translate, auth, init, modalCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.restangular = restangular;
        this.utils = utils;
        this.translate = translate;
        this.auth = auth;
        this.init = init;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.user = { avatar: '', videos: [], coupons: [] };
        this.load = this.restangular.one('users', this.auth.id).get();
    }
    ProfilePage.prototype.goToCoupon = function (coupon, used, state) {
        if (used === 0) {
            var modal = this.modalCtrl.create('CouponPage', { coupon: coupon, state: state });
            modal.present();
        }
    };
    ProfilePage.prototype.showDeletePopup = function (id) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.CONFIRMDELETE"),
            message: this.translate.instant("POPUPS.CONFIRMDELETE2"),
            buttons: [
                { text: 'Cancel', role: 'cancel', },
                {
                    text: 'OK',
                    handler: function () { _this.deleteVideo(id); }
                }
            ]
        });
        alert.present();
    };
    ProfilePage.prototype.deleteVideo = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.restangular.one('files', id).remove().toPromise()];
                    case 1:
                        _a.sent();
                        this.alertCtrl.create({ title: this.translate.instant("POPUPS.DELETED"), buttons: ['OK'] }).present();
                        this.restangular.one('users', this.auth.id).get().subscribe(function (data) {
                            _this.user = data;
                            _this.init.user = data;
                            _this.init.setPoints(data.points);
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.goToEdit = function () {
        this.navCtrl.push('EditProfilePage');
    };
    ProfilePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.load.subscribe(function (data) {
            _this.user = data;
            _this.init.user = data;
        });
    };
    ProfilePage.prototype.ionViewDidLeave = function () {
        this.load.unsubscribe();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\profile\profile.html"*/'<ion-header>\n\n\n\n    <ion-navbar color="primary" text-center>\n\n        <ion-buttons right>\n\n            <button ion-button clear (click)="goToEdit()">{{"ALL.EDIT" | translate}}</button>\n\n        </ion-buttons>\n\n        <ion-title>{{"MENU.PROFILE" | translate}}</ion-title>\n\n        <ion-buttons left>\n\n            <button ion-button menuToggle right>\n\n                <ion-icon name="menu"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-row margin-top align-items-center>\n\n        <ion-col col-6>\n\n            <ion-row>\n\n                <ion-col col-4>\n\n                    <div *ngIf="user.avatar !== \'\'" class="user-avatar" background-image [source]="user.avatar" height="50"></div>\n\n                    <div *ngIf="user.avatar === \'\'" class="user-avatar" background-image [source]="\'assets/images/avatar.jpg\'" height="50"></div>\n\n                </ion-col>\n\n                <ion-col col-8>\n\n                    <div>{{user.name}}</div>\n\n                    <div style="font-size: 12px;">{{user.phone}}</div>\n\n                    <ion-row no-padding>\n\n                        <ion-col col-4 text-center no-padding>\n\n                            <img style="padding-top: 3px;" src="assets/images/coin.png" height="26">\n\n                        </ion-col>\n\n                        <ion-col col-8 no-padding>\n\n                            <div class="points-text">{{"PROFILE.POINTS" | translate}}</div>\n\n                            <div>{{user.points}} <img src="assets/images/coin_icon.png" height="14" style="padding-top: 2px;"></div>\n\n                        </ion-col>\n\n                    </ion-row>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-col>\n\n        <ion-col col-6>\n\n            <ion-row text-center ion-text color="primary">\n\n                <ion-col col-4>\n\n                    <div>{{user.followers}}</div>\n\n                    <div class="small-font">{{"ALL.FOLLOWERS" | translate}}</div>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <div>{{user.videos.length}}</div>\n\n                    <div class="small-font">{{"ALL.VIDEOS" | translate}}</div>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <div>{{user.coupons.length}}</div>\n\n                    <div class="small-font">{{"ALL.PURCHASES" | translate}}</div>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-col>\n\n        <div class="vertical-line"></div>\n\n    </ion-row>\n\n\n\n    <div text-center *ngIf="user.videos && user.videos.length > 0">\n\n        <div ion-button small>{{"PROFILE.MYVIDEOS" | translate}}</div>\n\n    </div>\n\n\n\n    <div *ngIf="user.videos && user.videos.length > 0">\n\n\n\n        <ion-scroll scrollX="true">\n\n\n\n            <div *ngFor="let video of user.videos" class="video-block small-video" text-center dir="ltr" padding-top>\n\n\n\n                <div class="close-icon" (click)="showDeletePopup(video.id)">\n\n                    <ion-icon name="close-circle"></ion-icon>\n\n                </div>\n\n                <video my-video [source]="video.url" controls controlsList="nodownload" poster="{{video.thumbnail}}"></video>\n\n                <div padding-top>{{video.pivot.tag}}</div>\n\n\n\n            </div>\n\n\n\n        </ion-scroll>\n\n\n\n    </div>\n\n\n\n    <div text-center *ngIf="user.coupons && user.coupons.length > 0">\n\n        <div ion-button small>{{"PROFILE.MYCOUPONS" | translate}}</div>\n\n    </div>\n\n\n\n    <ion-list no-lines class="coupons" margin-top padding-horizontal>\n\n\n\n        <ion-row nowrap align-items-stretch *ngFor="let coupon of user.coupons" [ngClass]="{disabled: coupon.pivot.used === 1}">\n\n\n\n            <ion-col no-border no-padding no-margin class="company-logo" (click)="goToCoupon(coupon, coupon.pivot.used, \'content\')">\n\n                <div background-image [source]="coupon.image" *ngIf="coupon.image !== \'\'"></div>\n\n                <div background-image [source]="coupon.company.logo" *ngIf="coupon.image === \'\'"></div>\n\n                <div background-image [source]="\'assets/images/logo.png\'" *ngIf="coupon.company.logo === \'\' && coupon.image === \'\'"></div>\n\n            </ion-col>\n\n            <ion-col col-7 (click)="goToCoupon(coupon, coupon.pivot.used, \'content\')" style="position: relative;">\n\n                <div><b>{{coupon.title}}</b></div>\n\n                <div>{{coupon.company.title}}</div>\n\n                <div class="coupon-price"> {{coupon.price}} <img src="assets/images/coin_icon_white.png" class="coin-icon"></div>\n\n            </ion-col>\n\n            <ion-col col-2 text-center align-self-center (click)="goToCoupon(coupon, coupon.pivot.used, \'qr\')">\n\n                <img src="assets/images/qr.svg">\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n\n\n    </ion-list>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <common-footer></common-footer>\n\n</ion-footer>'/*ion-inline-end:"G:\github\vt-client-2\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ })

});
//# sourceMappingURL=8.js.map