webpackJsonp([13],{

/***/ 843:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home__ = __webpack_require__(869);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__home__["a" /* HomePage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 869:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, init, alertCtrl, modalCtrl, translate, utils, zone, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.init = init;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.translate = translate;
        this.utils = utils;
        this.zone = zone;
        this.storage = storage;
        this.region = 0;
        this.category = 0;
        this.banner = null;
        this.zone.run(function () {
            _this.utils.language$.subscribe(function (data) { return _this.language = data; });
        });
        this.init.company_categories$.subscribe(function (company_categories) { return _this.company_categories = company_categories; });
        this.init.regions$.subscribe(function (regions) { return _this.regions = regions; });
        this.init.banners$.subscribe(function (banners) {
            _this.banners = banners;
            _this.banner = _this.banners && _this.banners.length && _this.banners[Math.floor(Math.random() * _this.banners.length)];
        });
    }
    HomePage.prototype.goToCompanies = function (company_category_id, region_id) {
        console.log(company_category_id, region_id);
        if (company_category_id === 0) {
            var alert_1 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.PLEASECHOOSE"), buttons: ['OK'] });
            alert_1.present();
            return;
        }
        this.navCtrl.push('CompaniesPage', { company_category_id: company_category_id, region_id: region_id });
        this.region = 0;
        this.category = 0;
    };
    HomePage.prototype.goToCoupons = function () {
        this.navCtrl.push('CouponsPage', { company_id: 0 });
    };
    HomePage.prototype.goToMap = function () {
        var _this = this;
        if (this.init.coordinates.lat !== 0 && this.init.coordinates.lng !== 0) {
            this.navCtrl.push('MapPage');
        }
        else {
            this.init.getLocation().subscribe(function (data) { return _this.navCtrl.push('MapPage'); });
        }
    };
    HomePage.prototype.goToCode = function () {
        var modal = this.modalCtrl.create('FacebookPage', { entity: 'code' });
        modal.present();
        // modal.onDidDismiss(() => {});
    };
    HomePage.prototype.goToUpload = function () {
        this.navCtrl.push('UploadPage');
    };
    HomePage.prototype.goToNetwork = function () {
        this.navCtrl.push('UsersPage');
    };
    HomePage.prototype.goToUser = function (id) {
        this.navCtrl.push('UserPage', { user_id: id });
    };
    HomePage.prototype.goToBanner = function () {
        if (this.banner.type === 'explanation') {
            var modal = this.modalCtrl.create('FacebookPage', { entity: 'banner' });
            modal.present();
        }
        else {
            this.navCtrl.push('CompanyPage', { company_id: this.banner.company_id });
        }
    };
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get('explanation_displayed').then(function (data) {
            if (data == null) {
                var modal = _this.modalCtrl.create('FacebookPage', { entity: 'explanation' });
                modal.present();
                _this.storage.set('explanation_displayed', true);
            }
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\home\home.html"*/'<ion-header>\n\n\n\n    <picture-header></picture-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-scroll text-center scrollX="true">\n\n\n\n        <div class="friends">\n\n            <div class="friend" background-image [source]="\'assets/images/social_network.png\'" height="50" (click)="goToNetwork()"></div>\n\n        </div>\n\n        <div *ngFor="let user of init.following" class="friends">\n\n            <div (click)="goToUser(user.id)" class="friend" background-image [source]="user.avatar" *ngIf="user.avatar !== \'\'"  height="50"></div>\n\n            <div (click)="goToUser(user.id)" class="friend" background-image [source]="\'assets/images/avatar.jpg\'" *ngIf="user.avatar === \'\'" height="50"></div>\n\n        </div>\n\n\n\n    </ion-scroll>\n\n\n\n    <ion-grid no-padding>\n\n\n\n        <ion-row justify-content-center align-items-center class="search-row custom-border">\n\n\n\n            <ion-col col-4>\n\n                <ion-select interface="action-sheet" [(ngModel)]="region">\n\n                    <ion-option [value]="0">{{"ALL.REGION" | translate}}</ion-option>\n\n                    <ion-option *ngFor="let region of regions" [value]="region.id">{{region.title}}</ion-option>\n\n                </ion-select>\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <ion-select interface="action-sheet" [(ngModel)]="category">\n\n                    <ion-option [value]="0">{{"ALL.CATEGORY" | translate}}</ion-option>\n\n                    <ion-option *ngFor="let category of company_categories" [value]="category.id">{{category.title}}</ion-option>\n\n                </ion-select>\n\n            </ion-col>\n\n            <ion-col col-4 text-center>\n\n                <button class="search-button" ion-button clear color="primary" (click)="goToCompanies(category, region)">\n\n                    <ion-icon name="search"></ion-icon>&nbsp;<b>{{"ALL.SEARCH" | translate}}</b>\n\n                </button>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n\n\n        <ion-row class="imageRow">\n\n            <ion-col col-6 (click)="goToCompanies(1, 0)" *ngIf="language === \'he\'" background-image [source]="\'assets/images/main_pictures/hotels_he.jpg\'"></ion-col>\n\n            <ion-col col-6 (click)="goToCompanies(1, 0)" *ngIf="language === \'en\'" background-image [source]="\'assets/images/main_pictures/hotels_en.jpg\'"></ion-col>\n\n\n\n            <ion-col col-6 (click)="goToCompanies(2, 0)" *ngIf="language === \'he\'" background-image [source]="\'assets/images/main_pictures/restaurants_he.jpg\'"></ion-col>\n\n            <ion-col col-6 (click)="goToCompanies(2, 0)" *ngIf="language === \'en\'" background-image [source]="\'assets/images/main_pictures/restaurants_en.jpg\'"></ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row class="imageRow">\n\n            <ion-col col-6 (click)="goToCompanies(3, 0)" *ngIf="language === \'he\'" background-image [source]="\'assets/images/main_pictures/attractions_he.jpg\'"></ion-col>\n\n            <ion-col col-6 (click)="goToCompanies(3, 0)" *ngIf="language === \'en\'" background-image [source]="\'assets/images/main_pictures/attractions_en.jpg\'"></ion-col>\n\n            <ion-col col-6 (click)="goToCompanies(4, 0)" *ngIf="language === \'he\'" background-image [source]="\'assets/images/main_pictures/parties_he.jpg\'"></ion-col>\n\n            <ion-col col-6 (click)="goToCompanies(4, 0)" *ngIf="language === \'en\'" background-image [source]="\'assets/images/main_pictures/parties_en.jpg\'"></ion-col>\n\n        </ion-row>\n\n\n\n    </ion-grid>\n\n\n\n    <ion-toolbar class="home-footer" no-padding text-center>\n\n            <div class="home-footer-inner" (click)="goToCoupons()">\n\n                <div><img src="assets/images/gift.png"></div>\n\n                <div ion-text color="primary">{{"MENU.SHOP" | translate}}</div>\n\n            </div>\n\n            <div class="home-footer-inner" (click)="goToCode()">\n\n                <div><img src="assets/images/code.png"></div>\n\n                <div ion-text color="primary">{{"FACEBOOK.CODE" | translate}}</div>\n\n            </div>\n\n            <div class="home-footer-inner" (click)="goToMap()">\n\n                <div><img src="assets/images/map.png"></div>\n\n                <div ion-text color="primary">{{"MENU.MAP" | translate}}</div>\n\n            </div>\n\n            <div class="home-footer-inner" (click)="goToUpload()">\n\n                <div><img src="assets/images/v.png"></div>\n\n                <div ion-text color="primary">{{"MENU.UPLOAD" | translate}}</div>\n\n            </div>\n\n    </ion-toolbar>\n\n\n\n    <ion-toolbar no-padding class="home-footer-div" *ngIf="banner" (click)="goToBanner()">\n\n        <img [src]="banner.image" *ngIf="banner.image" style="width: 100%;">\n\n    </ion-toolbar>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=13.js.map