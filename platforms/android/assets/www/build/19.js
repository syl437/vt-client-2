webpackJsonp([19],{

/***/ 838:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CouponsPageModule", function() { return CouponsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__coupons__ = __webpack_require__(864);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CouponsPageModule = /** @class */ (function () {
    function CouponsPageModule() {
    }
    CouponsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__coupons__["a" /* CouponsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__coupons__["a" /* CouponsPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], CouponsPageModule);
    return CouponsPageModule;
}());

//# sourceMappingURL=coupons.module.js.map

/***/ }),

/***/ 864:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CouponsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CouponsPage = /** @class */ (function () {
    function CouponsPage(navCtrl, navParams, restangular, init, auth, modalCtrl, utils) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restangular = restangular;
        this.init = init;
        this.auth = auth;
        this.modalCtrl = modalCtrl;
        this.utils = utils;
        this.companies = [];
        this.company_id = this.navParams.get('company_id') || 0;
        this.region = 0;
        this.category = 0;
        this.filteredCompanies = [];
        this.init.company_categories$.subscribe(function (company_categories) { return _this.company_categories = company_categories; });
        this.init.regions$.subscribe(function (regions) { return _this.regions = regions; });
    }
    CouponsPage.prototype.filterCoupons = function () {
        var _this = this;
        if (this.category === 0 && this.region === 0) {
            return this.filteredCompanies = this.companies;
        }
        if (this.category === 0) {
            return this.filteredCompanies = this.companies.filter(function (company) { return company.region_id === _this.region; });
        }
        if (this.region === 0) {
            return this.filteredCompanies = this.companies.filter(function (company) { return company.company_category_id === _this.category; });
        }
        this.filteredCompanies = this.companies.filter(function (company) { return company.company_category_id === _this.category && company.region_id === _this.region; });
    };
    CouponsPage.prototype.useCoupon = function (company, coupon, price) {
        var _this = this;
        if (this.init.points < price) {
            var modal = this.modalCtrl.create('FacebookPage', { entity: 'points' });
            modal.present();
            return;
        }
        this.restangular.one('coupons', coupon.id).one('users', this.auth.id).customGET('bind')
            .subscribe(function (data) {
            console.log(data);
            _this.showPopup(company, coupon);
            _this.getCompanies();
            _this.init.user = data;
            _this.init.setPoints(data.points);
        });
    };
    CouponsPage.prototype.showPopup = function (company, coupon) {
        var modal = this.modalCtrl.create('FacebookPage', { company: company, entity: 'coupon' });
        modal.present();
        modal.onDidDismiss(function () {
            coupon.company = company;
            // let modal = this.modalCtrl.create('CouponPage', {coupon: coupon, from: 'profile'});
            // modal.present();
        });
    };
    CouponsPage.prototype.goToCoupon = function (coupon, used, company) {
        coupon.company = company;
        var modal = this.modalCtrl.create('CouponPage', { coupon: coupon, state: 'content' });
        modal.present();
    };
    CouponsPage.prototype.goToCompany = function (id) {
        console.log(5);
        this.navCtrl.push('CompanyPage', { company_id: id });
    };
    CouponsPage.prototype.getCompanies = function () {
        var _this = this;
        this.companies = [];
        this.filteredCompanies = [];
        this.restangular.all('coupons').getList({ company_id: this.company_id }).subscribe(function (data) {
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var item = data_1[_i];
                if ((item.coupons && item.coupons.length > 0) || item.coupon) {
                    _this.companies.push(item);
                    _this.filteredCompanies.push(item);
                }
            }
        });
    };
    CouponsPage.prototype.ionViewWillEnter = function () {
        this.getCompanies();
    };
    CouponsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-coupons',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\coupons\coupons.html"*/'<ion-header>\n\n\n\n    <common-header title=\'{{"MENU.SHOP" | translate}}\'></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-row align-items-center class="greyRow choice" *ngIf="company_id === 0">\n\n\n\n        <ion-col col-6 text-right>\n\n            <ion-select [(ngModel)]="region" (ionChange)="filterCoupons()">\n\n                <ion-option [value]="0">{{"ALL.CHOOSEREGION" | translate}}</ion-option>\n\n                <ion-option *ngFor="let region of regions" [value]="region.id">{{region.title}}</ion-option>\n\n            </ion-select>\n\n        </ion-col>\n\n        <ion-col col-6 text-right>\n\n            <ion-select [(ngModel)]="category" (ionChange)="filterCoupons()">\n\n                <ion-option [value]="0">{{"ALL.CHOOSECATEGORY" | translate}}</ion-option>\n\n                <ion-option *ngFor="let category of company_categories" [value]="category.id">{{category.title}}\n\n                </ion-option>\n\n            </ion-select>\n\n        </ion-col>\n\n\n\n    </ion-row>\n\n\n\n    <ion-grid *ngFor="let company of filteredCompanies" no-padding>\n\n\n\n        <ion-list *ngIf="company.coupons && company.coupons.length !== 0" no-margin>\n\n\n\n            <ion-row nowrap align-items-stretch *ngFor="let coupon of company.coupons">\n\n\n\n                <ion-col no-border no-padding no-margin class="company-logo" (click)="goToCoupon(coupon, 0, company)">\n\n                    <div background-image [source]="coupon.image" *ngIf="coupon.image !== \'\'"></div>\n\n                    <div background-image [source]="company.logo" *ngIf="coupon.image === \'\'"></div>\n\n                    <div background-image [source]="\'assets/images/logo.png\'" *ngIf="company.logo === \'\' && coupon.image === \'\'"></div>\n\n                </ion-col>\n\n                <ion-col col-5 (click)="goToCoupon(coupon, 0, company)">\n\n                    <div><b>{{coupon.title}}</b></div>\n\n                    <div>{{company.title}}</div>\n\n                    <div class="coupon-price"> {{coupon.price}} <img src="assets/images/coin_icon_white.png" class="coin-icon"></div>\n\n                </ion-col>\n\n                <ion-col col-2 align-self-start>\n\n                    <div class="company-homepage" (click)="goToCompany(company.id); $event.stopPropagation();">\n\n                        <img src="assets/images/company_icon_1.png">\n\n                        <div>{{"COUPONS.COMPANYPAGE" | translate}}</div>\n\n                    </div>\n\n                </ion-col>\n\n                <ion-col col-2  (click)="useCoupon(company, coupon, coupon.price); $event.stopPropagation();" text-center class="primaryColor" style="border-bottom: 2px solid white;">\n\n                    <div class="couponUse">\n\n                        <div>\n\n                            <div>+</div>\n\n                            <div>{{"COUPONS.USE" | translate}}</div>\n\n                        </div>\n\n                    </div>\n\n                </ion-col>\n\n\n\n            </ion-row>\n\n\n\n        </ion-list>\n\n\n\n        <ion-list *ngIf="company.coupon" no-margin>\n\n\n\n            <ion-row nowrap align-items-stretch class="couponDisabled">\n\n\n\n                <ion-col no-border no-padding no-margin class="company-logo" (click)="goToCoupon(company.coupon, 0, company)">\n\n                    <div background-image [source]="company.coupon.image" *ngIf="company.coupon.image !== \'\'"></div>\n\n                    <div background-image [source]="company.logo" *ngIf="company.coupon.image === \'\'"></div>\n\n                    <div background-image [source]="\'assets/images/logo.png\'" *ngIf="company.logo === \'\' && company.coupon.image === \'\'"></div>\n\n                </ion-col>\n\n                <ion-col col-5 (click)="goToCoupon(company.coupon, 0, company)">\n\n                    <div><b>{{company.coupon.title}}</b></div>\n\n                    <div>{{company.title}}</div>\n\n                    <div class="coupon-price"> {{company.coupon.price}} <img src="assets/images/coin_icon_white.png" class="coin-icon"></div>\n\n                </ion-col>\n\n                <ion-col col-2 align-self-start>\n\n                    <div class="company-homepage" (click)="goToCompany(company.id); $event.stopPropagation();">\n\n                        <img src="assets/images/company_icon_1.png">\n\n                        <div>{{"COUPONS.COMPANYPAGE" | translate}}</div>\n\n                    </div>\n\n                </ion-col>\n\n                <ion-col col-2 text-center class="primaryColor" class="couponUseDisabled" style="border-bottom: 2px solid white;">\n\n                    <div class="couponUse">\n\n                        <div *ngIf="company.bought && company.bought === 1">{{"COUPONS.USED" | translate}}</div>\n\n                    </div>\n\n                </ion-col>\n\n\n\n            </ion-row>\n\n\n\n        </ion-list>\n\n\n\n    </ion-grid>\n\n\n\n    <!--<div *ngIf="companies === null">No coupons</div>-->\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n    <common-footer></common-footer>\n\n</ion-footer>'/*ion-inline-end:"G:\github\vt-client-2\src\pages\coupons\coupons.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__["a" /* UtilsProvider */]])
    ], CouponsPage);
    return CouponsPage;
}());

//# sourceMappingURL=coupons.js.map

/***/ })

});
//# sourceMappingURL=19.js.map