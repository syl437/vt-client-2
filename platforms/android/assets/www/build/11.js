webpackJsonp([11],{

/***/ 847:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(873);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_push_service_push_service__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_onesignal__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, restangular, alertCtrl, auth, storage, init, utils, translate, menu, push, platform, onesignal) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.restangular = restangular;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.storage = storage;
        this.init = init;
        this.utils = utils;
        this.translate = translate;
        this.menu = menu;
        this.push = push;
        this.platform = platform;
        this.onesignal = onesignal;
        this.user = { email: '', password: '', push_id: '' };
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    LoginPage.prototype.authenticate = function () {
        var _this = this;
        if (this.user.email === '' || this.user.password === '') {
            var alert_1 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK'] });
            alert_1.present();
            return;
        }
        if (this.platform.is('cordova')) {
            this.onesignal.getIds().then(function (ids) {
                _this.user.push_id = ids['userId'];
                _this.restangular.all('tokens/users').post(_this.user).subscribe(function (data) {
                    _this.auth.setToken(data.api_token);
                    _this.storage.set('id', data.id).then(function () {
                        _this.auth.initialize().then(function () {
                            _this.init.onInitApp().then(function (success) {
                                _this.push.init();
                                _this.navCtrl.setRoot('HomePage');
                            });
                        });
                    });
                });
            });
        }
        else {
            this.user.push_id = null;
            this.restangular.all('tokens/users').post(this.user).subscribe(function (data) {
                _this.auth.setToken(data.api_token);
                _this.storage.set('id', data.id).then(function () {
                    _this.auth.initialize().then(function () {
                        _this.init.onInitApp().then(function (success) {
                            _this.push.init();
                            _this.navCtrl.setRoot('HomePage');
                        });
                    });
                });
            });
        }
    };
    LoginPage.prototype.goToRegister = function () {
        this.navCtrl.push('RegisterPage');
    };
    LoginPage.prototype.showForgotPopup = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: this.translate.instant("POPUPS.FORGOT"),
            message: this.translate.instant("POPUPS.ENTEREMAIL"),
            inputs: [
                {
                    name: 'email',
                    placeholder: this.translate.instant("POPUPS.EMAIL2"),
                },
            ],
            buttons: [
                { text: 'Cancel' },
                {
                    text: 'Submit',
                    handler: function (data) {
                        if (data.email !== '') {
                            _this.restangular.all('tokens/password').customPOST({ email: data.email }).subscribe(function (data) {
                                var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.PASSWORDSENT"), buttons: ['OK'] });
                                alert.present();
                            });
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    LoginPage.prototype.ionViewDidLeave = function () {
        this.menu.swipeEnable(true);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\login\login.html"*/'<ion-header>\n\n\n\n    <ion-navbar color="primary" text-center>\n\n        <ion-title>{{"LOGIN.LOGIN" | translate}}</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <div margin-top ion-text color="primary">{{"LOGIN.EXISTINGUSER" | translate}}</div>\n\n\n\n        <ion-list no-lines>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder=\'{{"ALL.EMAIL" | translate}}\' type="email" [(ngModel)]="user.email"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder=\'{{"ALL.PASSWORD" | translate}}\' type="password" [(ngModel)]="user.password"></ion-input>\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n\n\n        <button class="button-no-border" *ngIf="language === \'he\'" ion-button outline no-border color="primary" (click)="authenticate()">\n\n            {{"ALL.SEND" | translate}}&nbsp;<img src="assets/images/arrows_left.png" height="25">\n\n        </button>\n\n\n\n        <button class="button-no-border" *ngIf="language === \'en\'" ion-button outline no-border color="primary" (click)="authenticate()">\n\n            {{"ALL.SEND" | translate}}&nbsp;<img src="assets/images/arrows_right.png" height="25">\n\n        </button>\n\n\n\n    </div>\n\n\n\n    <div margin-top class="login-bar" float-left (click)="goToRegister()">\n\n        <span>{{"LOGIN.NEWUSER" | translate}}</span>\n\n    </div>\n\n\n\n    <div margin-top class="login-bar" float-left (click)="showForgotPopup()">\n\n        <span>{{"LOGIN.FORGOTPASSWORD" | translate}}</span>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_9__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_push_service_push_service__["a" /* PushService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_onesignal__["a" /* OneSignal */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=11.js.map