webpackJsonp([23],{

/***/ 833:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompaniesPageModule", function() { return CompaniesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__companies__ = __webpack_require__(859);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var CompaniesPageModule = /** @class */ (function () {
    function CompaniesPageModule() {
    }
    CompaniesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__companies__["a" /* CompaniesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__companies__["a" /* CompaniesPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_5__directives_directives_module__["a" /* DirectivesModule */]
            ],
        })
    ], CompaniesPageModule);
    return CompaniesPageModule;
}());

//# sourceMappingURL=companies.module.js.map

/***/ }),

/***/ 859:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompaniesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__ = __webpack_require__(87);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CompaniesPage = /** @class */ (function () {
    function CompaniesPage(navCtrl, navParams, restangular, init, utils, zone, auth, modalCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restangular = restangular;
        this.init = init;
        this.utils = utils;
        this.zone = zone;
        this.auth = auth;
        this.modalCtrl = modalCtrl;
        this.company_category_id = this.navParams.get('company_category_id');
        this.company_subcategory_id = 0;
        this.region_id = this.navParams.get('region_id');
        this.companies = [];
        this.company_categories = [];
        this.regions = [];
        this.filteredCompanies = [];
        this.user = this.init.user;
        this.load = this.restangular.all('companies').customGET('', { region_id: this.region_id, company_category_id: this.company_category_id });
        this.init.company_categories$.subscribe(function (company_categories) { return _this.company_categories = company_categories; });
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
        this.init.regions$.subscribe(function (regions) { return _this.regions = regions; });
    }
    CompaniesPage.prototype.filterCompanies = function () {
        var _this = this;
        if (this.company_subcategory_id === 0 && this.region_id === 0) {
            return this.filteredCompanies = this.companies;
        }
        if (this.region_id === 0) {
            return this.filteredCompanies = this.companies.filter(function (company) { return company.company_subcategory_id === _this.company_subcategory_id; });
        }
        if (this.company_subcategory_id === 0) {
            return this.filteredCompanies = this.companies.filter(function (company) { return company.region_id === _this.region_id; });
        }
        return this.filteredCompanies = this.companies.filter(function (company) { return company.company_subcategory_id === _this.company_subcategory_id && company.region_id === _this.region_id; });
    };
    CompaniesPage.prototype.goToCompany = function (id) {
        this.navCtrl.push('CompanyPage', { company_id: id });
    };
    CompaniesPage.prototype.goToUserReviews = function () {
        this.navCtrl.push('ReviewsPage', { company_category_id: this.company_category_id, region_id: this.region_id });
    };
    CompaniesPage.prototype.goToUser = function (id) {
        var modal = this.modalCtrl.create('UserPage', { user_id: id });
        modal.present();
    };
    CompaniesPage.prototype.ngOnInit = function () {
        var _this = this;
        this.load.subscribe(function (data) {
            console.log('ngOnInit', data);
            _this.companies = data;
            _this.filteredCompanies = data;
        });
    };
    CompaniesPage.prototype.ionViewDidLeave = function () {
        this.load.unsubscribe();
    };
    CompaniesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-companies',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\companies\companies.html"*/'<ion-header>\n\n\n\n    <picture-header></picture-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <ion-row align-items-center class="greyRow">\n\n\n\n        <ion-col col-6 text-right>\n\n            <ion-select [(ngModel)]="region_id" (ionChange)="filterCompanies()">\n\n                <ion-option [value]="0">{{"ALL.REGION" | translate}}</ion-option>\n\n                <ion-option *ngFor="let region of regions" [value]="region.id">{{region.title}}</ion-option>\n\n            </ion-select>\n\n        </ion-col>\n\n        <ion-col>\n\n            <ion-select [(ngModel)]="company_subcategory_id" (ionChange)="filterCompanies()" *ngIf="category !== 0">\n\n                <ion-option [value]="0">{{"ALL.SUBCATEGORY" | translate}}</ion-option>\n\n                <ion-option\n\n                        *ngFor="let subcategory of company_categories[company_category_id - 1].company_subcategories" [value]="subcategory.id">{{subcategory.title}}\n\n                </ion-option>\n\n            </ion-select>\n\n        </ion-col>\n\n\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n        <button margin class="user-reviews-button" block ion-button color="primary" (click)="goToUserReviews()">\n\n            {{"ALL.USERREVIEWS" | translate}}\n\n            <ion-icon name="ios-arrow-forward" [ngClass]="{\'icon-arrow1\' : language === \'he\', \'icon-arrow2\' : language === \'en\'}"></ion-icon>\n\n        </button>\n\n    </ion-row>\n\n\n\n    <ion-list *ngIf="companies.length > 0" no-padding no-margin no-border>\n\n\n\n        <ion-item *ngFor="let company of filteredCompanies" (click)="goToCompany(company.id)">\n\n\n\n            <ion-label fixed no-margin>\n\n                <img [src]="company.logo" *ngIf="company.logo != \'\'">\n\n                <img src="assets/images/logo.png" *ngIf="company.logo == \'\'">\n\n            </ion-label>\n\n            <div item-content>\n\n                <div><b>{{company.title}}</b></div>\n\n                <div>\n\n                    <ion-icon  *ngFor="let star of company.stars" name="ios-star" [ngClass]="{\'yellow-star\': star === true, \'grey-star\': star === false}"></ion-icon>\n\n                </div>\n\n                <div class="smallText">{{company.address}}</div>\n\n                <div class="smallText" style="padding-bottom: 5px;">{{company.phone}}</div>\n\n            </div>\n\n            <div item-end>\n\n                <img *ngIf="language === \'he\'" src="assets/images/arrows_left.png" width="20">\n\n                <img *ngIf="language === \'en\'" src="assets/images/arrows_right.png" width="20">\n\n            </div>\n\n\n\n        </ion-item>\n\n\n\n    </ion-list>\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n\n\n    <!--<div *ngIf="user_reviews.length > 0 && (company_category_id === 3 || company_category_id === 4)">-->\n\n\n\n        <!--<ion-scroll scrollX="true">-->\n\n\n\n            <!--<div *ngFor="let video of user_reviews" class="video-block small-video" text-center>-->\n\n\n\n                <!--<ion-item no-padding (click)="goToUser(video.user.id)">-->\n\n                    <!--<ion-avatar item-start>-->\n\n                        <!--<img [src]="video.user.avatar" *ngIf="video.user.avatar !== \'\'">-->\n\n                        <!--<img src="assets/images/avatar.jpg" *ngIf="video.user.avatar === \'\'">-->\n\n                    <!--</ion-avatar>-->\n\n                    <!--<div>{{video.user.name}}</div>-->\n\n                <!--</ion-item>-->\n\n                <!--<video my-video [source]="video.url" controls controlsList="nodownload" poster="{{video.thumbnail}}"></video>-->\n\n                <!--<div>{{video.pivot.tag}}</div>-->\n\n\n\n            <!--</div>-->\n\n\n\n        <!--</ion-scroll>-->\n\n\n\n    <!--</div>-->\n\n\n\n    <common-footer></common-footer>\n\n</ion-footer>'/*ion-inline-end:"G:\github\vt-client-2\src\pages\companies\companies.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
    ], CompaniesPage);
    return CompaniesPage;
}());

//# sourceMappingURL=companies.js.map

/***/ })

});
//# sourceMappingURL=23.js.map