webpackJsonp([21],{

/***/ 834:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyRegisterPageModule", function() { return CompanyRegisterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__company_register__ = __webpack_require__(860);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var CompanyRegisterPageModule = /** @class */ (function () {
    function CompanyRegisterPageModule() {
    }
    CompanyRegisterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__company_register__["a" /* CompanyRegisterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__company_register__["a" /* CompanyRegisterPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], CompanyRegisterPageModule);
    return CompanyRegisterPageModule;
}());

//# sourceMappingURL=company-register.module.js.map

/***/ }),

/***/ 860:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyRegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__agm_core__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth_service_auth_service__ = __webpack_require__(87);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CompanyRegisterPage = /** @class */ (function () {
    function CompanyRegisterPage(navCtrl, restangular, storage, utils, init, loader, auth) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.restangular = restangular;
        this.storage = storage;
        this.utils = utils;
        this.init = init;
        this.loader = loader;
        this.auth = auth;
        this.company_categories = [];
        this.company = this.restangular.restangularizeElement('', { email: '', password: '', title_en: '', title_he: '', phone: '',
            address: '', description_he: '', description_en: '', company_category_id: '', company_subcategory_id: '', region_id: '' }, 'companies');
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
        this.init.regions$.subscribe(function (regions) { return _this.regions = regions; });
        this.init.company_categories$.subscribe(function (company_categories) {
            _this.company_categories = company_categories.slice(1);
        });
    }
    CompanyRegisterPage.prototype.submit = function () {
        var _this = this;
        if (!this.utils.isAnyFieldEmpty(this.company.plain())) {
            this.storage.get('id').then(function (val) {
                _this.company.address = _this.formatted_address[0];
                _this.company.country = _this.formatted_address[1];
                _this.company.lat = _this.formatted_address[2];
                _this.company.lng = _this.formatted_address[3];
                console.log(_this.company.plain());
                _this.company.save().subscribe(function (data) {
                    _this.navCtrl.setRoot('CompanyRegisterAdditionalPage', { company_id: data.id });
                });
            });
        }
    };
    CompanyRegisterPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.loader.load().then(function () {
            var address = document.getElementById('company_address').getElementsByTagName('input')[0];
            var autocomplete = new google.maps.places.Autocomplete(address);
            autocomplete.addListener("place_changed", function () {
                var place = autocomplete.getPlace();
                console.log(place);
                var countryComponent = '';
                for (var _i = 0, _a = place.address_components; _i < _a.length; _i++) {
                    var item = _a[_i];
                    if (item.types[0] === 'country')
                        countryComponent = item.short_name;
                }
                _this.formatted_address = [place.formatted_address, countryComponent, place.geometry.location.lat(), place.geometry.location.lng()];
                console.log(_this.formatted_address);
            });
        });
    };
    CompanyRegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-company-register',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\company-register\company-register.html"*/'<ion-header>\n\n\n\n    <common-header title=\'{{"MENU.COMPANYREGISTER" | translate}}\'></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <div padding-top>If you are a hotel owner, please contact this phone: {{init.phone}}</div>\n\n\n\n        <ion-list no-lines>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input placeholder=\'{{"COMPANYREGISTER.TITLE_HE" | translate}}\' text-center [(ngModel)]="company.title_he"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input placeholder=\'{{"COMPANYREGISTER.TITLE_EN" | translate}}\' text-center [(ngModel)]="company.title_en"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input placeholder=\'{{"COMPANYREGISTER.ADDRESS" | translate}}\' text-center type="text" [(ngModel)]="company.address" id="company_address"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="customSelect deleteLeftPadding">\n\n                <ion-label color="primary">{{"ALL.CATEGORY" | translate}}</ion-label>\n\n                <ion-select [(ngModel)]="company.company_category_id">\n\n                    <ion-option *ngFor="let category of company_categories" [value]="category.id">{{category.title}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n\n\n            <ion-item *ngIf="company.company_category_id != \'\'" margin-top class="customSelect deleteLeftPadding">\n\n                <ion-label color="primary">{{"ALL.SUBCATEGORY" | translate}}</ion-label>\n\n                <ion-select [(ngModel)]="company.company_subcategory_id">\n\n                    <ion-option\n\n                            *ngFor="let subcategory of company_categories[company.company_category_id - 2].company_subcategories" [value]="subcategory.id">{{subcategory.title}}\n\n                    </ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input placeholder=\'{{"ALL.PHONE" | translate}}\' text-center type="tel" [(ngModel)]="company.phone"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input placeholder=\'{{"ALL.EMAIL" | translate}}\' text-center type="email" [(ngModel)]="company.email"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input placeholder=\'{{"ALL.PASSWORD" | translate}}\' text-center type="password" [(ngModel)]="company.password"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="customSelect deleteLeftPadding">\n\n                <ion-label color="primary">{{"ALL.REGION" | translate}}</ion-label>\n\n                <ion-select [(ngModel)]="company.region_id">\n\n                    <ion-option *ngFor="let region of regions" [value]="region.id">{{region.title}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-textarea placeholder=\'{{"COMPANYREGISTER.DESCRIPTION_HE" | translate}}\' text-right [(ngModel)]="company.description_he"></ion-textarea>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-textarea placeholder=\'{{"COMPANYREGISTER.DESCRIPTION_EN" | translate}}\' text-right [(ngModel)]="company.description_en"></ion-textarea>\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n\n\n        <button class="button-no-border" *ngIf="language === \'he\'" ion-button outline no-border color="primary" (click)="submit()">\n\n            {{"ALL.SUBMIT" | translate}}&nbsp;<img src="assets/images/arrows_left.png" height="25">\n\n        </button>\n\n\n\n        <button *ngIf="language === \'en\'" class="button-no-border" ion-button outline no-border color="primary" (click)="submit()">\n\n            {{"ALL.SUBMIT" | translate}}&nbsp;<img src="assets/images/arrows_right.png" height="25">\n\n        </button>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\company-register\company-register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_6__agm_core__["b" /* MapsAPILoader */],
            __WEBPACK_IMPORTED_MODULE_7__providers_auth_service_auth_service__["a" /* AuthServiceProvider */]])
    ], CompanyRegisterPage);
    return CompanyRegisterPage;
}());

//# sourceMappingURL=company-register.js.map

/***/ })

});
//# sourceMappingURL=21.js.map