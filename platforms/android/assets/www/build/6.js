webpackJsonp([6],{

/***/ 850:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register__ = __webpack_require__(876);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RegisterPageModule = /** @class */ (function () {
    function RegisterPageModule() {
    }
    RegisterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */]
            ],
        })
    ], RegisterPageModule);
    return RegisterPageModule;
}());

//# sourceMappingURL=register.module.js.map

/***/ }),

/***/ 876:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_push_service_push_service__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_onesignal__ = __webpack_require__(214);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, restangular, storage, utils, init, auth, fb, menu, push, onesignal) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.restangular = restangular;
        this.storage = storage;
        this.utils = utils;
        this.init = init;
        this.auth = auth;
        this.fb = fb;
        this.menu = menu;
        this.push = push;
        this.onesignal = onesignal;
        this.countries = [];
        this.form = fb.group({
            email: ['', [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].email]],
            password: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required],
            password_confirmation: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required],
            name: ['', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required],
            phone: ['', [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].minLength(9)]],
            country: ['IL', __WEBPACK_IMPORTED_MODULE_7__angular_forms__["f" /* Validators */].required]
        }, { validator: this.matchingPasswords('password', 'password_confirmation') });
        this.menu.swipeEnable(false);
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    RegisterPage.prototype.matchingPasswords = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        };
    };
    RegisterPage.prototype.submit = function () {
        var _this = this;
        var user = this.restangular.restangularizeElement('', {
            email: this.form.value.email,
            password: this.form.value.password,
            name: this.form.value.name,
            phone: this.form.value.phone,
            country: this.form.value.country,
            push_id: ''
        }, 'users');
        this.onesignal.getIds().then(function (ids) {
            user.push_id = ids['userId'];
            console.log(user);
            console.log(_this.form);
            user.save().subscribe(function (data) {
                _this.push.init();
                _this.storage.set('token', data.api_token).then(function () {
                    _this.auth.setToken(data.api_token);
                    _this.storage.set('id', data.id).then(function () {
                        _this.auth.initialize().then(function () {
                            _this.init.onInitApp().then(function () { _this.navCtrl.setRoot('HomePage'); });
                        });
                    });
                });
            });
        })
            .catch(function (error) { return console.log(error); });
    };
    RegisterPage.prototype.goToLogin = function () {
        this.navCtrl.push('LoginPage');
    };
    RegisterPage.prototype.ngOnInit = function () {
        var _this = this;
        this.restangular.all('init/countries').getList().subscribe(function (data) { return _this.countries = data; });
        this.menu.swipeEnable(false);
    };
    RegisterPage.prototype.ionViewDidLeave = function () {
        this.menu.swipeEnable(true);
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-register',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\register\register.html"*/'<ion-header>\n\n    <ion-navbar color="primary" text-center>\n\n        <ion-title>{{"REGISTER.REGISTER" | translate}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <form [formGroup]="form" (submit)="submit()">\n\n\n\n            <ion-row margin-top>\n\n              <ion-col col-12>\n\n                  <ion-input text-center type="email" id="email" name="email" placeholder=\'{{"ALL.EMAIL" | translate}}\' formControlName="email"></ion-input>\n\n                  <div class="green" *ngIf="!form.controls.email.hasError(\'required\') && !form.controls.email.hasError(\'email\')"><ion-icon name="checkmark"></ion-icon></div>\n\n              </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row margin-top>\n\n                <ion-col col-12>\n\n                    <ion-input text-center type="password" id="password" placeholder=\'{{"ALL.PASSWORD" | translate}}\' formControlName="password"></ion-input>\n\n                    <div class="green" *ngIf="!form.controls.password.hasError(\'required\')"><ion-icon name="checkmark"></ion-icon></div>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row margin-top>\n\n                <ion-col col-12>\n\n                    <ion-input text-center type="password" id="password_confirmation" placeholder=\'{{"ALL.PASSWORDCONFIRMATION" | translate}}\' formControlName="password_confirmation"></ion-input>\n\n                    <div class="green" *ngIf="!form.controls.password_confirmation.hasError(\'required\') && !form.hasError(\'mismatchedPasswords\')"><ion-icon name="checkmark"></ion-icon></div>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row margin-top>\n\n                <ion-col col-12>\n\n                    <ion-input text-center placeholder=\'{{"ALL.NAME" | translate}}\' id="name" formControlName="name"></ion-input>\n\n                    <div class="green" *ngIf="!form.controls.name.hasError(\'required\')"><ion-icon name="checkmark"></ion-icon></div>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row margin-top>\n\n                <ion-col col-12>\n\n                    <ion-input text-center type="tel" id="phone" placeholder=\'{{"ALL.PHONE" | translate}}\' formControlName="phone"></ion-input>\n\n                    <div class="green" *ngIf="!form.controls.phone.hasError(\'required\') && form.controls.phone.valid"><ion-icon name="checkmark"></ion-icon></div>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row margin-top>\n\n                <ion-col col-12>\n\n                    <ion-select formControlName="country" id="country">\n\n                        <ion-option *ngFor="let country of countries" value="{{country.code}}">{{country.name}}</ion-option>\n\n                    </ion-select>\n\n                    <div class="green" *ngIf="!form.controls.country.hasError(\'required\')"><ion-icon name="checkmark"></ion-icon></div>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n            <button margin-vertical class="button-no-border" *ngIf="language === \'he\'" [disabled]="form.invalid" ion-button outline no-border no-margin color="primary" type="submit">\n\n                {{"ALL.CONNECT" | translate}}&nbsp;<img src="assets/images/arrows_left.png" height="25">\n\n            </button>\n\n\n\n            <button margin-vertical class="button-no-border" *ngIf="language === \'en\'" [disabled]="form.invalid" ion-button outline no-border no-margin color="primary" type="submit">\n\n                {{"ALL.CONNECT" | translate}}&nbsp;<img src="assets/images/arrows_right.png" height="25">\n\n            </button>\n\n\n\n        </form>\n\n\n\n\n\n    </div>\n\n\n\n    <div class="login-bar" margin-bottom float-left (click)="goToLogin()">\n\n        <span>{{"REGISTER.EXISTINGUSER" | translate}}</span>\n\n    </div>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_push_service_push_service__["a" /* PushService */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_onesignal__["a" /* OneSignal */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ })

});
//# sourceMappingURL=6.js.map