webpackJsonp([12],{

/***/ 845:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeadPageModule", function() { return LeadPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_components_module__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lead__ = __webpack_require__(871);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var LeadPageModule = /** @class */ (function () {
    function LeadPageModule() {
    }
    LeadPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__lead__["a" /* LeadPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__lead__["a" /* LeadPage */]),
                __WEBPACK_IMPORTED_MODULE_2__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */],
            ],
        })
    ], LeadPageModule);
    return LeadPageModule;
}());

//# sourceMappingURL=lead.module.js.map

/***/ }),

/***/ 871:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LeadPage = /** @class */ (function () {
    function LeadPage(navParams, restangular, alertCtrl, auth, translate, utils) {
        var _this = this;
        this.navParams = navParams;
        this.restangular = restangular;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.translate = translate;
        this.utils = utils;
        this.is_hotel = this.navParams.get('is_hotel');
        this.company_id = this.navParams.get('company_id');
        this.lead = this.restangular.restangularizeElement('', { quantity: '', start: '', end: '', comments: '' }, 'leads');
        this.utils.language$.subscribe(function (data) { return _this.language = data; });
    }
    LeadPage.prototype.send = function () {
        var _this = this;
        console.log(this.lead.plain());
        if (this.lead.quantity === '' || this.lead.start === '') {
            var alert_1 = this.alertCtrl.create({ title: this.translate.instant("POPUPS.ALLFIELDS"), buttons: ['OK'] });
            alert_1.present();
            return;
        }
        this.lead.user_id = this.auth.id;
        this.lead.company_id = this.company_id;
        this.lead.save().subscribe(function () {
            var alert = _this.alertCtrl.create({ title: _this.translate.instant("POPUPS.REQUESTRECEIVED"), buttons: ['OK'] });
            alert.present();
            _this.lead = _this.restangular.restangularizeElement('', { quantity: '', start: '', end: '', comments: '' }, 'leads');
        });
    };
    LeadPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-lead',template:/*ion-inline-start:"G:\github\vt-client-2\src\pages\lead\lead.html"*/'<ion-header>\n\n\n\n    <common-header title=\'{{"LEAD.LEAD" | translate}}\'></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <ion-list no-lines>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder=\'{{"LEAD.NUMBEROFPERSONS" | translate}}\' type="number" [(ngModel)]="lead.quantity"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-label>{{"LEAD.STARTDATE" | translate}}</ion-label>\n\n                <ion-datetime displayFormat="DD/MM/YYYY" pickerFormat="DD/MMM/YYYY" [(ngModel)]="lead.start"></ion-datetime>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding" *ngIf="is_hotel === 1">\n\n                <ion-label>{{"LEAD.ENDDATE" | translate}}</ion-label>\n\n                <ion-datetime displayFormat="DD/MM/YYYY" pickerFormat="DD/MMM/YYYY" [(ngModel)]="lead.end"></ion-datetime>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-textarea placeholder=\'{{"LEAD.COMMENTS" | translate}}\' [(ngModel)]="lead.comments"></ion-textarea>\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n\n\n        <button class="button-no-border" *ngIf="language === \'he\'" ion-button outline no-border color="primary" (click)="send()">\n\n            {{"ALL.SEND" | translate}}&nbsp;<img src="assets/images/arrows_left.png" height="25">\n\n        </button>\n\n\n\n        <button class="button-no-border" *ngIf="language === \'en\'" ion-button outline no-border color="primary" (click)="send()">\n\n            {{"ALL.SEND" | translate}}&nbsp;<img src="assets/images/arrows_right.png" height="25">\n\n        </button>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\vt-client-2\src\pages\lead\lead.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */]])
    ], LeadPage);
    return LeadPage;
}());

//# sourceMappingURL=lead.js.map

/***/ })

});
//# sourceMappingURL=12.js.map